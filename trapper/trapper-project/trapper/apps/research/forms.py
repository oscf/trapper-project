# -*- coding: utf-8 -*-
"""Forms used by research application.

This module contains forms mainly related to
:class:`apps.research.models.ResearchProject`,
:class:`apps.research.models.ResearchProjectCollection` and
:class:`apps.research.models.ResearchProjectRole` models
"""
from crispy_forms.layout import Layout, Fieldset, HTML
from django import forms
from django.contrib.auth import get_user_model
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from extra_views import InlineFormSetFactory
from taggit.forms import TagField
from trapper.apps.common.fields import OwnerModelChoiceField
from trapper.apps.common.forms import (
    BaseCrispyForm,
    BaseCrispyModelForm,
    ProjectBaseInlineFormSet,
)
from trapper.apps.common.tools import parse_pks
from trapper.apps.research.models import ResearchProject, ResearchProjectRole
from trapper.apps.research.taxonomy import ResearchProjectRoleType
from trapper.apps.storage.models import Collection
from trapper.middleware import get_current_user

User = get_user_model()


class ResearchProjectForm(BaseCrispyModelForm):
    """
    Model form for creating or updating
    :class:`apps.research.models.ResearchProject` objects
    """

    acronym = forms.CharField(min_length=3, max_length=10)
    keywords = TagField(
        required=False, label=_("Keywords"), help_text="Comma or space delimited tags."
    )

    class Meta:
        model = ResearchProject
        exclude = ["project_roles", "owner", "status", "collections"]

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset(
                "",
                "name",
                "acronym",
                "sampling_design",
                "sensor_method",
                "animal_types",
                "bait_use",
                "event_interval",
                "keywords",
                "abstract",
                "methods",
                "description",
                HTML('{% include "research/project_role_formset.html" %}'),
            ),
        )

    def __init__(self, **kwargs):
        """:class:`ResearchProjectForm` contains few textarea fields that
        by default should be collapsed"""
        super().__init__(**kwargs)
        self.fields["description"].label_css = "collapsable"
        self.fields["abstract"].label_css = "collapsable"
        self.fields["methods"].label_css = "collapsable"


class ProjectCollectionAddForm(BaseCrispyForm):
    """"""

    project = forms.ModelChoiceField(
        queryset=ResearchProject.objects.none(),
        help_text=_(
            "To which research project you want to add the selected collection(s)?"
        ),
        label=_("Project"),
    )
    collections_pks = forms.CharField(
        widget=forms.HiddenInput(), label=_("Collections PKs")
    )

    def get_layout(self):
        """Define layout for crispy form helper"""
        return Layout(
            Fieldset("", "project", "collections_pks"),
        )

    def clean_collections_pks(self):
        collections_pks = self.cleaned_data.pop("collections_pks", None)
        project = self.cleaned_data.get("project", None)

        if project and collections_pks:
            pks_list = parse_pks(collections_pks)
            collections = Collection.objects.get_accessible().filter(pk__in=pks_list)
            project_collections_pks = list(
                set(project.collections.values_list("pk", flat=True))
            )
            new_collections = collections.exclude(pk__in=project_collections_pks)
            if not new_collections:
                error = mark_safe(
                    _("All selected collections are already in this research project.")
                )
                self.cleaned_data["error"] = error
            else:
                self.cleaned_data["new_collections"] = new_collections
        else:
            error = mark_safe(
                _(
                    "Nothing to process (most probably you have no permission "
                    "to run this action on the selected collections)."
                )
            )
            self.cleaned_data["error"] = error

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = get_current_user()
        self.fields["project"].queryset = ResearchProject.objects.get_accessible(
            user=self.user, role_levels=[1, 3]  # 1: Admin, 3: Collaborator
        ).order_by("acronym")


class ProjectRoleForm(BaseCrispyModelForm):
    """
    Model form for creating or updating
    :class:`apps.research.models.ResearchProjectRole` objects
    """

    form_style = "inline"
    user = OwnerModelChoiceField(queryset=User.objects.all())

    class Meta:
        model = ResearchProjectRole
        exclude = ["project", "date_created"]

    def __init__(self, *args, **kwargs):
        """Remove labels for formset"""
        super().__init__(*args, **kwargs)
        self.fields["user"].label = ""
        self.fields["user"].widget.attrs["class"] = "select2-formset form-control"
        self.fields["name"].label = ""

    def has_changed(self):
        """
        https://stackoverflow.com/a/22789926/11207840
        """
        changed_data = super().has_changed()
        if self.instance.pk:
            return changed_data
        else:
            return bool(self.initial or changed_data)


class ProjectRoleInline(InlineFormSetFactory):
    """Utility-class: ProjectRoles displayed as a InlineFormset"""

    model = ResearchProjectRole
    form_class = ProjectRoleForm
    formset_class = ProjectBaseInlineFormSet
    factory_kwargs = {"extra": 0}


class ProjectRoleCreateInline(ProjectRoleInline):
    """"""

    factory_kwargs = {"extra": 1}

    def get_initial(self):
        initial = [{"user": self.request.user, "name": ResearchProjectRoleType.ADMIN}]
        return initial
