# -*- coding: utf-8 -*-
"""Views used by DRF to display json data used by research application"""

from trapper.apps.common.views_api import PaginatedReadOnlyModelViewSet
from trapper.apps.research import serializers as research_serializers
from trapper.apps.research.filters import (
    ResearchProjectFilter,
    ResearchProjectCollectionFilter,
)
from trapper.apps.research.models import ResearchProject, ResearchProjectCollection


class ResearchProjectViewSet(PaginatedReadOnlyModelViewSet):
    """Return list of research projects.
    List of projects is limited only to those which are accessible
    for current user (if provided)
    """

    serializer_class = research_serializers.ResearchProjectSerializer
    filterset_class = ResearchProjectFilter
    search_fields = ["name", "acronym", "abstract", "owner__username"]

    def get_queryset(self):
        """
        Limit projects depend on user login status
        """
        base_querset = ResearchProject.objects.all()
        params = {"base_queryset": base_querset}

        user = self.request.user
        is_authenticated = user.is_authenticated
        if is_authenticated:
            params["user"] = user

        if "no-pagination" in self.request.GET:
            self.pagination_class = None

        queryset = ResearchProject.objects.get_accessible(**params).order_by("name")
        return queryset.select_related("owner").prefetch_related(
            "keywords", "project_roles__user"
        )


class ResearchProjectCollectionViewSet(PaginatedReadOnlyModelViewSet):
    """Return list of research project collections.
    List of project collections is limited only to those which are accessible
    for current user (if provided)
    """

    serializer_class = research_serializers.ResearchProjectCollectionSerializer
    filterset_class = ResearchProjectCollectionFilter
    search_fields = ["collection__name", "collection__owner__username"]

    def get_queryset(self):
        project_pk = self.kwargs["project_pk"]
        try:
            project = ResearchProject.objects.get(pk=project_pk)
        except ResearchProject.DoesNotExist:
            return ResearchProject.objects.none()
        if project.can_view(user=self.request.user):
            return ResearchProjectCollection.objects.filter(
                project=project
            ).select_related(
                "collection", "collection__owner", "project", "project__owner"
            )
        else:
            return ResearchProjectCollection.objects.none()
