import factory

from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory


class ResearchProjectCollectionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ResearchProjectCollection
        exclude = ("now",)

    project = factory.SubFactory(ResearchProjectFactory)
    collection = factory.SubFactory(CollectionFactory)
