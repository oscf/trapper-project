# -*- coding: utf-8 -*-
"""Filters used in research application when backend is used to
limit data"""
import django_filters
from trapper.apps.common.filters import BaseFilterSet, BaseOwnBooleanFilter
from trapper.apps.research.models import ResearchProject, ResearchProjectCollection
from trapper.apps.research.taxonomy import ResearchProjectRoleType


class ResearchProjectFilter(BaseFilterSet):
    """Filter for owned :class:`apps.research.models.ResearchProject` model"""

    owner = BaseOwnBooleanFilter(
        managers=False, role_levels=ResearchProjectRoleType.ANY
    )
    keywords = django_filters.MultipleChoiceFilter()
    acronym = django_filters.CharFilter()

    class Meta:
        model = ResearchProject
        fields = ["owner", "keywords", "acronym"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filters["keywords"].field.choices = ResearchProject.keywords.values_list(
            "pk", "name"
        )


class ResearchProjectCollectionFilter(BaseFilterSet):
    """Filter for owned
    :class:`apps.research.models.ResearchProjectCollection` model"""

    owner = BaseOwnBooleanFilter(
        owner_field="collection__owner",
        managers_field="collection__managers",
    )
    owners = django_filters.MultipleChoiceFilter(field_name="collection__owner")
    status = django_filters.Filter(field_name="collection__status")

    class Meta:
        model = ResearchProjectCollection
        fields = ["owner", "status"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # set choices for multiple choice filters
        self.filters["owners"].field.choices = set(
            self.queryset.values_list(
                "collection__owner__pk", "collection__owner__username"
            )
        )
