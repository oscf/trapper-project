from trapper.apps.geomap.models import Deployment
from trapper.apps.media_classification.models import (
    ClassificationProject,
    Classification,
)
from trapper.apps.storage.taxonomy import ResourceStatus

from rest_framework.generics import get_object_or_404
from django.db.models import Q

from trapper.apps.teams.models import TeamMembership


class BaseCSDeploymentMixin:
    def get_deployments(self, classification_project, user):
        # deployment_ids = classification_project.classifications.values_list(
        #     "resource__deployment_id", flat=True
        # )
        #
        # queryset = Deployment.objects.filter(id__in=deployment_ids)

        # Get all deployments related with specific Research Project
        queryset = classification_project.research_project.deployments.all()

        # return queryset if user can view all classifications in the project
        # i.e. it has a role included in `ClassificationProjectRoleLevels.VIEW_CLASSIFICATIONS`
        if classification_project.can_view_classifications(user):
            return queryset

        return queryset.filter(
            (
                Q(owner=user)
                | Q(status=Deployment.Status.PUBLIC)
                | (
                    Q(team_deployments__team_memberships__user=user)
                    & Q(
                        team_deployments__team_memberships__status=TeamMembership.MembershipStatus.APPROVED
                    )
                )
            )
        ).distinct()


class BaseCSMixin:
    classification_project = None

    def get_object(self):
        return self.get_classification_project()

    def get_classification_project(self):
        if not self.classification_project:
            slug_id = self.kwargs["cs_project_slug"][-12:]
            self.classification_project = get_object_or_404(
                ClassificationProject.objects.only_cs(self.request.user),
                slug_id=slug_id,
            )
        return self.classification_project

    def _get_base_classifications_queryset(self, classification_project):
        return (
            Classification.objects.filter(
                approved_source_ai__isnull=False,
                project=classification_project,
                # TODO: uncomment on prod
                # dynamic_attrs__observation_type__in=[ObservationType.ANIMAL, ObservationType.BLANK]
            )
            .exclude(resource__file_thumbnail="")
            .order_by("resource__date_recorded", "pk")  # resource__name
            .distinct()
        )

    def get_classifications_queryset(self, classification_project=None, user=None):
        if not classification_project:
            classification_project = self.get_classification_project()
        if not user:
            user = self.request.user
        queryset = self._get_base_classifications_queryset(classification_project)

        # return queryset if user can view all classifications in the project
        # i.e. it has a role included in `ClassificationProjectRoleLevels.VIEW_CLASSIFICATIONS`
        if classification_project.can_view_classifications(user):
            return queryset

        return queryset.filter(
            Q(resource__status=ResourceStatus.PUBLIC)
            | Q(resource__owner=user)
            | (
                Q(resource__deployment__team_deployments__team_memberships__user=user)
                & Q(
                    resource__deployment__team_deployments__team_memberships__status=TeamMembership.MembershipStatus.APPROVED
                )
            )
        )

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["classification_project"] = self.get_classification_project()
        return context
