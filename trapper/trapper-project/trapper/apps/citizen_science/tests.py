import base64
import json
import pytz
from django.contrib.gis.geos import Point
from django.core.files.uploadedfile import SimpleUploadedFile
from django.urls import reverse
from rest_framework import status
from rest_framework.fields import DateTimeField
from rest_framework.test import APITestCase, APITransactionTestCase

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.citizen_science.models import CitizenScienceConfiguration
from trapper.apps.common.models import Consent
from trapper.apps.common.tests.commons import BaseAPITestCase
from trapper.apps.extra_tables.tests.factories import SpeciesFactory
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.models import (
    ClassificationProject,
    Classification,
)
from trapper.apps.media_classification.taxonomy import (
    ObservationType,
    ClassificationProjectRoleLevels,
)
from trapper.apps.media_classification.tests.factories.ai_classification import (
    AIClassificationFactory,
    AIClassificationDynamicAttrsFactory,
)
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationFactory,
    ClassificationDynamicAttrsFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_collection import (
    ClassificationProjectCollectionFactory,
)
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.media_classification.tests.factories.sequence import SequenceFactory
from trapper.apps.media_classification.tests.factories.user_classification import (
    UserClassificationFactory,
    UserClassificationDynamicAttrsFactory,
)
from trapper.apps.messaging.models import Message
from trapper.apps.messaging.taxonomies import MessageType
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import CollectionStatus, ResourceStatus
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class CSBaseViewTests(APITestCase):
    def test_gdpr_correct(self):
        gdpr = Consent.objects.create(
            project=Consent.Project.CS,
            type=Consent.Type.GDPR,
            language="en",
            content="<p>asdasd</p>\r\n<p><strong>asdasd</strong></p>\r\n",
            version="1",
        )
        response = self.client.get(reverse("cs_gdpr"))
        self.assertEqual(
            response.json(),
            {
                "date_modified": DateTimeField().to_representation(gdpr.date_modified),
                "content": "<p>asdasd</p>\r\n<p><strong>asdasd</strong></p>\r\n",
                "version": "1",
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_tos_correct(self):
        tos = Consent.objects.create(
            project=Consent.Project.CS,
            type=Consent.Type.TOS,
            language="en",
            content="<p>asdasd</p>\r\n<p><strong>asdasd</strong></p>\r\n",
            version="1",
        )
        response = self.client.get(reverse("cs_tos"))
        self.assertEqual(
            response.json(),
            {
                "date_modified": DateTimeField().to_representation(tos.date_modified),
                "content": "<p>asdasd</p>\r\n<p><strong>asdasd</strong></p>\r\n",
                "version": "1",
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_project_settings_correct(self):
        from PIL import Image

        img = Image.new("RGB", (800, 1280), (255, 255, 255))
        img.save("image.png", "PNG")

        linkedin_url = "http://linkedin.com/"
        facebook_url = "http://facebook.com/"
        twitter_url = "http://twitter.com/"
        additional_styles = "body { background-color: red; }"
        CitizenScienceConfiguration.objects.create(
            project_name="Trapper",
            project_logo=SimpleUploadedFile(
                name="test_image.jpg", content=img.tobytes(), content_type="image/jpeg"
            ),
            linkedin_url=linkedin_url,
            facebook_url=facebook_url,
            twitter_url=twitter_url,
            default_coordinates=Point(x=50, y=50),
            additional_styles=additional_styles,
        )
        response = self.client.get(reverse("cs_settings"))
        self.assertDictEqual(
            response.json(),
            {
                "project_name": "Trapper",
                "project_logo": "/api/cs/logo/",
                "linkedin_url": linkedin_url,
                "facebook_url": facebook_url,
                "twitter_url": twitter_url,
                "default_latitude": 50.0,
                "default_longitude": 50.0,
                "additional_styles": additional_styles,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DashboardTests(BaseAPITestCase):
    def setUp(self):
        super().setUp()
        self.user1 = UserFactory()
        self.classification_project = ClassificationProjectFactory(
            owner=self.user1,
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC,
        )
        self.classification_project.classification_project_roles.create(
            user=self.user1, name=ClassificationProjectRoleLevels.CS_USER
        )
        self.classification = ClassificationFactory(
            owner=self.user1,
            project=self.classification_project,
        )
        self.species1 = SpeciesFactory()
        self.species2 = SpeciesFactory()
        self.species3 = SpeciesFactory()
        self.sequence1 = SequenceFactory()
        self.sequence2 = SequenceFactory()
        ClassificationDynamicAttrsFactory.create(
            classification__project=self.classification_project,
            classification__approved_source_ai=AIClassificationFactory.create(
                classification=self.classification
            ),
            classification__sequence=self.sequence1,
            species=self.species1,
            observation_type=ObservationType.ANIMAL,
            classification__created_at="2022-10-16T01:01:01.806702+02:00",
            classification__resource__date_recorded="2022-10-16T01:01:01.806702+02:00",
        )
        ClassificationDynamicAttrsFactory.create(
            classification__project=self.classification_project,
            classification__approved_source_ai=AIClassificationFactory.create(
                classification=self.classification
            ),
            classification__sequence=self.sequence1,
            species=self.species1,
            observation_type=ObservationType.ANIMAL,
        )
        ClassificationDynamicAttrsFactory.create(
            classification__project=self.classification_project,
            classification__approved_source_ai=AIClassificationFactory.create(
                classification=self.classification
            ),
            observation_type=ObservationType.ANIMAL,
            species=self.species3,
        )
        ClassificationDynamicAttrsFactory.create(
            classification__project=self.classification_project,
            classification__approved_source_ai=AIClassificationFactory.create(
                classification=self.classification
            ),
            classification__sequence=self.sequence2,
            species=self.species3,
            observation_type=ObservationType.ANIMAL,
        )
        ClassificationDynamicAttrsFactory.create(
            classification__project=self.classification_project,
            classification__approved_source_ai=AIClassificationFactory.create(
                classification=self.classification
            ),
            classification__sequence=self.sequence2,
            species=self.species2,
            observation_type=ObservationType.ANIMAL,
        )
        ClassificationDynamicAttrsFactory.create(
            classification__project=self.classification_project,
            classification__approved_source_ai=AIClassificationFactory.create(
                classification=self.classification
            ),
            species=self.species2,
            observation_type=ObservationType.ANIMAL,
        )
        ClassificationDynamicAttrsFactory.create(
            classification__project=self.classification_project,
            classification__approved_source_ai=AIClassificationFactory.create(
                classification=self.classification
            ),
            species=self.species2,
            observation_type=ObservationType.ANIMAL,
        )

    def test_dashboard_correct(self):
        response = self.client.get(
            reverse("cs_dashboard", args=(self.classification_project.slug,))
        )
        self.assertDictEqual(
            response.json(),
            {
                "joined_date": None,
                "own_statistics": {
                    "camera_trap_days": 0,
                    "deployments": 0,
                    "images": 0,
                    "locations": 0,
                    "recordings": 0,
                    "pie_chart": [],
                    "bar_chart": [],
                },
                "project_statistics": {
                    "active_users": 1,
                    "camera_trap_days": 140,
                    "deployments": 7,
                    "images": 7,
                    "locations": 7,
                    "recordings": 7,
                    "pie_chart": [
                        {"english_name": self.species2.english_name, "total": 3},
                        {"english_name": self.species1.english_name, "total": 2},
                        {"english_name": self.species3.english_name, "total": 2},
                    ],
                    "bar_chart": [
                        {
                            "english_name": self.species2.english_name,
                            "images": 3,
                            "sequences": 3,
                        },
                        {
                            "english_name": self.species1.english_name,
                            "images": 2,
                            "sequences": 1,
                        },
                        {
                            "english_name": self.species3.english_name,
                            "images": 2,
                            "sequences": 2,
                        },
                    ],
                },
                "start_date": "2022-10-16T01:01:01.806702+02:00",
            },
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CSMediaTests(BaseAPITestCase):
    def setUp(self):
        super().setUp()
        """
        Authenticated user is self.user1.
        User have classifications:
        - self.classification1 with self.resource1:
            - with self.ai_classification1
        - self.classification2 with self.resource2:
            - with self.ai_classification2
        - self.classification3 with self.resource2:
            - with self.user_classification3
        - self.classification4 with self.resource4:
            - with self.user_classification4
        - self.classification5 with self.resource:
            - with approved self.ai_classification
            - with one self.user_classification with bboxes and species
        """
        self.user1 = UserFactory()

        self.research_project = ResearchProjectFactory(owner=self.user1)

        self.timezone = pytz.timezone("Europe/Warsaw")
        self.location1 = LocationFactory(
            owner=self.user1,
            research_project=self.research_project,
            timezone="Europe/Warsaw",
        )

        self.deployment1 = DeploymentFactory(
            owner=self.user1,
            research_project=self.research_project,
            location=self.location1,
        )

        self.resource1 = ResourceFactory(owner=self.user1, deployment=self.deployment1)
        self.resource2 = ResourceFactory(owner=self.user1, deployment=self.deployment1)
        self.resource3 = ResourceFactory(
            owner=self.user1, deployment=self.deployment1, inherit_prefix=True
        )
        self.resource4 = ResourceFactory(owner=self.user1, deployment=self.deployment1)

        self.collection1 = CollectionFactory(owner=self.user1)
        self.collection2 = CollectionFactory(owner=self.user1)

        # # Set collection resources
        self.collection1.resources.set((self.resource1,))
        self.collection2.resources.set((self.resource1, self.resource3))

        self.collection = CollectionFactory(
            owner=self.user1, status=CollectionStatus.PRIVATE
        )
        self.collection.resources.set(Resource.objects.all())
        self.classificator = ClassificatorFactory(owner=self.user1)

        self.classification_project = ClassificationProjectFactory(
            research_project=self.research_project,
            owner=self.user1,
            classificator=self.classificator,
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC,
        )

        self.research_project.collections.set([self.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=self.classification_project.research_project.pk
        )[0]
        self.classification_project_collection = ClassificationProjectCollectionFactory(
            project=self.classification_project, collection=collection
        )

        self.resource = ResourceFactory(
            owner=self.user1, deployment=self.deployment1, status=ResourceStatus.PRIVATE
        )
        self.sequence1 = SequenceFactory(
            collection=self.classification_project_collection
        )
        self.sequence1.resources.set([self.resource1, self.resource2])
        self.sequence3 = SequenceFactory(
            collection=self.classification_project_collection
        )
        self.sequence1.resources.set([self.resource3, self.resource4])

        self.species1 = SpeciesFactory()
        self.species2 = SpeciesFactory()
        self.classification1 = Classification.objects.get(
            project=self.classification_project, resource=self.resource1
        )
        self.classification1.sequence = self.sequence1
        self.classification1.save()

        self.ai_classification1 = AIClassificationFactory(
            classification=self.classification1, owner=self.user1
        )
        self.dynamic_ai_classification1 = AIClassificationDynamicAttrsFactory(
            ai_classification=self.ai_classification1,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )
        self.classification2 = Classification.objects.get(
            project=self.classification_project, resource=self.resource2
        )
        self.classification2.sequence = self.sequence1
        self.classification2.save()

        self.ai_classification2 = AIClassificationFactory(
            classification=self.classification2, owner=self.user1
        )
        self.dynamic_ai_classification2 = AIClassificationDynamicAttrsFactory(
            ai_classification=self.ai_classification2,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )
        self.classification3 = Classification.objects.get(
            project=self.classification_project, resource=self.resource3
        )
        self.classification3.sequence = self.sequence3
        self.classification3.save()

        self.user_classification3 = UserClassificationFactory(
            classification=self.classification3, owner=self.user1
        )
        self.dynamic_user_classification3 = UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification3,
            observation_type=ObservationType.ANIMAL,
            species=self.species1,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )
        self.classification4 = Classification.objects.get(
            project=self.classification_project, resource=self.resource4
        )
        self.classification4.sequence = SequenceFactory(
            collection=self.classification_project_collection
        )
        self.classification4.save()

        self.user_classification4 = UserClassificationFactory(
            classification=self.classification4, owner=self.user1
        )
        self.dynamic_user_classification4 = UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            species=self.species2,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )

        self.classification5 = ClassificationFactory(
            resource=self.resource,
            owner=self.user1,
            project=self.classification_project,
            collection=self.classification_project_collection,
            sequence=SequenceFactory(collection=self.classification_project_collection),
        )
        self.ai_classification5 = AIClassificationFactory.create(
            classification=self.classification5
        )
        self.classification5.approved_source_ai = self.ai_classification5
        self.classification5.save()
        self.user_classification5 = UserClassificationFactory.create(
            classification=self.classification5,
            owner=self.user1,
        )
        self.user_classifications_dynamic = (
            UserClassificationDynamicAttrsFactory.create(
                userclassification=self.user_classification5,
                observation_type=ObservationType.ANIMAL,
                species=self.species1,
                bboxes=[[0.444, 0.444, 0.333, 0.333]],
            )
        )
        self.client.force_authenticate(self.user1)

    def test_get_projects_correct(self):
        response = self.client.get(reverse("cs_projects"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertListEqual(
            response.json(),
            [
                {
                    "name": self.classification_project.name,
                    "slug": self.classification_project.slug,
                    "description": self.classification_project.research_project.description,
                    "tags": [],
                    "image_background": "",
                }
            ],
        )

    def test_get_inbox_correct(self):
        data = {
            "subject": "subject",
            "text": "text",
            "user_from": self.user1,
            "user_to": self.user1,
            "message_type": MessageType.STANDARD,
        }
        message = Message.objects.create(**data)
        response = self.client.get(reverse("cs_inbox"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(
            response.json(),
            {
                "pagination": {
                    "page": 1,
                    "page_size": 10,
                    "pages": 1,
                    "count": 1,
                    "total": 1,
                },
                "results": [
                    {
                        "pk": message.pk,
                        "subject": message.subject,
                        "text": message.text,
                        "date_sent": DateTimeField().to_representation(
                            message.date_sent
                        ),
                    }
                ],
            },
        )

    def test_mark_as_received_correct(self):
        data = {
            "subject": "subject",
            "text": "text",
            "user_from": self.user1,
            "user_to": self.user1,
            "message_type": MessageType.STANDARD,
        }
        message = Message.objects.create(**data)
        response = self.client.patch(
            reverse("cs_inbox_mark_as_received", args=[message.pk])
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        message.refresh_from_db()
        self.assertIsNotNone(message.date_received)

    def test_cs_media_correct(self):
        response = self.client.get(
            reverse("cs_media", args=[self.classification_project.slug])
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(
            response.json(),
            {
                "pagination": {
                    "count": 1,
                    "page": 1,
                    "page_size": 10,
                    "pages": 1,
                    "total": 1,
                },
                "results": [
                    {
                        "bboxes": [
                            {
                                "height": 0.333,
                                "left": 0.444,
                                "top": 0.444,
                                "width": 0.333,
                            }
                        ],
                        "date_recorded": DateTimeField().to_representation(
                            self.classification5.resource.date_recorded
                        ),
                        "deployment": {
                            "deployment_code": self.deployment1.deployment_code,
                            "deployment_id": self.deployment1.deployment_id,
                            "pk": self.deployment1.pk,
                        },
                        "file_name": self.resource.get_file_name(),
                        "has_feedback": False,
                        "is_approved": False,
                        "is_classified_by_me": True,
                        "is_favorite": False,
                        "location_name": self.location1.name,
                        "name": self.classification5.resource.name,
                        "observation_type": ["Animal"],
                        "original_url": self.classification5.resource.get_cs_url(),
                        "owner": {
                            "avatar": "",
                            "id": self.user1.pk,
                            "name": self.user1.username,
                        },
                        "pk": self.classification5.pk,
                        "sequence": {
                            "id": self.classification5.sequence.pk,
                            "sequence_id": self.classification5.sequence.sequence_id,
                        },
                        "species": [
                            {
                                "__str__": str(
                                    self.user_classifications_dynamic.species
                                ),
                                "english_name": self.user_classifications_dynamic.species.english_name,
                                "family": None,
                                "genus": None,
                                "id": self.user_classifications_dynamic.species.pk,
                                "latin_name": self.user_classifications_dynamic.species.latin_name,
                            }
                        ],
                        "thumbnail_url": self.classification5.resource.get_cs_url(
                            field="tfile"
                        ),
                    }
                ],
            },
        )

    def test_filter_cs_project_media(self):
        response = self.client.get(
            f"{reverse('cs_media', args=[self.classification_project.slug])}?species={self.species1.id}&by_me=1"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(
            response.json()["pagination"],
            {
                "count": 1,
                "page": 1,
                "page_size": 10,
                "pages": 1,
                "total": 1,
            },
        )


class CSMediaScrollTests(BaseAPITestCase):
    def setUp(self):
        super().setUp()
        """
        Authenticated user is self.user1.
        User have classifications:
        - self.classification1 with self.resource1:
            - with self.ai_classification1
        - self.classification2 with self.resource2:
            - with self.ai_classification2
        - self.classification3 with self.resource2:
            - with self.user_classification3
        - self.classification4 with self.resource4:
            - with self.user_classification4
        - self.classification5 with self.resource:
            - with approved self.ai_classification
            - with one self.user_classification with bboxes and species
        """
        self.user1 = UserFactory()

        self.research_project = ResearchProjectFactory(owner=self.user1)

        self.timezone = pytz.timezone("Europe/Warsaw")
        self.location1 = LocationFactory(
            owner=self.user1,
            research_project=self.research_project,
            timezone="Europe/Warsaw",
        )

        self.deployment1 = DeploymentFactory(
            owner=self.user1,
            research_project=self.research_project,
            location=self.location1,
        )

        self.resource1 = ResourceFactory(owner=self.user1, deployment=self.deployment1)
        self.resource2 = ResourceFactory(owner=self.user1, deployment=self.deployment1)
        self.resource3 = ResourceFactory(
            owner=self.user1, deployment=self.deployment1, inherit_prefix=True
        )
        self.resource4 = ResourceFactory(owner=self.user1, deployment=self.deployment1)

        self.collection1 = CollectionFactory(owner=self.user1)
        self.collection2 = CollectionFactory(owner=self.user1)

        # # Set collection resources
        self.collection1.resources.set((self.resource1,))
        self.collection2.resources.set((self.resource1, self.resource3))

        self.collection = CollectionFactory(
            owner=self.user1, status=CollectionStatus.PRIVATE
        )
        self.collection.resources.set(Resource.objects.all())
        self.classificator = ClassificatorFactory(owner=self.user1)

        self.classification_project = ClassificationProjectFactory(
            research_project=self.research_project,
            owner=self.user1,
            classificator=self.classificator,
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC,
        )

        self.research_project.collections.set([self.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=self.classification_project.research_project.pk
        )[0]
        self.classification_project_collection = ClassificationProjectCollectionFactory(
            project=self.classification_project, collection=collection
        )

        self.resource = ResourceFactory(
            owner=self.user1,
            deployment=self.deployment1,
            status=ResourceStatus.PRIVATE,
        )
        self.sequence1 = SequenceFactory(
            collection=self.classification_project_collection
        )
        self.sequence1.resources.set([self.resource1, self.resource2])
        self.sequence3 = SequenceFactory(
            collection=self.classification_project_collection
        )
        self.sequence1.resources.set([self.resource3, self.resource4])

        self.species1 = SpeciesFactory()
        self.species2 = SpeciesFactory()
        self.classification1 = Classification.objects.get(
            project=self.classification_project, resource=self.resource1
        )
        self.classification1.sequence = self.sequence1
        self.classification1.save()

        self.ai_classification1 = AIClassificationFactory(
            classification=self.classification1, owner=self.user1
        )
        self.dynamic_ai_classification1 = AIClassificationDynamicAttrsFactory(
            ai_classification=self.ai_classification1,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )
        self.classification2 = Classification.objects.get(
            project=self.classification_project, resource=self.resource2
        )
        self.classification2.sequence = self.sequence1
        self.classification2.save()

        self.ai_classification2 = AIClassificationFactory(
            classification=self.classification2, owner=self.user1
        )
        self.dynamic_ai_classification2 = AIClassificationDynamicAttrsFactory(
            ai_classification=self.ai_classification2,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )
        self.classification3 = Classification.objects.get(
            project=self.classification_project, resource=self.resource3
        )
        self.classification3.sequence = self.sequence3
        self.classification3.save()

        self.user_classification3 = UserClassificationFactory(
            classification=self.classification3, owner=self.user1
        )
        self.dynamic_user_classification3 = UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification3,
            observation_type=ObservationType.ANIMAL,
            species=self.species1,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )
        self.classification4 = Classification.objects.get(
            project=self.classification_project, resource=self.resource4
        )
        self.classification4.sequence = SequenceFactory(
            collection=self.classification_project_collection
        )
        self.classification4.save()

        self.user_classification4 = UserClassificationFactory(
            classification=self.classification4, owner=self.user1
        )
        self.dynamic_user_classification4 = UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            species=self.species2,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )

        self.classification5 = ClassificationFactory(
            resource=self.resource,
            owner=self.user1,
            project=self.classification_project,
            collection=self.classification_project_collection,
            sequence=SequenceFactory(collection=self.classification_project_collection),
        )
        self.ai_classification5 = AIClassificationFactory.create(
            classification=self.classification5
        )
        self.classification5.approved_source_ai = self.ai_classification5
        self.classification5.save()
        self.user_classification5 = UserClassificationFactory.create(
            classification=self.classification5,
            owner=self.user1,
        )
        self.user_classifications_dynamic = (
            UserClassificationDynamicAttrsFactory.create(
                userclassification=self.user_classification5,
                observation_type=ObservationType.ANIMAL,
                species=self.species1,
                bboxes=[[0.444, 0.444, 0.333, 0.333]],
            )
        )
        self.classification1.approved_source_ai = self.ai_classification1
        self.classification1.save()
        self.classification2.approved_source_ai = self.ai_classification2
        self.classification2.save()
        ai_classification3 = AIClassificationFactory.create(
            classification=self.classification3
        )
        self.classification3.approved_source_ai = ai_classification3
        self.classification3.save()
        ai_classification4 = AIClassificationFactory.create(
            classification=self.classification4
        )
        self.classification4.approved_source_ai = ai_classification4
        self.classification4.save()
        self.client.force_authenticate(self.user1)

    def test_cs_media_scroll_correct(self):
        response = self.client.get(
            reverse("cs_media_scroll", args=[self.classification_project.slug])
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        results = response.json()["results"]

        # self.classification1 1 seq:  1
        # self.classification2 2 seq:  1
        # self.classification3 3 seq:  2
        # self.classification4 4 seq:  3
        # self.classification5 5 seq:  4

        seq_next_pk = {
            1: self.classification3.pk,
            2: self.classification4.pk,
            3: self.classification5.pk,
            4: None,
        }

        seq_previous_pk = {
            1: None,
            2: self.classification1.pk,
            3: self.classification3.pk,
            4: self.classification4.pk,
        }

        expected_seq = [
            (self.classification1.pk, 1),
            (self.classification2.pk, 1),
            (self.classification3.pk, 2),
            (self.classification4.pk, 3),
            (self.classification5.pk, 4),
        ]

        for i, result in enumerate(results):
            seq = result["sequence"]["sequence_id"]
            self.assertEqual(result["pk"], expected_seq[i][0], f"PK invalid for {i}")
            self.assertEqual(seq, expected_seq[i][1], f"Sequence invalid for {i}")
            self.assertEqual(
                result["previous_classification_sequence"],
                seq_previous_pk.get(seq),
                f"previous_classification_sequence invalid for {i}",
            )
            self.assertEqual(
                result["next_classification_sequence"],
                seq_next_pk.get(seq),
                f"next_classification_sequence invalid for {i}",
            )


class CSMediaScrollPaginationTests(APITestCase):
    def setUp(self) -> None:
        super().setUp()

        self.user = UserFactory()
        self.client.force_authenticate(self.user)

        self.research_project = ResearchProjectFactory(owner=self.user)
        self.location = LocationFactory(
            owner=self.user, research_project=self.research_project
        )
        self.deployment = DeploymentFactory(
            owner=self.user,
            research_project=self.research_project,
            location=self.location,
        )
        self.classification_project = ClassificationProjectFactory(
            research_project=self.research_project,
            owner=self.user,
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC,
        )

        self.classification_project_collection = ClassificationProjectCollectionFactory(
            project=self.classification_project
        )
        self.collection = self.classification_project_collection.collection.collection
        self.collection.status = CollectionStatus.PRIVATE
        self.collection.save()

        self.resources = []
        # Create 10 resources with sequential date_recorded (default)
        for i in range(10):
            resource = ResourceFactory(owner=self.user, deployment=self.deployment)
            self.resources.append(resource)

        # Create 10 resources with identical date_recorded
        for i in range(10):
            resource = ResourceFactory(
                owner=self.user,
                deployment=self.deployment,
                date_recorded=self.resources[0].date_recorded,
            )
            self.resources.append(resource)

        self.collection.resources.set(self.resources)

        # create approved AI classifications for all generated classifications
        for classification in Classification.objects.filter(
                resource__in=self.resources
        ).all():
            ai_classification = AIClassificationFactory.create(
                classification=classification, owner=self.user
            )
            classification.approved_source_ai = ai_classification
            classification.save()

    def _fetch_roll_page(self, pk: int, rev: bool = False, page_size=10):
        query_string = f"?page_size={page_size}"
        if pk:
            cursor = base64.encodebytes(f"r={int(rev)}&p={pk}".encode()).decode()
            query_string += f"&cursor={cursor}"
        url = reverse("cs_media_scroll", args=[self.classification_project.slug])
        url += query_string
        response = self.client.get(url)
        self.assertEqual(
            response.status_code,
            status.HTTP_200_OK,
            f"Failed to fetch data for pk={pk} rev={rev} page_size={page_size}",
        )

        return response.json()["results"]

    def test_cs_media_scroll_pagination(self):
        # we expect classifications to be ordered by resoure date_recorded, then by pk
        expected_classifications = list(
            Classification.objects.filter(
                project=self.classification_project,
                resource__deployment=self.deployment,
            )
            .order_by("resource__date_recorded", "pk")
            .values_list("pk", flat=True)
        )
        n = len(expected_classifications)

        self.assertEqual(n, 20)

        # page_sizes = [1, 5, 10, 15, 20]
        page_sizes = [1, 5]
        for page_size in page_sizes:
            with self.subTest(page_size=page_size):
                with self.subTest("no cursor"):
                    expected_subset = expected_classifications[:page_size]
                    classifications = self._fetch_roll_page(
                        pk=None, rev=False, page_size=page_size
                    )
                    pks = [c["pk"] for c in classifications]
                    self.assertEqual(
                        pks,
                        expected_subset,
                        f"mismatching classifications without cursor page_size={page_size}\n"
                        + f"EXP: {expected_subset}\nGOT: {pks}\nALL: {expected_classifications}",
                    )

                with self.subTest("forward"):
                    for i in range(n):
                        start_pk = expected_classifications[i]
                        expected_subset = expected_classifications[i: i + page_size]
                        classifications = self._fetch_roll_page(
                            pk=start_pk, rev=False, page_size=page_size
                        )
                        pks = [c["pk"] for c in classifications]
                        self.assertEqual(
                            pks,
                            expected_subset,
                            f"mismatching classifications for pk={start_pk} rev=False page_size={page_size}\n"
                            + f"EXP: {expected_subset}\nGOT: {pks}\nALL: {expected_classifications}",
                        )
                with self.subTest("reverse"):
                    for i in range(n):
                        start_pk = expected_classifications[i]
                        expected_subset = expected_classifications[max(0, i - page_size + 1): i + 1]
                        classifications = self._fetch_roll_page(
                            pk=start_pk, rev=True, page_size=page_size
                        )
                        pks = [c["pk"] for c in classifications]
                        self.assertEqual(
                            pks,
                            expected_subset,
                            f"mismatching classifications for pk={start_pk} rev=True page_size={page_size}\n"
                            + f"EXP: {expected_subset}\nGOT: {pks}\nALL: {expected_classifications}",
                        )


class CSMediaUploadTests(APITransactionTestCase):
    def setUp(self):
        self.user = UserFactory()
        self.research_project = ResearchProjectFactory(owner=self.user)
        self.timezone = pytz.timezone("Europe/Warsaw")
        self.location = LocationFactory(
            owner=self.user,
            research_project=self.research_project,
            timezone="Europe/Warsaw",
        )

        self.deployment = DeploymentFactory(
            owner=self.user,
            research_project=self.research_project,
            location=self.location,
        )
        self.collection1 = CollectionFactory(owner=self.user)
        self.collection2 = CollectionFactory(owner=self.user)
        self.classificator = ClassificatorFactory(owner=self.user)
        self.classification_project = ClassificationProjectFactory(
            research_project=self.research_project,
            owner=self.user,
            classificator=self.classificator,
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC,
        )
        self.research_project.collections.set([self.collection1, self.collection2])
        rp_collection1 = ResearchProjectCollection.objects.get(
            project=self.classification_project.research_project.pk,
            collection=self.collection1.pk,
        )
        ClassificationProjectCollectionFactory(
            project=self.classification_project, collection=rp_collection1
        )
        rp_collection2 = ResearchProjectCollection.objects.get(
            project=self.classification_project.research_project.pk,
            collection=self.collection2.pk,
        )
        ClassificationProjectCollectionFactory(
            project=self.classification_project, collection=rp_collection2
        )

        self.client.force_authenticate(self.user)

    def test_upload_media_with_the_same_name(self):
        """
        Try adding two media files with the same name to a deployment.
        Both should be added.
        """
        file1 = SimpleUploadedFile("test1.jpg", b"abcde", content_type="image/jpeg")
        file2 = SimpleUploadedFile("test1.jpg", b"fghij", content_type="image/jpeg")

        file_metadata = {
            "test1": {
                "name": "test1",
                "last_modified_date": "2021-01-01",
            }
        }

        response = self.client.post(
            reverse("cs_upload_media", args=(self.classification_project.slug,)),
            {
                "collection": self.collection1.pk,
                "metadata": json.dumps(file_metadata),
                "file": [file1, file2],
                "deployment": self.deployment.pk,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(self.collection1.resources.count(), 2)
        self.assertEqual(
            self.collection1.resources.values_list("name", flat=True)[::1],
            ["test1", "test1"],
        )

    def test_upload_duplicated_media(self):
        """
        Try uploading two duplicated pictures into collection.
        The files have different names but the same content. Only one file should be added.
        """
        file1 = SimpleUploadedFile("test1.jpg", b"klmno", content_type="image/jpeg")
        file2 = SimpleUploadedFile("test2.jpg", b"klmno", content_type="image/jpeg")

        file_metadata = {
            "test1": {
                "name": "test1",
                "last_modified_date": "2021-01-01",
            },
            "test2": {
                "name": "test2",
                "last_modified_date": "2021-01-01",
            },
        }
        response = self.client.post(
            reverse("cs_upload_media", args=(self.classification_project.slug,)),
            {
                "collection": self.collection2.pk,
                "metadata": json.dumps(file_metadata),
                "file": [file1, file2],
                "deployment": self.deployment.pk,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(self.collection2.resources.count(), 1)
        self.assertEqual(
            self.collection2.resources.values_list("name", flat=True)[::1], ["test1"]
        )
