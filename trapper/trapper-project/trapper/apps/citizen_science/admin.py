from django.contrib import admin
from solo.admin import SingletonModelAdmin

from trapper.apps.citizen_science.models import CitizenScienceConfiguration

admin.site.register(CitizenScienceConfiguration, SingletonModelAdmin)
