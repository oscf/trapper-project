# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-12-22 17:56
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("storage", "0002_resource_deployment"),
    ]

    operations = [
        migrations.RunSQL(
            "CREATE INDEX idx_column ON storage_resource (date_recorded)"
        ),
        # this format should also work for mysql
    ]
