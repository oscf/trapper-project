# Generated by Django 4.2.4 on 2024-01-05 16:53

from django.db import migrations, models
import trapper.apps.storage.models


class Migration(migrations.Migration):

    dependencies = [
        ('storage', '0027_merge_20231206_1036'),
    ]

    operations = [
        migrations.AlterField(
            model_name='resource',
            name='file_thumbnail',
            field=models.ImageField(blank=True, db_index=True, editable=False, null=True, upload_to=trapper.apps.storage.models.THUMBNAIL_DIR_F, verbose_name='File thumbnail'),
        ),
    ]
