from storages.backends.azure_storage import AzureStorage
from storages.backends.s3boto3 import S3Boto3Storage
from django.utils.functional import LazyObject
from django.conf import settings
from django.core.files.storage import FileSystemStorage


class AzureMediaStorage(AzureStorage):
    location = getattr(settings, "MEDIA_LOCATION", "media")
    file_overwrite = False
    expiration_secs = 60 * 60 * 24  # 1 day


class S3MediaStorage(S3Boto3Storage):
    location = getattr(settings, "MEDIA_LOCATION", "media")
    file_overwrite = False
    expiration_secs = 60 * 60 * 24  # 1 day


class OVHMediaStorage(S3Boto3Storage):
    location = getattr(settings, "MEDIA_LOCATION", "media")
    file_overwrite = False
    expiration_secs = 60 * 60 * 24  # 1 day
    custom_domain = None


class TrapperMediaStorage(LazyObject):
    def _setup(self):
        storage_engine = settings.DEFAULT_FILE_STORAGE_ENGINE
        if storage_engine == "azure":
            self._wrapped = AzureMediaStorage()
        elif storage_engine == "amazon_s3":
            self._wrapped = S3MediaStorage()
        elif storage_engine == "ovh":
            self._wrapped = OVHMediaStorage()
        else:
            self._wrapped = FileSystemStorage(
                location=settings.MEDIA_ROOT, base_url=settings.MEDIA_URL
            )
