# -*- coding: utf-8 -*-
"""
Simple admin interface to control changes in models.
"""
from django.contrib.gis import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from trapper.apps.geomap.models import Location, Deployment
from trapper.apps.storage.models import Collection
from trapper.apps.citizen_science.tasks import complete_collection_upload


class LocationAdmin(admin.ModelAdmin):
    list_display = (
        "location_id",
        "name",
        "coordinates",
        "timezone",
        "ignore_DST",
        "city",
        "county",
        "state",
        "country",
        "is_public",
        "date_created",
        "owner",
        "link",
    )
    search_fields = ("location_id", "name", "description", "owner__username")
    list_filter = ("research_project", "is_public", "date_created", "owner")
    raw_id_fields = ("owner", "managers")

    def link(self, item):
        url = "{url}?locations={pk}".format(url=reverse("geomap:map_view"), pk=item.pk)
        return mark_safe('<a href="{url}" target="_blank">Link</a>'.format(url=url))

    link.short_description = _("Show on map")


class DeploymentAdmin(admin.ModelAdmin):
    list_display = (
        "deployment_id",
        "deployment_code",
        "location",
        "research_project",
        "start_date",
        "end_date",
        "date_created",
        "view_quality",
        "correct_setup",
        "correct_tstamp",
    )
    search_fields = ("deployment_id", "owner__username")
    list_filter = (
        "research_project",
        "date_created",
        "view_quality",
        "correct_setup",
        "correct_tstamp",
        "owner",
    )
    raw_id_fields = ("owner", "managers", "location")
    actions = ["rerun_cs_pipeline"]

    @admin.action(description="Rerun CS pipeline")
    def rerun_cs_pipeline(self, request, queryset):
        for depl in queryset:
            owner = depl.owner
            collection = Collection.objects.get(
                cs_deployment=depl,
                owner=owner,
            )
            research_project_collection = (
                collection.researchprojectcollection_set.first()
            )
            classification_project_collection = (
                research_project_collection.classificationprojectcollection_set.first()
            )
            classification_project = classification_project_collection.project

            complete_collection_upload.delay(
                classification_project.pk,
                owner.pk,
                collection.pk,
            )
        self.message_user(request, "Correct rerun for selected deployments.")

    def get_queryset(self, request):
        return (
            super(DeploymentAdmin, self)
            .get_queryset(request)
            .prefetch_related("location", "research_project")
        )


admin.site.register(Location, LocationAdmin)
admin.site.register(Deployment, DeploymentAdmin)
