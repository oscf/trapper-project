import datetime
import json

import factory
import mock
import pytz
from django.contrib.auth.models import AnonymousUser
from django.contrib.gis.geos import Point
from django.urls import reverse
from django.utils import timezone
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.common.utils.test_tools import (
    ExtendedTestCase,
    LocationTestMixin,
)
from trapper.apps.geomap.models import Location
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import ResourceStatus, CollectionStatus
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class BaseGeomapTestCase(ExtendedTestCase, LocationTestMixin):
    def setUp(self):
        super().setUp()
        self.summon_alice()
        self.summon_ziutek()


class AnonymousLocationTestCase(BaseGeomapTestCase):
    """Location logic for anonymous users"""

    # TODO: Fails before updating packages
    # def test_location_list(self):
    #     """Anonymous user can access to location list"""
    #     url = reverse('geomap:location_list')
    #     self.assert_access_granted(url)

    # TODO: Error before updating packages
    # def test_location_json(self):
    #     """Anonymous user can see only PUBLIC locations"""
    #     name_public = 'ID_PUBLIC'
    #     name_private = 'ID_PRIVATE'
    #
    #     location_public = self.create_location(
    #         owner=self.alice, name=name_public, location_id=name_public,
    #         is_public=True
    #     )
    #     location_private = self.create_location(
    #         owner=self.alice, name=name_private, location_id=name_private,
    #         is_public=False
    #     )
    #
    #     url = reverse('geomap:api-location-list')
    #     response = self.client.get(url)
    #     content = json.loads(response.content)['results']
    #
    #     location_pk_list = [item['pk'] for item in content]
    #
    #     self.assertIn(location_public.pk, location_pk_list, 'Public not shown')
    #     self.assertNotIn(
    #         location_private.pk, location_pk_list, 'Private shown'
    #     )

    def test_location_delete(self):
        """Anonymous user should not be able to access this."""
        url = reverse("geomap:location_delete", kwargs={"pk": 1})
        self.assert_forbidden(url=url, method="get")
        self.assert_forbidden(url=url, method="post")


class LocationTestCase(BaseGeomapTestCase):
    """Location permission logic for authenticated users"""

    def setUp(self):
        """Login alice by default for all tests"""
        super().setUp()
        self.login_alice()

    def test_location_list(self):
        """Authenticated user can access to location list"""
        url = reverse("geomap:location_list")
        self.assert_access_granted(url)

    def test_location_json(self):
        """Authenticated user can see own private resources locations"""
        name_public = "ID_PUBLIC"
        name_private = "ID_PRIVATE"
        name_private2 = "ID_PRIVATE_2"

        location_public = self.create_location(
            owner=self.alice, name=name_public, location_id=name_public, is_public=True
        )
        location_private = self.create_location(
            owner=self.alice,
            name=name_private,
            location_id=name_private,
            is_public=False,
        )
        location_private2 = self.create_location(
            owner=self.ziutek,
            name=name_private2,
            location_id=name_private2,
            is_public=False,
        )

        url = reverse("geomap:api-location-list")
        response = self.client.get(url)
        content = json.loads(response.content)["results"]

        location_pk_list = [item["pk"] for item in content]

        self.assertIn(location_public.pk, location_pk_list, "Public not shown")
        self.assertIn(location_private.pk, location_pk_list, "Private not shown")
        self.assertNotIn(location_private2.pk, location_pk_list, "Private2 shown")

    def test_location_delete_owner(self):
        """Owner can delete own location. Public status doesn't matter."""
        location = self.create_location(
            owner=self.alice, name="ID_1", location_id="ID_1"
        )
        url = reverse("geomap:location_delete", kwargs={"pk": location.pk})
        redirection = reverse("geomap:location_list")
        self.assert_redirect(url, redirection=redirection)

        self.assertFalse(Location.objects.filter(pk=location.pk).exists())

    def test_location_delete_other(self):
        """Authenticated user cannot delete someone else location. Public
        status doesn't matter"""
        location = self.create_location(
            owner=self.ziutek, name="ID_2", location_id="ID_2"
        )
        url = reverse("geomap:location_delete", kwargs={"pk": location.pk})
        self.assert_forbidden(url)
        self.assertTrue(Location.objects.filter(pk=location.pk).exists())

    # TODO: Fails before updating packages
    # def test_location_delete_multiple(self):
    #     """Owner can delete own location using multiple delete.
    #     Public status doesn't matter.
    #     """
    #     location_owner = self.create_location(
    #         owner=self.alice, name='ID_1', location_id='ID_1'
    #     )
    #     location_other = self.create_location(
    #         owner=self.ziutek, name='ID_2', location_id='ID_2'
    #     )
    #
    #     params = {
    #         'pks': ",".join([
    #             str(location_owner.pk),
    #             str(location_other.pk),
    #             str(self.TEST_PK)
    #         ]),
    #     }
    #     url = reverse('geomap:location_delete_multiple')
    #     response = self.assert_access_granted(url, method='post', data=params)
    #     status = self.assert_json_context_variable(response, 'status')
    #     self.assertTrue(status)
    #
    #     self.assert_invalid_pk(response)
    #     self.assertFalse(
    #         Location.objects.filter(pk=location_owner.pk).exists()
    #     )
    #     self.assertTrue(
    #         Location.objects.filter(pk=location_other.pk).exists()
    #     )


def get_current_user_mock():
    return UserFactory()


def get_current_user_anon_mock():
    return AnonymousUser()


class LocationTest(BaseGeomapTestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        cls.user1 = UserFactory()
        cls.user2 = UserFactory()
        cls.user3 = UserFactory()
        cls.user4 = UserFactory()
        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        cls.location1 = LocationFactory(
            owner=cls.user1, research_project=cls.research_project
        )
        cls.deployment = DeploymentFactory(
            owner=cls.user1,
            location=cls.location1,
            research_project=cls.research_project,
        )
        ResourceFactory.create_batch(
            2, owner=cls.user1, deployment=cls.deployment, status=ResourceStatus.PRIVATE
        )
        cls.collection = CollectionFactory(
            owner=cls.user1, status=CollectionStatus.PRIVATE
        )
        cls.collection.resources.set(Resource.objects.all())
        cls.collection.managers.set([cls.user3])
        cls.classificator = ClassificatorFactory(owner=cls.user1)
        cls.research_project.collections.set([cls.collection])

    def test_owner_property(self):
        self.assertTrue(self.location1.owner_name)

    @mock.patch(
        "trapper.apps.geomap.models.get_current_user", side_effect=get_current_user_mock
    )
    def test_manager_get_available(self, get_current_user_mock):
        self.assertEqual(len(Location.objects.get_available()), 0)

        queryset = Location.objects.all()
        self.assertEqual(len(Location.objects.get_available(base_queryset=queryset)), 0)

    @mock.patch(
        "trapper.apps.geomap.models.get_current_user",
        side_effect=get_current_user_anon_mock,
    )
    def test_manager_get_available_anon(self, get_current_user_mock):
        self.assertEqual(len(Location.objects.get_available()), 0)

    # TODO: Uncomment if detail route is created
    # def test_manager_api_detail_context(self):
    #     route = reverse('geomap:location_detail', kwargs={'pk': self.location1.pk})
    #     self.assertEqual(Location.objects.api_detail_context(self.location1, self.user1), route)

    def test_location_list_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("geomap:location_list"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Locations")

    def test_location_bulk_update_route(self):
        self.client.force_login(self.user1)
        response = self.client.get(reverse("geomap:location_bulk_update"))
        self.assertEqual(response.status_code, 200)

    def test_location_delete_route(self):
        self.client.force_login(self.user1)
        location1 = LocationFactory(
            owner=self.user1, research_project=self.research_project
        )
        self.assertTrue(Location.objects.filter(id=location1.pk).exists())
        response = self.client.get(
            reverse("geomap:location_delete", kwargs={"pk": location1.pk})
        )
        self.assertEqual(response.status_code, 302)
        self.assertFalse(Location.objects.filter(id=location1.pk).exists())

    def test_location_create_route(self):
        self.client.force_login(self.user1)
        self.assertEqual(Location.objects.all().count(), 1)
        new_location = {
            "location_id": factory.Sequence(lambda n: str(n)),
            "name": factory.lazy_attribute(lambda a: f"location_{a.location_id}"),
            "now": factory.LazyFunction(timezone.now),
            "date_created": factory.LazyAttribute(
                lambda o: o.now - datetime.timedelta(days=10)
            ),
            "description": factory.Faker("paragraph", nb_sentences=3),
            "is_public": False,
            "point": factory.Faker("latlng"),
            "coordinates": factory.LazyAttribute(
                lambda o: Point([float(cord) for cord in o.point])
            ),
            "timezone": timezone.get_default_timezone_name(),
            "owner": factory.SubFactory(UserFactory),
            "country": factory.Faker("country"),
            "state": factory.Faker("word"),
            "county": factory.Faker("word"),
            "city": factory.Faker("city"),
            "research_project": factory.SubFactory(ResearchProjectFactory),
        }

        response = self.client.get(reverse("geomap:location_create"), new_location)
        self.assertEqual(response.status_code, 302)

    def test_location_import(self):
        self.client.force_login(self.user1)
        new_location_import = {
            "research_project": self.research_project.pk,
            "timezone": pytz.UTC,
            "update": False,
        }
        response = self.client.get(reverse("geomap:location_list"))
        self.assertEqual(response.status_code, 200)
        self.client.post(reverse("geomap:location_list"), new_location_import)

    def test_location_create_form(self):
        response = self.client.get(reverse("geomap:locations_createform"), follow=True)
        self.assertEqual(response.status_code, 200)

    # def test_deployment_update_route(self):
    #     self.client.force_login(self.user1)
    #     deployment1 = DeploymentFactory(deployment_code='test',
    #                                     owner=self.user1,
    #                                     research_project=self.research_project,
    #                                     location=self.location1)
    #     self.assertEqual(deployment1.deployment_code, 'test')
    #     response = self.client.get(reverse('geomap:deployment_update', kwargs={'pk': deployment1.pk}))
    #     self.assertEqual(response.status_code, 200)
    #     self.assertEqual(deployment1.deployment_code, 'test')

    def test_location_api_update_context(self):
        Location.objects.api_update_context(self.location1, self.user1)
        Location.objects.api_update_context(self.location1, self.user2)
        Location.objects.api_update_context(self.location1, self.user3)

    def test_location_api_delete_context(self):
        Location.objects.api_delete_context(self.location1, self.user1)
        Location.objects.api_delete_context(self.location1, self.user2)
        Location.objects.api_delete_context(self.location1, self.user3)

    def test_location_can_view(self):
        self.location1.can_view(user=self.user1)
        self.location1.can_view(user=self.user2)
        self.location1.can_view(user=self.user3)
