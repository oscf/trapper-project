from unittest.mock import patch

import pytz
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.geomap.forms import LocationFilterForm, LocationImportForm
from trapper.apps.research.models import ResearchProject
from trapper.apps.research.taxonomy import ResearchProjectRoleType
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.research.tests.factories.research_project_role import (
    ResearchProjectRoleFactory,
)


class TestForms(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TestForms, cls).setUpTestData()
        cls.user1 = UserFactory()
        cls.user2 = UserFactory()

        cls.research_project = ResearchProjectFactory(owner=cls.user1)
        ResearchProjectRoleFactory(
            user=cls.user1,
            name=ResearchProjectRoleType.ADMIN,
            project=cls.research_project,
        )

    @patch("trapper.apps.research.models.ResearchProject.objects.get_accessible")
    def test_form(self, mock_get_accessible):
        self.client.force_login(self.user1)
        mock_get_accessible.return_value = ResearchProject.objects.all()

        form = LocationFilterForm(
            data={
                "location_id": "10",
                "name": "TestName",
                "description": "TestDescription",
                "coordinates": "",
                "timezone": timezone.now(),
                "managers": [],
                "country": "",
                "state": "",
                "county": "",
                "city": "",
                "research_project": self.research_project,
            }
        )

    def test_map_list(self):
        response = self.client.get(reverse("geomap:map_list"), follow=True)
        self.assertEqual(response.status_code, 200)

    def test_map_permissions(self):
        response = self.client.get(
            reverse("geomap:map_permissions", kwargs={"map_id": 0}), follow=True
        )
        self.assertEqual(response.status_code, 200)

    def test_map(self):
        response = self.client.get(
            reverse("geomap:map", kwargs={"pk": 0, "slug": "test"}), follow=True
        )
        self.assertEqual(response.status_code, 200)

    def test_map_delete(self):
        response = self.client.get(reverse("geomap:map_delete", kwargs={"pk": 0}))
        self.assertEqual(response.status_code, 403)

    @patch("trapper.apps.research.models.ResearchProject.objects.get_accessible")
    def test_location_import_form(self, mock_get_accessible):
        mock_get_accessible.return_value = ResearchProject.objects.all()

        location_import_form = LocationImportForm(
            data={
                "research_project": self.research_project,
                "timezone": pytz.UTC,
            }
        )

        location_import_form.is_valid()
        location_import_form.clean()
        location_import_form.clean_gpx_file()
        location_import_form.clean_csv_file()
