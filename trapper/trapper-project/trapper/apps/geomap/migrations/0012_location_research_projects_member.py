# Generated by Django 3.2.5 on 2022-08-01 19:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('research', '0007_alter_researchproject_status'),
        ('geomap', '0011_auto_20220107_1559'),
    ]

    operations = [
        migrations.AddField(
            model_name='location',
            name='research_projects_member',
            field=models.ManyToManyField(blank=True, related_name='shared_locations', to='research.ResearchProject'),
        ),
    ]
