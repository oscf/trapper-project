from django.urls import path

from .views_rest import (
    CustomLoginView,
    CustomLogoutView,
    RegisterView,
    ChangePasswordView,
    ProfileView,
    SetDefaultCSProjectView,
    ActivateAccountView,
    RequestPasswordResetView,
    PasswordResetView,
    UserProfileView,
    ResendAccountActivationView,
    UserAvatarView,
)

urlpatterns = [
    path("login/", CustomLoginView.as_view(), name="auth_login"),
    path("logout/", CustomLogoutView.as_view(), name="auth_logout"),
    path("register/", RegisterView.as_view(), name="auth_register"),
    path(
        "activate/<uidb64>/<token>/",
        ActivateAccountView.as_view(),
        name="activate_account",
    ),
    path(
        "resend-activation/<uidb64>/",
        ResendAccountActivationView.as_view(),
        name="resend_account_activation",
    ),
    path(
        "request-password-reset/",
        RequestPasswordResetView.as_view(),
        name="request_password_reset",
    ),
    path(
        "password-reset/<uidb64>/<token>/",
        PasswordResetView.as_view(),
        name="api_password_reset",
    ),
    path("profile/", ProfileView.as_view(), name="profile"),
    path("change-password/", ChangePasswordView.as_view(), name="change_password"),
    path(
        "set-default-cs-project/",
        SetDefaultCSProjectView.as_view(),
        name="set_default_cs_project",
    ),
    path("profile/<int:user_pk>/", UserProfileView.as_view(), name="show_user_profile"),
    path("avatar/<int:user_pk>/", UserAvatarView.as_view(), name="cs_user_avatar"),
]
