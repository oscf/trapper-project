import os
import shutil
from pathlib import Path

from django.conf import settings
from django.db import migrations


def move_packages_to_external_media(apps, schema_editor):
    media_url = settings.MEDIA_ROOT
    ext_media_url = settings.EXTERNAL_MEDIA_ROOT

    for location in os.walk(media_url):
        if location[0].endswith("data_packages"):
            root_url = Path(location[0])
            target_url = Path(location[0].replace(media_url, ext_media_url))
            packages = os.listdir(location[0])
            for package in packages:
                path = root_url.joinpath(package)
                target_path = target_url.joinpath(package)
                shutil.move(path, target_path)


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0015_alter_userdatapackage_package"),
    ]

    operations = [
        migrations.RunPython(move_packages_to_external_media)
    ]
