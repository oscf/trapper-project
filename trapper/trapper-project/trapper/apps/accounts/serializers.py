# -*- coding: utf-8 -*-
from typing import Any, Dict
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.tokens import default_token_generator
from django.utils import timezone
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import gettext_lazy as _, get_language
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from trapper.apps.accounts.tasks import (
    celery_send_registration_emails,
    celery_send_password_reset_emails,
    celery_resend_activation_email,
)
from trapper.apps.accounts.utils import get_pretty_username
from trapper.apps.accounts.validators import phone_regex, ImageValidator
from trapper.apps.common.serializers import BasePKSerializer, MixinErrorSerializer
from trapper.apps.common.tools import non_field_errors
from trapper.apps.media_classification.models import ClassificationProject
from django.contrib.auth import authenticate, login, logout

User = get_user_model()


class UserSerializer(BasePKSerializer):
    class Meta:
        model = User
        fields = ("pk", "username", "first_name", "last_name", "email", "name")

    name = serializers.SerializerMethodField()

    def get_name(self, item):
        """Custom method for retrieving prettified username"""
        return get_pretty_username(user=item)


class CSUserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    avatar = serializers.CharField(source="userprofile.cs_avatar_url")

    class Meta:
        model = User
        fields = (
            "id",
            "name",
            "avatar",
        )

    @staticmethod
    def get_name(obj):
        return obj.username


class RegisterSerializer(MixinErrorSerializer, serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True, validators=[UniqueValidator(queryset=User.objects.all())]
    )

    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )
    password2 = serializers.CharField(write_only=True, required=True)

    phone_number = serializers.CharField(
        validators=[phone_regex], source="userprofile.phone_number", allow_blank=True
    )
    institution = serializers.CharField(
        source="userprofile.institution", allow_blank=True
    )
    about_me = serializers.CharField(source="userprofile.about_me", allow_blank=True)
    gdpr = serializers.BooleanField(source="userprofile.gdpr")
    tos = serializers.BooleanField(source="userprofile.tos")

    class Meta:
        model = User
        fields = (
            "email",
            "username",
            "first_name",
            "last_name",
            "password",
            "password2",
            "phone_number",
            "institution",
            "about_me",
            "gdpr",
            "tos",
        )
        extra_kwargs = {
            "username": {"max_length": 30},
            "first_name": {"required": True, "allow_blank": False},
            "last_name": {"required": True, "allow_blank": False},
        }

    def validate(self, attrs):
        if attrs["password"] != attrs["password2"]:
            self._add_error("password", _("Password fields didn't match."))
        for field in ["gdpr", "tos"]:
            if not attrs["userprofile"].get(field):
                self._add_error(field, _("Consent is required."))
        return attrs

    def create(self, validated_data):
        user = User.objects.create(
            email=validated_data["email"],
            username=validated_data["username"],
            first_name=validated_data["first_name"],
            last_name=validated_data["last_name"],
            is_citizen_science=True,
            is_active=False,
        )

        user.set_password(validated_data["password"])
        user.save()
        for field in ["phone_number", "institution", "gdpr", "tos", "about_me"]:
            setattr(
                user.userprofile, field, validated_data["userprofile"].get(field, "")
            )
        # Consent fields
        for consent in ["gdpr", "tos"]:
            if validated_data["userprofile"].get(consent, ""):
                setattr(user.userprofile, f"{consent}_accepted_date", timezone.now())
        user.userprofile.save()
        if settings.CELERY_ENABLED:
            celery_send_registration_emails.delay(user.pk, get_language())
        else:
            celery_send_registration_emails(user.pk, get_language())
        return user


class RequestPasswordResetSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("email",)
        extra_kwargs = {
            "email": {"required": True, "allow_blank": False, "write_only": True}
        }

    def create(self, validated_data):
        try:
            user = User.objects.get(email=validated_data["email"])
            if settings.CELERY_ENABLED:
                celery_send_password_reset_emails.delay(user.pk, get_language())
            else:
                celery_send_password_reset_emails(user.pk, get_language())
        except User.DoesNotExist:
            pass
        return {}


class PasswordResetSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )
    new_password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = ("new_password", "new_password2")

    def validate(self, attrs):
        if attrs["new_password"] != attrs["new_password2"]:
            raise serializers.ValidationError(
                {"new_password": _("Password fields didn't match.")}
            )
        return attrs

    def create(self, validated_data):
        data = self.context["request"].parser_context["kwargs"]
        try:
            uid = urlsafe_base64_decode(data["uidb64"]).decode()
            user = User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        if user is not None and default_token_generator.check_token(
            user, data["token"]
        ):
            user.set_password(validated_data["new_password"])
            user.save()
            return {}
        else:
            raise non_field_errors(_("Invalid reset link."))


class ActivateAccountSerializer(serializers.Serializer):
    def get_user(self):
        data = self.context["request"].parser_context["kwargs"]
        try:
            uid = urlsafe_base64_decode(data["uidb64"]).decode()
            return User.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            raise serializers.ValidationError(
                _("Unable to activate an account. Please contact with administrators.")
            )

    def validate(self, attrs):
        user = self.get_user()
        if user.is_active:
            raise serializers.ValidationError(_("Your account is already active."))
        return attrs

    def create(self, validated_data):
        data = self.context["request"].parser_context["kwargs"]
        user = self.get_user()
        if user is not None and default_token_generator.check_token(
            user, data["token"]
        ):
            user.is_active = True
            user.save()
            return {}
        else:
            raise non_field_errors(_("Invalid activation link."))


class ResendAccountActivationSerializer(ActivateAccountSerializer):
    def create(self, validated_data):
        user = self.get_user()
        if settings.CELERY_ENABLED:
            celery_resend_activation_email.delay(user.pk, get_language())
        else:
            celery_resend_activation_email(user.pk, get_language())
        return {}


class ProfileSerializer(serializers.ModelSerializer):
    phone_number = serializers.CharField(
        validators=[phone_regex], source="userprofile.phone_number", allow_blank=True
    )
    avatar = serializers.ImageField(
        validators=[
            ImageValidator(size=1 * 1024 * 1024, min_width=250, min_height=250)
        ],
        source="userprofile.avatar",
        allow_null=True,
    )
    institution = serializers.CharField(
        source="userprofile.institution", allow_blank=True
    )
    about_me = serializers.CharField(source="userprofile.about_me", allow_blank=True)
    gdpr = serializers.BooleanField(source="userprofile.gdpr")
    gdpr_accepted_date = serializers.ReadOnlyField(
        source="userprofile.gdpr_accepted_date"
    )
    tos = serializers.BooleanField(source="userprofile.tos")
    tos_accepted_date = serializers.ReadOnlyField(
        source="userprofile.tos_accepted_date"
    )
    default_cs_project = serializers.SlugRelatedField(
        source="userprofile.default_cs_project",
        slug_field="slug",
        read_only=True,
    )

    class Meta:
        model = User
        fields = (
            "username",
            "first_name",
            "last_name",
            "phone_number",
            "avatar",
            "about_me",
            "institution",
            "email",
            "gdpr",
            "gdpr_accepted_date",
            "tos",
            "tos_accepted_date",
            "default_cs_project",
        )
        extra_kwargs = {
            "username": {"max_length": 30},
            "email": {"read_only": True},
            "first_name": {"allow_blank": False},
            "last_name": {"allow_blank": False},
        }

    def to_representation(self, instance):
        data = super().to_representation(instance)
        # Return correct avatar url
        data["avatar"] = instance.userprofile.cs_avatar_url
        return data

    def validate(self, attrs):
        for consent in ["gdpr", "tos"]:
            if (
                attrs.get("userprofile")
                and consent in attrs["userprofile"].keys()
                and not attrs["userprofile"][consent]
            ):
                raise serializers.ValidationError(
                    {consent: _("You can only accept consent.")}
                )
        return attrs

    def update(self, instance, validated_data):
        user_profile = validated_data.pop("userprofile", None)
        if user_profile:
            for key, value in user_profile.items():
                setattr(instance.userprofile, key, value)
            # Consent fields
            for consent in ["gdpr", "tos"]:
                if user_profile.get(consent, ""):
                    setattr(
                        instance.userprofile, f"{consent}_accepted_date", timezone.now()
                    )
            instance.userprofile.save()
        return super().update(instance, validated_data)


class UserProfileSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    avatar = serializers.ReadOnlyField(source="userprofile.cs_avatar_url")
    classification_projects = serializers.SerializerMethodField()
    institution = serializers.ReadOnlyField(source="userprofile.institution")
    about_me = serializers.ReadOnlyField(source="userprofile.about_me")

    class Meta:
        model = User
        fields = (
            "pk",
            "name",
            "first_name",
            "last_name",
            "avatar",
            "username",
            "institution",
            "about_me",
            "classification_projects",
        )

    @staticmethod
    def get_name(obj):
        return obj.get_full_name() or obj.username

    def get_classification_projects(self, obj):
        return (
            ClassificationProject.objects.only_cs(obj)
            .filter(collections__collection__resources__owner=obj)
            .values_list("name", flat=True)
        )


class ChangePasswordSerializer(MixinErrorSerializer, serializers.ModelSerializer):
    old_password = serializers.CharField(write_only=True, required=True)
    new_password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password]
    )
    new_password2 = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = (
            "old_password",
            "new_password",
            "new_password2",
        )

    def validate(self, attrs):
        if attrs["new_password"] != attrs["new_password2"]:
            self._add_error("new_password", _("Password fields didn't match."))
        if not self.instance.check_password(attrs["old_password"]):
            self._add_error(
                "old_password",
                _("Your old password was entered incorrectly. Please enter it again."),
            )
        return attrs

    def update(self, instance, validated_data):
        instance.set_password(validated_data["new_password"])
        instance.save(update_fields=["password"])
        return instance


class SetDefaultCSProjectSerializer(serializers.ModelSerializer):
    default_cs_project = serializers.SlugRelatedField(
        source="userprofile.default_cs_project",
        slug_field="slug",
        queryset=ClassificationProject.objects.only_cs(),
        error_messages={
            "does_not_exist": _("The selected project is invalid."),
            "incorrect_type": _("Incorrect type."),
        },
        allow_null=True,
    )

    class Meta:
        model = User
        fields = ("default_cs_project",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        default_cs_project = self.fields["default_cs_project"]
        default_cs_project.queryset = ClassificationProject.objects.only_cs(
            self.context["request"].user
        )

    def update(self, instance, validated_data):
        instance.userprofile.default_cs_project = validated_data["userprofile"][
            "default_cs_project"
        ]
        instance.userprofile.save(update_fields=["default_cs_project"])
        return instance


class CustomLoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=150)
    password = serializers.CharField(max_length=128, write_only=True)
    gdpr = serializers.BooleanField(required=False)
    tos = serializers.BooleanField(required=False)

    def validate(self, attrs: Dict[str, Any]) -> Dict[str, str]:
        credentials = {
            "username": attrs.get("username"),
            "password": attrs.get("password"),
        }

        self.user = authenticate(request=self.context.get("request"), **credentials)
        if not self.user:
            raise serializers.ValidationError(
                _("Unable to log in with provided credentials.")
            )

        if not self.user.userprofile.gdpr:
            if attrs.get("gdpr"):
                self.user.userprofile.gdpr = True
                self.user.userprofile.gdpr_accepted_date = timezone.now()
                self.user.userprofile.save(update_fields=["gdpr", "gdpr_accepted_date"])
            else:
                raise serializers.ValidationError(
                    {"gdpr": _("Please accept GDPR before login.")}
                )
        if not self.user.userprofile.tos:
            if attrs.get("tos"):
                self.user.userprofile.tos = True
                self.user.userprofile.tos_accepted_date = timezone.now()
                self.user.userprofile.save(update_fields=["tos", "tos_accepted_date"])
            else:
                raise serializers.ValidationError(
                    {"tos": _("Please accept Terms of Service before login.")}
                )

        return {"user": self.user}

    def create(self, validated_data):
        login(
            user=validated_data["user"],
            request=self.context.get("request"),
        )
        return validated_data["user"]


class CustomLogoutSerializer(serializers.Serializer):
    def create(self, validated_data):
        request = self.context.get("request")
        logout(request)
        return {}
