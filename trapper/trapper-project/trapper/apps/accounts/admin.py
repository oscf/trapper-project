# -*- coding: utf-8 -*-
"""
Simple admin interface to control changes in models.

Admin is used to activate or deactivate accounts
"""
from django.conf import settings
from django.contrib import admin, messages
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Permission, ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from trapper.apps.research.taxonomy import ResearchProjectRoleType
from trapper.apps.media_classification.taxonomy import ClassificationProjectRoleLevels
from trapper.apps.accounts.forms import (
    AdminSetUserRolesForm,
    AdminMailUsersForm,
    AdminCreateFtpUsersForm,
)
from trapper.apps.accounts.models import (
    UserProfile,
    UserTask,
    UserRemoteTask,
    UserDataPackage,
    UserFTPAccount,
)
from trapper.apps.media_classification.models import (
    ClassificationProjectRole,
    ClassificationProject,
)
from trapper.apps.research.models import ResearchProjectRole, ResearchProject

User = get_user_model()


class UserClassificationProjectFilter(admin.SimpleListFilter):
    title = _("classification project")
    parameter_name = "classification_project"

    def lookups(self, request, model_admin):
        qs = ClassificationProject.objects.all().order_by("name")
        return [(cp.id, cp.name) for cp in qs]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(
                classification_project_roles__classification_project_id=self.value()
            )
        else:
            return queryset


class UserClassificationProjectRoleFilter(admin.SimpleListFilter):
    title = _("role in classification project")
    parameter_name = "classification_project_role"

    def lookups(self, request, model_admin):
        return ClassificationProjectRoleLevels.CHOICES

    def queryset(self, request, queryset):
        query_params = request.GET.copy()
        if (
            "classification_project_role" in query_params
            and "classification_project" not in query_params
        ):
            messages.warning(
                request,
                _(
                    "To filter by classification project roles, please select a classification project first."
                ),
            )
            return queryset

        if self.value():
            cproject_id = query_params.get("classification_project")
            return queryset.filter(
                classification_project_roles__classification_project_id=cproject_id,
                classification_project_roles__name=self.value(),
            )
        else:
            return queryset


class UserResearchProjectFilter(admin.SimpleListFilter):
    title = _("research project")
    parameter_name = "research_project"

    def lookups(self, request, model_admin):
        qs = ResearchProject.objects.all().order_by("acronym")
        return [(rp.id, rp.acronym) for rp in qs]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(research_project_roles__project_id=self.value())
        else:
            return queryset


class UserResearchProjectRoleFilter(admin.SimpleListFilter):
    title = _("role in research project")
    parameter_name = "research_project_role"

    def lookups(self, request, model_admin):
        return ResearchProjectRoleType.CHOICES

    def queryset(self, request, queryset):
        query_params = request.GET.copy()
        if (
            "research_project_role" in query_params
            and "research_project" not in query_params
        ):
            messages.warning(
                request,
                _(
                    "To filter by research project roles, please select a research project first."
                ),
            )
            return queryset

        if self.value():
            rproject_id = query_params.get("research_project")
            return queryset.filter(
                research_project_roles__project_id=rproject_id,
                research_project_roles__name=self.value(),
            )
        else:
            return queryset


class UserProfileInline(admin.StackedInline):
    model = UserProfile


class TrapperUserAdmin(UserAdmin):
    list_display = (
        "username",
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "is_active",
        "is_citizen_science",
        "has_ftp",
        "gdpr_accepted",
        "tos_accepted",
    )
    readonly_fields = ("date_joined", "last_login")
    list_select_related = ("userprofile",)
    search_fields = ("username", "email", "first_name", "last_name")
    list_filter = (
        "is_staff",
        "is_active",
        "last_login",
        "userprofile__has_ftp_account",
        UserClassificationProjectFilter,
        UserClassificationProjectRoleFilter,
        UserResearchProjectFilter,
        UserResearchProjectRoleFilter,
    )
    actions = [
        "set_user_roles_action",
        "mail_users_action",
        "create_ftp_users_action",
        "delete_ftp_users_action",
    ]
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_citizen_science",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )

    inlines = [UserProfileInline]

    @admin.display(
        boolean=True,
        description=_("FTP account"),
    )
    def has_ftp(self, item):
        return item.userprofile.has_ftp_account

    @admin.display(
        boolean=True,
        description="GDPR Accepted?",
    )
    def gdpr_accepted(self, obj):
        return obj.userprofile.gdpr

    @admin.display(
        boolean=True,
        description="TOS Accepted?",
    )
    def tos_accepted(self, obj):
        return obj.userprofile.tos

    def set_user_roles_action(self, request, queryset):
        if "do_action" in request.POST:
            form = AdminSetUserRolesForm(request.POST)
            if form.is_valid():
                rprojects = form.cleaned_data["rproject"]
                rproject_role = form.cleaned_data["rproject_role"]
                cprojects = form.cleaned_data["cproject"]
                cproject_role = form.cleaned_data["cproject_role"]
                activate_all = form.cleaned_data["activate_all"]
                for user in queryset:
                    if activate_all and not user.is_active:
                        # Activate user
                        user.is_active = True
                        user.save()
                    for rp in rprojects:
                        rpr, created = ResearchProjectRole.objects.get_or_create(
                            user=user, project=rp, defaults={"name": int(rproject_role)}
                        )
                        if not created:
                            rpr.name = int(rproject_role)
                            rpr.save()
                    for cp in cprojects:
                        cpr, created = ClassificationProjectRole.objects.get_or_create(
                            user=user,
                            classification_project=cp,
                            defaults={"name": int(cproject_role)},
                        )
                        if not created:
                            cpr.name = int(cproject_role)
                            cpr.save()
                self.message_user(
                    request,
                    _("You have successfully set all the selected user roles."),
                    level=25,
                )
                return
        else:
            form = AdminSetUserRolesForm(
                initial={
                    "_selected_action": request.POST.getlist(
                        admin.helpers.ACTION_CHECKBOX_NAME
                    )
                }
            )
        return render(
            request,
            "admin/accounts/action_set_user_roles.html",
            {"title": _("Set user roles"), "objects": queryset, "form": form},
        )

    set_user_roles_action.short_description = _("Set roles for selected users")

    def create_ftp_users_action(self, request, queryset):
        """Create FTPD accounts for selected users"""
        if "do_action" in request.POST:
            form = AdminCreateFtpUsersForm(request.POST)
            if form.is_valid():
                max_nfiles = form.cleaned_data["max_nfiles"]
                max_quota = form.cleaned_data["max_quota"] * 1024**3
                queryset = queryset.exclude(userprofile__has_ftp_account=True)
                n_users = len(queryset)
                i = 0
                for user in queryset:
                    user: User
                    try:
                        p = user.password.split("$")
                        password = "$" + "$".join([p[1], p[3], p[4]])
                        username = user.username
                        ftp_data = {
                            "user": user,
                            "username": username,
                            "password": password,
                            "quota_size": max_quota,
                            "quota_files": max_nfiles,
                            "dir": f"{settings.EXTERNAL_MEDIA_ROOT}/{username}/./",
                            "uid": settings.TRAPPER_USER_UID,
                            "gid": settings.TRAPPER_USER_GID,
                        }

                        UserFTPAccount.objects.create(**ftp_data)

                        user.userprofile.has_ftp_account = True
                        user.userprofile.save()
                        i += 1
                    except Exception as e:
                        self.message_user(
                            request,
                            _(
                                f'Error when creating ftpd account for "{user.username}": {str(e)}'
                            ),
                            level=40,
                        )
                if i >= 1:
                    self.message_user(
                        request,
                        _(
                            f"You have successfully created {i}/{n_users} FTP account(s)."
                        ),
                        level=25,
                    )
                return
        else:
            form = AdminCreateFtpUsersForm(
                initial={
                    "_selected_action": request.POST.getlist(
                        admin.helpers.ACTION_CHECKBOX_NAME
                    )
                }
            )
        return render(
            request,
            "admin/accounts/action_create_ftp_users.html",
            {"title": _("Create FTP users"), "objects": queryset, "form": form},
        )

    create_ftp_users_action.short_description = _(
        "Create FTP accounts for selected users"
    )

    def delete_ftp_users_action(self, request, queryset):
        queryset = queryset.exclude(userprofile__has_ftp_account=False)
        n_users = len(queryset)
        for user in queryset:
            try:
                user.userftpaccount.delete()
            except ObjectDoesNotExist:
                pass
            user.userprofile.has_ftp_account = False
            user.userprofile.save()
        self.message_user(
            request,
            _(f"You have successfully deleted {n_users} FTP account(s)."),
            level=25,
        )
        return

    delete_ftp_users_action.short_description = _(
        "Delete FTP accounts of selected users"
    )

    def mail_users_action(self, request, queryset):
        if "do_action" in request.POST:
            form = AdminMailUsersForm(request.POST)
            if form.is_valid():
                subject = form.cleaned_data["subject"]
                text = form.cleaned_data["text"]
                for user in queryset:
                    if user.userprofile.system_notifications:
                        send_mail(
                            subject=subject,
                            message=text,
                            from_email=None,
                            recipient_list=[user.email],
                            fail_silently=True,
                        )

                self.message_user(
                    request,
                    _("You have successfully send an email to selected users."),
                    level=25,
                )
                return
        else:
            form = AdminMailUsersForm(
                initial={
                    "_selected_action": request.POST.getlist(
                        admin.helpers.ACTION_CHECKBOX_NAME
                    )
                }
            )

        return render(
            request,
            "admin/accounts/action_mail_users.html",
            {"title": _("Send an email"), "objects": queryset, "form": form},
        )

    mail_users_action.short_description = _("Send an email to selected users")


class UserDataPackageAdmin(admin.ModelAdmin):
    list_display = (
        "filename",
        "package_type",
        "user",
        "date_created",
        "uuid4",
        "description",
        "link",
    )
    search_fields = ("user__username", "package", "description")
    list_filter = ("package_type", "date_created", "user")

    def link(self, item):
        return mark_safe(
            f'<a href="{item.get_download_url()}" target="_blank">' + "Link" + "</a>"
        )

    link.short_description = _("Get file")


class UserRemoteTaskAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "user",
        "task_id",
        "status",
        "created_at",
        "updated_at",
    )
    search_fields = ("user__username", "name")
    list_filter = ("status", "created_at", "updated_at")
    raw_id_fields = ("user",)


@admin.register(UserFTPAccount)
class UserFTPAccountAdmin(admin.ModelAdmin):
    list_display = ("user", "quota_files", "quota_size", "created_at", "updated_at")
    readonly_fields = [
        "user",
        "username",
        "password",
        "uid",
        "gid",
        "dir",
        "created_at",
        "updated_at",
    ]


admin.site.register(User, TrapperUserAdmin)
admin.site.register(UserProfile)
admin.site.register(UserTask)
admin.site.register(UserRemoteTask, UserRemoteTaskAdmin)
admin.site.register(UserDataPackage, UserDataPackageAdmin)
admin.site.register(Permission)
admin.site.register(ContentType)
