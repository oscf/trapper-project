from django.urls import path

from trapper.apps.media_classification.views_rest import (
    SingleClassifyView,
    SingleClassifyVideoView,
    GroupClassifyView,
    SequencesView,
    MarkClassifyAsFavoriteView,
    FeedbackClassifyView,
    FeedbackGroupClassifyView,
    RequestNewSpeciesForClassifyView,
)

urlpatterns = [
    path(
        "<slug:cs_project_slug>/sequences/",
        SequencesView.as_view(),
        name="sequences_details",
    ),
    path(
        "<slug:cs_project_slug>/classify/",
        GroupClassifyView.as_view(),
        name="group_classify",
    ),
    path(
        "<slug:cs_project_slug>/feedback/",
        FeedbackGroupClassifyView.as_view(),
        name="feedback_group_classify",
    ),
    path(
        "<slug:cs_project_slug>/classify/<int:classification_pk>/",
        SingleClassifyView.as_view(),
        name="single_classify",
    ),
    path(
        "<slug:cs_project_slug>/classify_video/<int:classification_pk>/",
        SingleClassifyVideoView.as_view(),
        name="single_classify_video",
    ),
    path(
        "<slug:cs_project_slug>/feedback/<int:classification_pk>/",
        FeedbackClassifyView.as_view(),
        name="feedback_classify",
    ),
    path(
        "<slug:cs_project_slug>/request-new-species/<int:classification_pk>/",
        RequestNewSpeciesForClassifyView.as_view(),
        name="request_new_species_for_classify",
    ),
    path(
        "<slug:cs_project_slug>/mark-as-favorite/<int:classification_pk>/",
        MarkClassifyAsFavoriteView.as_view(),
        name="set_as_favorite",
    ),
]
