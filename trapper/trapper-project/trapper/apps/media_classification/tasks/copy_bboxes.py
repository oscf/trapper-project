from itertools import chain
from operator import itemgetter

from django.conf import settings
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from trapper.apps.media_classification.models import (
    AIClassification,
    Classification,
    ClassificationDynamicAttrs,
    ClassificationProject,
    UserClassificationDynamicAttrs,
)


class CopyBboxesFromAI:
    """
    Class that implements asynchronous task that copies bounding boxes from AIClassifications
    to approved Classifications made by human experts. It asumes that AIClassifications are
    produced by AI model able to detect objects and generate bounding boxes (as e.g. Megadetector).

    The matching rules are as follows:
    1. The total count of detected objects has to be the same for both expert's and AI classification.
    2. The observation type has to be the same for all objects of both classifications.
    3. The expert's classification has to be "unambiguous" i.e. all detected objects MUST
    have exactly the same values for all attributes provided in the "match_fields" list. The most
    trivial case is a single individual detected on an image.

    If the above conditions are met, then bounding boxes from AI classification can randomly be
    assigned to expert classification objects.
    """

    def __init__(
        self,
        user,
        project_id,
        ai_classification_pks,
        match_fields,
        overwrite_bboxes=False,
    ):
        self.user = user
        self.project = ClassificationProject.objects.get(pk=project_id)
        self.ai_classification_pks = ai_classification_pks
        self.match_fields = match_fields

        self.ai_classifications = (
            AIClassification.objects.filter(
                classification__project=self.project,
                pk__in=self.ai_classification_pks,
                has_bboxes=True,
                dynamic_attrs__observation_type__in=["animal", "human", "vehicle"],
            )
            # exclude classifications not yet approved
            .exclude(classification__status=False)
            # exclude classifications without dynamic_attrs
            .exclude(classification__dynamic_attrs__isnull=True)
            .select_related("classification", "classification__approved_source")
            .prefetch_related(
                "dynamic_attrs",
                "classification__dynamic_attrs",
                "classification__approved_source__dynamic_attrs",
            )
        )
        if not overwrite_bboxes:
            # exclude approved classifications that already have bboxes
            self.ai_classifications = self.ai_classifications.exclude(
                classification__has_bboxes=True
            )

    def is_unambiguous(self, classification):
        dynamic_attrs = classification.dynamic_attrs.all()
        # first get total count of objects
        total_count = sum([k.get("count") or 0 for k in dynamic_attrs.values()])
        # get all attributes to check
        check_values = [
            itemgetter(*self.match_fields)(k) for k in dynamic_attrs.values()
        ]
        unambiguous = len(set(check_values)) == 1
        # first value in check_values is the observation_type
        observation_type = check_values[0][0]
        return (unambiguous, total_count, observation_type, dynamic_attrs)

    def run(self):
        classifications_list = []
        dynamic_attrs_list = []
        source_dynamic_attrs_list = []
        matched_count = 0

        for ai_classification in self.ai_classifications:
            classification = ai_classification.classification
            approved_source = classification.approved_source
            added_bboxes = False

            (
                unambiguous_user,
                tcount_user,
                observation_value_user,
                dattrs_user,
            ) = self.is_unambiguous(classification)

            (
                unambiguous_ai,
                tcount_ai,
                observation_value_ai,
                dattrs_ai,
            ) = self.is_unambiguous(ai_classification)

            if (
                unambiguous_user
                and unambiguous_ai
                and (tcount_user == tcount_ai)
                and (observation_value_user == observation_value_ai)
            ):

                added_bboxes = True
                matched_count += 1

                # get and unpack bboxes into a flat list
                bboxes = dattrs_ai.values_list("bboxes", flat=True)
                bboxes = list(chain(*bboxes))

                # merge bboxes with the approved user (expert) classification
                # as both user (expert) classification and AI classification are unambiguous
                # we can randomly assign AI bboxes to user classification's dynamic attributes
                for dattr in dattrs_user:
                    dattr.bboxes = []
                    for i in range(dattr.count):
                        dattr.bboxes.append(bboxes.pop(0))
                    dynamic_attrs_list.append(dattr)

                if approved_source:
                    dattrs_source = approved_source.dynamic_attrs.all()
                    # we know that the source has the same data as approved classification
                    for i, dattr in enumerate(dattrs_user):
                        dattr_source = dattrs_source[i]
                        dattr_source.bboxes = dattr.bboxes
                        source_dynamic_attrs_list.append(dattr_source)

            if added_bboxes:
                classification.has_bboxes = True
                classification.status_ai = True
                classification.approved_ai_by = self.user
                classification.approved_ai_at = timezone.now()
                classification.approved_source_ai = ai_classification

                classifications_list.append(classification)

        if matched_count > 0:
            ClassificationDynamicAttrs.objects.bulk_update(
                dynamic_attrs_list,
                [
                    "bboxes",
                ],
                batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
            )
            Classification.objects.bulk_update(
                classifications_list,
                [
                    "has_bboxes",
                    "status_ai",
                    "approved_ai_by",
                    "approved_ai_at",
                    "approved_source_ai",
                ],
                batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
            )
            if source_dynamic_attrs_list:
                UserClassificationDynamicAttrs.objects.bulk_update(
                    source_dynamic_attrs_list,
                    [
                        "bboxes",
                    ],
                    batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
                )

        msg = _(
            f"You have successfully matched and copied bounding boxes for {matched_count} pairs of "
            f"AI & user classifications out of {len(self.ai_classifications)} initially submitted."
        )
        return msg
