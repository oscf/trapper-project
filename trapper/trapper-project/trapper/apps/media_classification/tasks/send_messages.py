from typing import List

from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

from trapper.apps.common.tools import TrapperCSMail
from trapper.apps.messaging.models import Message

User = get_user_model()


class SendMessages:
    def __init__(
        self,
        title: str,
        message: str,
        recipient_ids: List[str],
        sender_id: int,
        send_message: bool = True,
        send_email: bool = True,
    ):
        self.title = title
        self.message = message
        self.recipient_ids = recipient_ids
        self.sender_id = sender_id
        self.send_message = send_message
        self.send_email = send_email
        self.recipients = User.objects.filter(id__in=self.recipient_ids)

    def run(self):
        if self.send_message:
            self._send_message()
        if self.send_email:
            self._send_email()
        msg = _("You have successfully sent messages.")
        return msg

    def _send_message(self):
        for user in self.recipients:
            Message.objects.create(
                subject=self.title,
                text=self.message,
                user_from_id=self.sender_id,
                user_to=user,
            )

        return

    def _send_email(self):
        template_name = "common/generic_message.html"
        context = {"content": self.message}

        user_mails = list(self.recipients.values_list("email", flat=True))

        TrapperCSMail(
            template_name,
            title=self.title,
            context=context,
            user_mails=user_mails,
        ).send()

        return
