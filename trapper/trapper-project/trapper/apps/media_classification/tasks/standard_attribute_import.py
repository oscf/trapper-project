from django.conf import settings
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from trapper.apps.media_classification.models import (
    ClassificationDynamicAttrs,
    Classification,
)
from trapper.apps.media_classification.taxonomy import ClassificatorSettings


class ImportStandardAttributes:
    """
    Imports standard attributes from `self.resource.data` JSONField.
    """

    def __init__(self, user, collections):
        self.user = user
        self.collections = collections
        self.timestamp = now()

    def run(self):
        standard_attrs_S = ClassificatorSettings.STANDARD_ATTRS_STATIC
        standard_attrs_D = ClassificatorSettings.STANDARD_ATTRS_DYNAMIC
        # iterate over classification project collections
        for collection in self.collections:
            classifications = collection.classifications.all()
            classifications_to_update = []
            dattrs_to_create = []
            for classification in classifications:
                # check if data is available
                if not getattr(classification.resource, "data", None):
                    continue
                classification.updated_at = self.timestamp
                classification.updated_by = self.user
                for attr in standard_attrs_S:
                    val = classification.resource.data.get(attr, None)
                    if val:
                        setattr(classification, attr, val)
                classifications_to_update.append(classification)
                observations = classification.resource.data.get("observations", [])
                if not observations:
                    continue
                for observation in observations:
                    dattrs = ClassificationDynamicAttrs(classification=classification)
                    for attr in standard_attrs_D:
                        if attr == "species":
                            attr = "species_id"
                        val = observation.get(attr, None)
                        if val:
                            setattr(dattrs, attr, val)
                    dattrs_to_create.append(dattrs)
            # bulk update Classification objects
            Classification.objects.bulk_update(
                classifications_to_update,
                standard_attrs_S + ["updated_at", "updated_by"],
                batch_size=settings.HEAVY_BULK_UPDATE_BATCH_SIZE,
            )
            # bulk delete ClassificationDynamicAttrs
            ClassificationDynamicAttrs.objects.filter(
                classification__in=classifications_to_update
            ).delete()
            # bulk create ClassificationDynamicAttrs
            ClassificationDynamicAttrs.objects.bulk_create(
                dattrs_to_create, batch_size=settings.BULK_BATCH_SIZE
            )
            return _(
                "You have successfully imported standard attributes for "
                f"{len(classifications_to_update)} classifications."
            )
