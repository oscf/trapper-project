# Generated by Django 2.2.17 on 2021-08-05 11:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0014_userremotetask_stage"),
        ("media_classification", "0029_auto_20210804_0557"),
    ]

    operations = [
        migrations.CreateModel(
            name="AIClassificationTask",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "ai_provider",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="media_classification.AIProvider",
                    ),
                ),
                (
                    "user_remote_task",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="accounts.UserRemoteTask",
                    ),
                ),
            ],
        ),
    ]
