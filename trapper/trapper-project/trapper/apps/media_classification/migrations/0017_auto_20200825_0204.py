# Generated by Django 2.2.11 on 2020-08-25 00:04

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0016_auto_20200730_1306"),
    ]

    operations = [
        migrations.AddField(
            model_name="classification",
            name="has_bboxes",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="userclassification",
            name="has_bboxes",
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name="ailabel",
            name="age",
            field=models.CharField(
                choices=[
                    ("Undefined", "Undefined"),
                    ("Adult", "Adult"),
                    ("Subadult", "Subadult"),
                    ("Juvenile", "Juvenile"),
                    ("Offspring", "Offspring"),
                ],
                default="Undefined",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="ailabel",
            name="sex",
            field=models.CharField(
                choices=[
                    ("Undefined", "Undefined"),
                    ("Female", "Female"),
                    ("Male", "Male"),
                ],
                default="Undefined",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="classificationdynamicattrs",
            name="age",
            field=models.CharField(
                choices=[
                    ("Undefined", "Undefined"),
                    ("Adult", "Adult"),
                    ("Subadult", "Subadult"),
                    ("Juvenile", "Juvenile"),
                    ("Offspring", "Offspring"),
                ],
                default="Undefined",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="classificationdynamicattrs",
            name="behaviour",
            field=models.CharField(
                choices=[
                    ("Undefined", "Undefined"),
                    ("Grazing", "Grazing"),
                    ("Browsing", "Browsing"),
                    ("Rooting", "Rooting"),
                    ("Vigilance", "Vigilance"),
                    ("Running", "Running"),
                    ("Walking", "Walking"),
                ],
                default="Undefined",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="classificationdynamicattrs",
            name="sex",
            field=models.CharField(
                choices=[
                    ("Undefined", "Undefined"),
                    ("Female", "Female"),
                    ("Male", "Male"),
                ],
                default="Undefined",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="classificationprojectrole",
            name="user",
            field=models.ForeignKey(
                help_text="User for which the role is defined",
                on_delete=django.db.models.deletion.CASCADE,
                related_name="classification_project_roles",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AlterField(
            model_name="userclassificationdynamicattrs",
            name="age",
            field=models.CharField(
                choices=[
                    ("Undefined", "Undefined"),
                    ("Adult", "Adult"),
                    ("Subadult", "Subadult"),
                    ("Juvenile", "Juvenile"),
                    ("Offspring", "Offspring"),
                ],
                default="Undefined",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="userclassificationdynamicattrs",
            name="behaviour",
            field=models.CharField(
                choices=[
                    ("Undefined", "Undefined"),
                    ("Grazing", "Grazing"),
                    ("Browsing", "Browsing"),
                    ("Rooting", "Rooting"),
                    ("Vigilance", "Vigilance"),
                    ("Running", "Running"),
                    ("Walking", "Walking"),
                ],
                default="Undefined",
                max_length=20,
            ),
        ),
        migrations.AlterField(
            model_name="userclassificationdynamicattrs",
            name="sex",
            field=models.CharField(
                choices=[
                    ("Undefined", "Undefined"),
                    ("Female", "Female"),
                    ("Male", "Male"),
                ],
                default="Undefined",
                max_length=20,
            ),
        ),
    ]
