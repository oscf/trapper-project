# Generated by Django 4.2.4 on 2023-09-08 13:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('media_classification', '0059_remove_aiclassificationdynamicattrs_confidence'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aiprovider',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='polymorphic_%(app_label)s.%(class)s_set+', to='contenttypes.contenttype'),
        ),
    ]
