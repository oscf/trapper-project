# Generated by Django 2.2.17 on 2021-10-26 13:03

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('media_classification', '0024_classificator_is_setup'),
    ]

    operations = [
        migrations.AddField(
            model_name='classificationdynamicattrs',
            name='classification_confidence',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=3, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1)]),
        ),
        migrations.AddField(
            model_name='classificator',
            name='classification_confidence',
            field=models.BooleanField(default=False, verbose_name='Classification confidence'),
        ),
        migrations.AddField(
            model_name='userclassificationdynamicattrs',
            name='classification_confidence',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=3, null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(1)]),
        ),
    ]
