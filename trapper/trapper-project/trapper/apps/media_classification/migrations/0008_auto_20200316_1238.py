# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2020-03-16 11:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0007_auto_20200227_1542"),
    ]

    operations = [
        migrations.AlterField(
            model_name="classification",
            name="status",
            field=models.BooleanField(
                choices=[(True, "Approved"), (False, "Rejected")], default=False
            ),
        ),
        migrations.AlterField(
            model_name="classificationproject",
            name="status",
            field=models.IntegerField(
                choices=[(1, "Ongoing"), (2, "Finished")], default=1
            ),
        ),
        migrations.AlterField(
            model_name="classificationprojectrole",
            name="name",
            field=models.IntegerField(
                choices=[(1, "Admin"), (2, "Expert"), (3, "Collaborator")]
            ),
        ),
        migrations.AlterField(
            model_name="classificator",
            name="template",
            field=models.CharField(
                choices=[("inline", "Inline"), ("tab", "Tabbed")],
                default="inline",
                max_length=50,
            ),
        ),
    ]
