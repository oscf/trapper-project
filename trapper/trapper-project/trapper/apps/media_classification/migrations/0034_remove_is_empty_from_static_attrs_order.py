from django.db import migrations


def remove_is_empty_from_static_attrs_order(apps, schema_editor):
    Classificator = apps.get_model("media_classification", "Classificator")
    cls = Classificator.objects.filter(static_attrs_order__isnull=False).exclude(static_attrs_order="")
    cls = cls.filter(static_attrs_order__contains="is_empty")
    for c in cls:
        attrs_list = c.static_attrs_order.split(",")
        attrs_list.remove("is_empty")
        attrs_str = ','.join(attrs_list)
        c.static_attrs_order = attrs_str
    Classificator.objects.bulk_update(cls, ["static_attrs_order"])


class Migration(migrations.Migration):

    dependencies = [
        ("media_classification", "0033_auto_20211209_1313"),
    ]

    operations = [migrations.RunPython(remove_is_empty_from_static_attrs_order)]
