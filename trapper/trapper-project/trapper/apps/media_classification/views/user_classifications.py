# -*- coding: utf-8 -*-
"""
Views used to handle logic related to user classification management
in media classification application
"""
from braces.views import UserPassesTestMixin, JSONResponseMixin

from django.conf import settings
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.shortcuts import get_object_or_404, redirect
from django.views import generic

from trapper.apps.accounts.models import UserTask
from trapper.apps.common.tools import parse_pks
from trapper.apps.common.views import LoginRequiredMixin, BaseDeleteView
from trapper.apps.media_classification.models import (
    ClassificationProject,
    UserClassification,
    ClassificationProjectCollection,
)
from trapper.apps.media_classification.tasks import celery_approve_user_classifications
from trapper.apps.media_classification.taxonomy import ClassificationProjectRoleLevels

User = get_user_model()


class UserClassificationGridContextMixin:
    """Mixin used with views that use any classification listing for changing
    list behaviour.
    This mixin can be used to:

    * change classifiction url (i.e. add filtering)
    * if classification project is specified then add all:

        * collections assigned to this project
    """

    def get_user_classification_url(self, **kwargs):
        return reverse("media_classification:api-user-classification-list")

    def get_user_classification_context(self, **kwargs):
        project = kwargs.get("project", None)
        is_admin = project.is_project_admin(self.request.user)
        context = {
            "data_url": self.get_user_classification_url(**kwargs),
            "project": project,
            "model_name": "user classifications",
            "hide_delete": not is_admin,
            "hide_detail": True,
            "is_admin": is_admin,
        }

        if project:
            context["users"] = (
                User.objects.filter(
                    user_classifications__classification__project=project
                )
                .values_list("pk", "username")
                .order_by("username")
                .distinct()
            )
            context["collections"] = (
                ClassificationProjectCollection.objects.filter(project=project)
                .values_list("pk", "collection__collection__name")
                .order_by("collection__collection__name")
                .distinct()
            )

        return context


class UserClassificationListView(
    LoginRequiredMixin,
    UserPassesTestMixin,
    generic.ListView,
    UserClassificationGridContextMixin,
):
    """List view of the
    :class:`apps.media_classification.models.UserClassification` instances.
    UserClassifications are always limited to project that contains them.

    This view is accesible only for persons that have enough permissions
    to view user classifications.
    """

    template_name = "media_classification/user_classifications/list.html"
    raise_exception = True
    project = None

    def get_user_classification_url(self, **kwargs):
        """Alter url for user classifications DRF API, to get only user classifications
        that belong to given classification project"""
        project_pk = self.kwargs["pk"]
        url = reverse("media_classification:api-user-classification-list")
        return f"{url}?project={project_pk}"

    def get_project(self):
        """Return classification project for given pk or return HTTP 404"""
        return get_object_or_404(ClassificationProject, pk=self.kwargs["pk"])

    def test_func(self, user):
        """Check if user has enough permissions to view classifications"""
        self.project = self.get_project()
        return user.is_staff or self.project.is_project_admin(user)

    def get_context_data(self, **kwargs):
        """Update context used to render template with user classification context
        and filters"""
        context = {
            "user_classification_context": self.get_user_classification_context(
                project=self.project
            )
        }
        context["user_classification_context"]["update_redirect"] = False
        return context

    def get_queryset(self):
        pass


view_user_classification_list = UserClassificationListView.as_view()


class BulkApproveUserClassificationView(
    LoginRequiredMixin, UserPassesTestMixin, generic.View, JSONResponseMixin
):
    """
    TODO: docstrings
    """

    http_method_names = ["post"]
    raise_exception = True
    project = None

    def get_project(self):
        """Return classification project for given pk or return HTTP 404"""
        return get_object_or_404(ClassificationProject, pk=self.kwargs["pk"])

    def test_func(self, user):
        """Check if user has enough permissions to aprove user classifications"""
        self.project = self.get_project()
        return user.is_staff or self.project.is_project_admin(user)

    def post(self, request, *args, **kwargs):
        user = request.user
        data = request.POST.get("pks", None)
        if not data:
            context = {"status": False, "msg": "Invalid request"}
            return self.render_json_response(context)
        values = parse_pks(pks=data)
        user_classifications = (
            UserClassification.objects.filter(
                classification__project=self.project, pk__in=values
            )
            .select_related("classification")
            .prefetch_related("dynamic_attrs")
        )
        if user_classifications.count() == 0:
            msg = _(
                "No items to approve (most probably you have no permission to do that)."
            )
            context = {"status": False, "msg": msg}
            return self.render_json_response(context)
        params = {
            "user": self.request.user,
            "project": self.project,
            "user_classifications_pks": values,
        }
        if settings.CELERY_ENABLED:
            task = celery_approve_user_classifications.delay(**params)
            user_task = UserTask(user=user, task_id=task.task_id)
            user_task.save()
            msg = _("You have successfully run the classifications approve action!")
        else:
            msg = celery_approve_user_classifications(**params)
        return self.render_json_response({"status": True, "msg": msg})


view_user_classification_bulk_approve = BulkApproveUserClassificationView.as_view()


class UserClassificationDeleteView(BaseDeleteView):
    """
    TODO: docstrings
    """

    model = UserClassification
    model_name = "user classification"

    def get_redirect_url(self):
        return redirect(self.request.META["HTTP_REFERER"])

    def filter_editable(self, queryset, user):
        return self.model.objects.get_accessible(
            user=user,
            base_queryset=queryset,
            role_levels=[ClassificationProjectRoleLevels.ADMIN],
        )


view_user_classification_delete = UserClassificationDeleteView.as_view()
