# -*- coding: utf-8 -*-
from django.urls import reverse

from trapper.apps.common.utils.test_tools import BaseMediaClassificationTestCase
from trapper.apps.media_classification.taxonomy import (
    ClassificationProjectRoleLevels,
    ClassificationStatus,
    ClassifyMessages,
)
from trapper.apps.media_classification.models import Classification, UserClassification


class BaseClassifyTestCase(BaseMediaClassificationTestCase):
    """"""

    data = {
        "form-0-observation_type": "human",
        "form-0-species": "",
        "form-0-count": 1,
        "form-TOTAL_FORMS": 1,
        "form-INITIAL_FORMS": 0,
        "form-MIN_NUM_FORMS": 0,
        "form-MAX_NUM_FORMS": 1000,
    }


class AnonymousClassifyTestCase(BaseClassifyTestCase):
    """
    Classify process logic for anonymous users.
    """

    def test_details(self):
        """
        Anonymous user has to login to get access to the classification form.
        """
        url = reverse("media_classification:classify", kwargs={"pk": 1})
        self.assert_forbidden(url)

    def test_create(self):
        """
        Anonymous user has to login before getting access to create
        classification action.
        """
        url = reverse("media_classification:classify", kwargs={"pk": 1})
        self.assert_forbidden(url, method="post", data={})

    def test_update(self):
        """
        Anonymous user has to login before getting access to update
        classification action.
        """
        url = reverse("media_classification:classify", kwargs={"pk": 1})
        self.assert_forbidden(url, method="post", data={})

    def test_approve(self):
        """
        Anonymous user has to login before getting access to approve
        classification action.
        """
        url = reverse("media_classification:classify", kwargs={"pk": 1})
        self.assert_forbidden(url, method="post", data={"approve_classification": True})

    def test_create_multiple(self):
        """
        Anonymous user has to login before getting access to create
        multiple classifications action.
        """
        url = reverse("media_classification:classify", kwargs={"pk": 1})
        self.assert_forbidden(url, method="post", data={"classify_multiple": True})

    def test_box_approve(self):
        """
        Anonymous user has to login before getting access to approve
        classification action in the classification box.
        """
        url = reverse("media_classification:classify_approve", kwargs={"pk": 1})
        self.assert_auth_required(url, method="post", data={})


class ClassifyFormPermissionsTestCase(BaseClassifyTestCase):
    """
    Classify access permission logic for authenticated users.
    """

    url = None

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, status=True):
        super()._call_helper(owner, roles, status=status)
        self.url = reverse(
            "media_classification:classify_form",
            kwargs={
                "project_pk": self.classification_project.pk,
                "classification_pk": self.classification.pk,
            },
        )

    def test_owner(self):
        """
        Owner can see the classification form for approved classification.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_role_admin(self):
        """
        User with ADMIN role can see the classification form for approved
        classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_role_expert(self):
        """
        User with EXPERT role can see the classification form for approved
        classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role can see the classification form for approved
        classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url)

    def test_no_roles(self):
        """
        User with no roles can not see the classification form.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url)


class ClassifyCreatePermissionsTestCase(BaseClassifyTestCase):
    """
    Classify create process logic for authenticated users.
    """

    url = None

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, status=False):
        """
        Classification is not approved (status=False) and user classification
        does not exist yet.
        """
        super()._call_helper(owner, roles, status=status)
        self.url = reverse(
            "media_classification:classify_form",
            kwargs={
                "project_pk": self.classification_project.pk,
                "classification_pk": self.classification.pk,
            },
        )

    def test_owner(self):
        """
        Project owner can create classification.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url, method="post", data=self.data)
        self.assertTrue(
            UserClassification.objects.filter(
                classification=self.classification,
                dynamic_attrs__observation_type="human",
            ).exists()
        )

    def test_role_admin(self):
        """
        User with ADMIN role can create classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url, method="post", data=self.data)
        self.assertTrue(
            UserClassification.objects.filter(
                classification=self.classification,
                dynamic_attrs__observation_type="human",
            ).exists()
        )

    def test_role_expert(self):
        """
        User with EXPERT role can create classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url, method="post", data=self.data)
        self.assertTrue(
            UserClassification.objects.filter(
                classification=self.classification,
                dynamic_attrs__observation_type="human",
            ).exists()
        )

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role can create classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        self.assert_access_granted(self.url, method="post", data=self.data)
        self.assertTrue(
            UserClassification.objects.filter(
                classification=self.classification,
                dynamic_attrs__observation_type="human",
            ).exists()
        )

    def test_no_roles(self):
        """
        User with no roles can not create classification.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url, method="post", data={})


class ClassifyUpdatePermissionsTestCase(BaseClassifyTestCase):
    """
    Classify update logic for authenticated users.
    """

    url = None
    before_update = None

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, status=True):
        """
        Classification is approved (status=True) and user classification
        already exists.
        """
        super()._call_helper(owner, roles, status=status)
        self.url = reverse(
            "media_classification:classify_form",
            kwargs={
                "project_pk": self.classification_project.pk,
                "classification_pk": self.classification.pk,
                "user_pk": owner.pk,
            },
        )
        self.before_update = self.user_classification.updated_at

    def test_owner(self):
        """
        Owner can update classification.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.data)
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        qs = UserClassification.objects.filter(
            classification=self.classification,
            dynamic_attrs__observation_type="human",
            owner=owner,
        )
        self.assertTrue(qs.exists())
        self.assertNotEqual(self.before_update, getattr(qs.first(), "updated_at", None))

    def test_role_admin(self):
        """
        User with ADMIN role can update other user classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.data)
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        qs = UserClassification.objects.filter(
            classification=self.classification,
            dynamic_attrs__observation_type="human",
            owner=owner,
        )
        self.assertTrue(qs.exists())
        self.assertNotEqual(self.before_update, getattr(qs.first(), "updated_at", None))

    def test_role_expert(self):
        """
        User with EXPERT role can not update other user classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.data)
        status = self.assert_json_context_variable(response, "status")
        self.assertFalse(status)
        qs = UserClassification.objects.filter(
            classification=self.classification,
            dynamic_attrs__observation_type="animal",
            owner=owner,
        )
        self.assertTrue(qs.exists())
        self.assertEqual(self.before_update, getattr(qs.first(), "updated_at", None))

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role can not update other user classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(self.url, method="post", data=self.data)
        status = self.assert_json_context_variable(response, "status")
        self.assertFalse(status)
        qs = UserClassification.objects.filter(
            classification=self.classification,
            dynamic_attrs__observation_type="animal",
            owner=owner,
        )
        self.assertTrue(qs.exists())
        self.assertEqual(self.before_update, getattr(qs.first(), "updated_at", None))

    def test_no_roles(self):
        """User with no roles can not update other user classification"""
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url, method="post", data={})


class ClassifyApprovePermissionsTestCase(BaseClassifyTestCase):
    """
    Classify approve process logic for authenticated users.
    """

    url = None
    data_approve = None

    def setUp(self):
        """Login alice by default for all tests"""
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, status=False):
        """
        Classification is not approved yet (status=False).
        """
        super()._call_helper(owner, roles, status=status)
        self.data_approve = self.data.copy()
        self.data_approve["approve"] = True
        self.url = reverse(
            "media_classification:classify_form",
            kwargs={
                "project_pk": self.classification_project.pk,
                "classification_pk": self.classification.pk,
            },
        )

    def test_owner(self):
        """
        Project owner can approve classifications.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data=self.data_approve
        )
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        qs = Classification.objects.filter(
            resource=self.resource,
            project=self.classification_project,
            status=True,
            approved_source__owner=self.alice,
        )
        self.assertTrue(qs.exists())

    def test_role_admin(self):
        """
        User with ADMIN role can approve classifications.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data=self.data_approve
        )
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        qs = Classification.objects.filter(
            resource=self.resource,
            project=self.classification_project,
            status=True,
            approved_source__owner=self.alice,
        )
        self.assertTrue(qs.exists())

    def test_role_expert(self):
        """
        User with EXPERT role can not approve other user classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data=self.data_approve
        )
        status = self.assert_json_context_variable(response, "status")
        self.assertFalse(status)
        qs = Classification.objects.filter(
            resource=self.resource,
            project=self.classification_project,
            status=True,
            approved_source__owner=self.alice,
        )
        self.assertFalse(qs.exists())

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role can not approve other user classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data=self.data_approve
        )
        status = self.assert_json_context_variable(response, "status")
        self.assertFalse(status)
        qs = Classification.objects.filter(
            resource=self.resource,
            project=self.classification_project,
            status=True,
            approved_source__owner=self.alice,
        )
        self.assertFalse(qs.exists())

    def test_no_roles(self):
        """
        User with no roles can not see classification details.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(self.url, method="post", data=self.data_approve)


class ClassifyCreateMultiplePermissionsTestCase(BaseClassifyTestCase):
    """
    Classify multiple process permission logic for authenticated users.
    """

    resource2 = None
    data_classify_multiple = None
    url = None

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, status=False):
        super()._call_helper(owner, roles, status=status)
        self.resource2 = self.create_resource(owner=owner)
        # classification collection will be automatically updated
        self.collection.resources.add(self.resource2)
        self.data_classify_multiple = self.data.copy()
        self.data_classify_multiple.update(
            {
                "selected_resources": ",".join(
                    [str(self.resource.pk), str(self.resource2.pk)]
                )
            }
        )
        self.url = reverse(
            "media_classification:classify_form",
            kwargs={
                "project_pk": self.classification_project.pk,
                "classification_pk": self.classification.pk,
            },
        )

    def test_owner(self):
        """
        Project owner can create multiple classifications.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data=self.data_classify_multiple
        )
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        self.assertEqual(
            UserClassification.objects.filter(
                classification__resource__in=[self.resource, self.resource2],
                dynamic_attrs__observation_type="human",
                owner=self.alice,
            ).count(),
            2,
        )

    def test_role_admin(self):
        """
        User with ADMIN role can create multiple classifications.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data=self.data_classify_multiple
        )
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        self.assertEqual(
            UserClassification.objects.filter(
                classification__resource__in=[self.resource, self.resource2],
                dynamic_attrs__observation_type="human",
                owner=self.alice,
            ).count(),
            2,
        )

    def test_role_expert(self):
        """
        User with EXPERT role can create multiple classifications.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data=self.data_classify_multiple
        )
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        self.assertEqual(
            UserClassification.objects.filter(
                classification__resource__in=[self.resource, self.resource2],
                dynamic_attrs__observation_type="human",
                owner=self.alice,
            ).count(),
            2,
        )

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role can create multiple classifications.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data=self.data_classify_multiple
        )
        status = self.assert_json_context_variable(response, "status")
        self.assertTrue(status)
        self.assertEqual(
            UserClassification.objects.filter(
                classification__resource__in=[self.resource, self.resource2],
                dynamic_attrs__observation_type="human",
                owner=self.alice,
            ).count(),
            2,
        )

    def test_no_roles(self):
        """
        User with no roles can not create multiple classifications."""
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        self.assert_forbidden(
            self.url, method="post", data={"selected_resources": self.resource.pk}
        )


class ClassifyBoxApprovePermissionsTestCase(BaseClassifyTestCase):
    """"""

    url = None
    user_classification = None
    dynamic_attrs = None

    def setUp(self):
        """
        Login alice by default for all tests.
        """
        super().setUp()
        self.login_alice()

    def _call_helper(self, owner, roles, status=False):
        """"""
        super()._call_helper(owner, roles, status=status)
        self.user_classification = self.create_user_classification(
            owner, self.classification
        )
        self.dynamic_attrs = self.create_user_classification_dynamic_attrs(
            self.user_classification
        )
        self.url = reverse(
            "media_classification:classify_approve",
            kwargs={
                "pk": self.user_classification.pk,
            },
        )

    def test_owner(self):
        """
        Project owner can approve own classifications.
        """
        owner = self.alice
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data={}, follow=True
        )
        self.assert_has_no_message(response, ClassifyMessages.MSG_APPROVE_PERMS)
        qs = Classification.objects.filter(
            resource=self.resource,
            project=self.classification_project,
            status=True,
            approved_source__owner=self.alice,
        )
        self.assertTrue(qs.exists())

    def test_role_admin(self):
        """
        User with ADMIN role can approve other user classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.ADMIN)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data={}, follow=True
        )
        self.assert_has_no_message(response, ClassifyMessages.MSG_APPROVE_PERMS)
        qs = Classification.objects.filter(
            resource=self.resource,
            project=self.classification_project,
            status=True,
            approved_source__owner=self.ziutek,
        )
        self.assertTrue(qs.exists())

    def test_role_expert(self):
        """
        User with EXPERT role can not approve other user classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.EXPERT)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data={}, follow=True
        )
        self.assert_has_message(response, ClassifyMessages.MSG_APPROVE_PERMS)
        qs = Classification.objects.filter(
            resource=self.resource,
            project=self.classification_project,
            status=True,
            approved_source__owner=self.ziutek,
        )
        self.assertFalse(qs.exists())

    def test_role_collaborator(self):
        """
        User with COLLABORATOR role can not approve other user classification.
        """
        owner = self.ziutek
        roles = [(self.alice, ClassificationProjectRoleLevels.COLLABORATOR)]
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data={}, follow=True
        )
        self.assert_has_message(response, ClassifyMessages.MSG_APPROVE_PERMS)
        qs = Classification.objects.filter(
            resource=self.resource,
            project=self.classification_project,
            status=True,
            approved_source__owner=self.ziutek,
        )
        self.assertFalse(qs.exists())

    def test_no_roles(self):
        """
        User with no roles can not approve other user classification.
        """
        owner = self.ziutek
        roles = None
        self._call_helper(owner=owner, roles=roles)
        response = self.assert_access_granted(
            self.url, method="post", data={}, follow=True
        )
        self.assert_has_message(response, ClassifyMessages.MSG_APPROVE_PERMS)
        qs = Classification.objects.filter(
            resource=self.resource,
            project=self.classification_project,
            status=True,
            approved_source__owner=self.ziutek,
        )
        self.assertFalse(qs.exists())
