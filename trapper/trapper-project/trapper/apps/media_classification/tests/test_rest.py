import pytz
from django.urls import reverse
from rest_framework import status
from rest_framework.fields import DateTimeField
from rest_framework.test import APITestCase

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.models import (
    ClassificationProject,
    Classification,
)
from trapper.apps.media_classification.tests.factories.ai_classification import (
    AIClassificationFactory,
)
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_collection import (
    ClassificationProjectCollectionFactory,
)
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.media_classification.tests.factories.sequence import SequenceFactory
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import CollectionStatus, ResourceStatus
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory


class CSRestViewsTests(APITestCase):
    def setUp(self):
        super().setUp()
        self.user1 = UserFactory()
        self.user2 = UserFactory()

        self.research_project = ResearchProjectFactory(owner=self.user1)

        self.timezone = pytz.timezone("Europe/Warsaw")
        self.location1 = LocationFactory(
            owner=self.user1,
            research_project=self.research_project,
            timezone="Europe/Warsaw",
        )

        self.deployment1 = DeploymentFactory(
            owner=self.user1,
            research_project=self.research_project,
            location=self.location1,
        )

        self.resource1 = ResourceFactory(owner=self.user1, deployment=self.deployment1)
        self.resource2 = ResourceFactory(owner=self.user1, deployment=self.deployment1)
        self.resource3 = ResourceFactory(
            owner=self.user1, deployment=self.deployment1, inherit_prefix=True
        )
        self.resource4 = ResourceFactory(owner=self.user1)

        self.collection1 = CollectionFactory(owner=self.user1)
        self.collection2 = CollectionFactory(owner=self.user1)

        # # Set collection resources
        self.collection1.resources.set((self.resource1,))
        self.collection2.resources.set((self.resource1, self.resource3))

        self.collection = CollectionFactory(
            owner=self.user1, status=CollectionStatus.PRIVATE
        )
        self.collection.resources.set(Resource.objects.all())
        self.classificator = ClassificatorFactory(owner=self.user1)

        self.classification_project = ClassificationProjectFactory(
            research_project=self.research_project,
            owner=self.user1,
            classificator=self.classificator,
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC,
        )

        self.research_project.collections.set([self.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=self.classification_project.research_project.pk
        )[0]
        self.classification_project_collection = ClassificationProjectCollectionFactory(
            project=self.classification_project, collection=collection
        )

        self.sequence = SequenceFactory(
            collection=self.classification_project_collection
        )
        self.sequence.resources.set(
            [self.resource1, self.resource2, self.resource3, self.resource4]
        )
        self.client.force_authenticate(self.user1)

    def test_no_sequences_on_get_sequences_correct(self):
        url = reverse(
            "sequences_details",
            args=[
                self.classification_project.slug,
            ],
        )
        response = self.client.get(url)
        self.assertEqual(response.json(), [])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_one_sequence_details_from_sequences_correct(self):
        url = reverse(
            "sequences_details",
            args=[
                self.classification_project.slug,
            ],
        )
        url_with_params = f"{url}?id={self.sequence.id}"

        classifications = Classification.objects.filter(
            resource__in=[
                self.resource1,
                self.resource2,
                self.resource3,
                self.resource4,
            ],
            project=self.classification_project,
        )
        for classification in classifications:
            classification.sequence = self.sequence
            classification.approved_source_ai = AIClassificationFactory(
                classification=classification
            )
            classification.save()
        classifications = classifications.values_list("id", flat=True)

        response = self.client.get(url_with_params)
        self.assertEqual(
            response.json(),
            [
                {
                    "id": self.sequence.id,
                    "sequence_id": self.sequence.sequence_id,
                    "classifications": list(classifications),
                },
            ],
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_sequences_details_correct(self):
        classifications = Classification.objects.filter(
            resource__in=[
                self.resource1,
                self.resource2,
                self.resource3,
                self.resource4,
            ],
            project=self.classification_project,
        )
        for classification in classifications:
            classification.sequence = self.sequence
            classification.approved_source_ai = AIClassificationFactory(
                classification=classification
            )
            classification.save()
        classifications = classifications.values_list("id", flat=True)
        sequence1 = SequenceFactory(collection=self.classification_project_collection)
        classification1 = ClassificationFactory(
            owner=self.user1,
            project=self.classification_project,
            collection=self.classification_project_collection,
            sequence=sequence1,
        )
        classification1.approved_source_ai = AIClassificationFactory(
            classification=classification1
        )
        classification2 = ClassificationFactory(
            owner=self.user1,
            project=self.classification_project,
            collection=self.classification_project_collection,
            sequence=sequence1,
        )
        classification2.approved_source_ai = AIClassificationFactory(
            classification=classification2
        )
        classification1.save()
        classification2.save()

        sequence1.resources.set([classification1.resource, classification2.resource])

        url = reverse(
            "sequences_details",
            args=[
                self.classification_project.slug,
            ],
        )
        url_with_params = f"{url}?id={self.sequence.id},{sequence1.id}"
        response = self.client.get(url_with_params)

        self.assertEqual(
            response.json(),
            [
                {
                    "id": self.sequence.id,
                    "sequence_id": self.sequence.sequence_id,
                    "classifications": list(classifications),
                },
                {
                    "id": sequence1.id,
                    "sequence_id": sequence1.sequence_id,
                    "classifications": [classification1.id, classification2.id],
                },
            ],
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
