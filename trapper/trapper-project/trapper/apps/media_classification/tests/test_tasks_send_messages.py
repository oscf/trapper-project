from django.core import mail
from django.test import TestCase, override_settings

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.media_classification.tasks import SendMessages
from trapper.apps.media_classification.tests.factories.instance_settings import (
    CitizenScienceConfigurationFactory,
)


class SendMessagesTasksTest(TestCase):
    def setUp(self):
        super().setUp()
        self.sender = UserFactory()
        self.recipient = UserFactory(email="to@example.com")

        # Create project settings (name, logo, social media)
        CitizenScienceConfigurationFactory(
            project_name="Trapper",
            linkedin_url="http://linkedin.com/",
            facebook_url="http://facebook.com/",
            twitter_url="http://twitter.com/",
        ).update_object()

    def test_send_messages_correct(self):
        data = {
            "title": "Title",
            "message": "Message",
            "recipient_ids": [self.recipient.id],
            "sender_id": self.sender.id,
        }
        with override_settings(DOMAIN_NAME="localhost"):
            output = SendMessages(**data).run()

        self.assertEqual(output, "You have successfully sent messages.")
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.subject, "[Trapper][LOCALHOST] Title")
        self.assertIsNotNone(email.body)
        self.assertEqual(email.to, ["to@example.com"])
        self.assertEqual(email.from_email, "webmaster@localhost")
        received_messages = self.recipient.received_messages.all()
        self.assertEqual(received_messages.count(), 1)
        message = received_messages.first()
        self.assertEqual(message.subject, "Title")
        self.assertEqual(message.text, "Message")
