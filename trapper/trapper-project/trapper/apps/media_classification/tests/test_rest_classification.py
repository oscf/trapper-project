from unittest.mock import patch

import pytz
from django.core import mail
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from trapper.apps.accounts.tests.factories.user import UserFactory
from trapper.apps.extra_tables.models import Species
from trapper.apps.extra_tables.tests.factories import SpeciesFactory
from trapper.apps.geomap.tests.factories.deployment import DeploymentFactory
from trapper.apps.geomap.tests.factories.location import LocationFactory
from trapper.apps.media_classification.models import (
    ClassificationProject,
    Classification,
)
from trapper.apps.media_classification.taxonomy import (
    ObservationType,
    ClassificationProjectRoleLevels,
)
from trapper.apps.media_classification.tests.factories.ai_classification import (
    AIClassificationFactory,
    AIClassificationDynamicAttrsFactory,
)
from trapper.apps.media_classification.tests.factories.classification import (
    ClassificationFactory,
)
from trapper.apps.media_classification.tests.factories.classification_project_collection import (
    ClassificationProjectCollectionFactory,
)
from trapper.apps.media_classification.tests.factories.classificator import (
    ClassificatorFactory,
)
from trapper.apps.media_classification.tests.factories.sequence import SequenceFactory
from trapper.apps.media_classification.tests.factories.user_classification import (
    UserClassificationFactory,
    UserClassificationDynamicAttrsFactory,
)
from trapper.apps.research.models import ResearchProjectCollection
from trapper.apps.research.tests.factories.research_project import (
    ResearchProjectFactory,
)
from trapper.apps.storage.models import Resource
from trapper.apps.storage.taxonomy import CollectionStatus, ResourceStatus
from trapper.apps.storage.tests.factories.classification_project import (
    ClassificationProjectFactory,
)
from trapper.apps.storage.tests.factories.collection import CollectionFactory
from trapper.apps.storage.tests.factories.resource import ResourceFactory
from trapper.apps.media_classification.tests.factories.instance_settings import (
    CitizenScienceConfigurationFactory,
)


class CSRestClassificationViewsTests(APITestCase):
    def setUp(self):
        super().setUp()
        self.user1 = UserFactory()
        self.user2 = UserFactory(email="to@example.com")

        self.research_project = ResearchProjectFactory(owner=self.user1)

        self.timezone = pytz.timezone("Europe/Warsaw")
        self.location1 = LocationFactory(
            owner=self.user1,
            research_project=self.research_project,
            timezone="Europe/Warsaw",
        )

        self.deployment1 = DeploymentFactory(
            owner=self.user1,
            research_project=self.research_project,
            location=self.location1,
        )

        self.resource1 = ResourceFactory(owner=self.user1, deployment=self.deployment1)
        self.resource2 = ResourceFactory(owner=self.user1, deployment=self.deployment1)
        self.resource3 = ResourceFactory(
            owner=self.user1, deployment=self.deployment1, inherit_prefix=True
        )
        self.resource4 = ResourceFactory(owner=self.user1)

        self.collection1 = CollectionFactory(owner=self.user1)
        self.collection2 = CollectionFactory(owner=self.user1)

        # # Set collection resources
        self.collection1.resources.set((self.resource1,))
        self.collection2.resources.set((self.resource1, self.resource3))

        self.collection = CollectionFactory(
            owner=self.user1, status=CollectionStatus.PRIVATE
        )
        self.collection.resources.set(Resource.objects.all())
        self.species1 = SpeciesFactory()
        self.species2 = SpeciesFactory()
        self.classificator = ClassificatorFactory(owner=self.user1)
        self.classificator.species.add(self.species1, self.species2)
        self.classificator.tracked_species.add(self.species2)
        self.classification_project = ClassificationProjectFactory(
            research_project=self.research_project,
            owner=self.user1,
            classificator=self.classificator,
            citizen_science_status=ClassificationProject.CitizenScienceStatus.PUBLIC,
        )
        self.classification_project.classification_project_roles.create(
            name=ClassificationProjectRoleLevels.ADMIN, user=self.user2
        )
        self.research_project.collections.set([self.collection])

        collection = ResearchProjectCollection.objects.filter(
            project=self.classification_project.research_project.pk
        )[0]
        self.classification_project_collection = ClassificationProjectCollectionFactory(
            project=self.classification_project, collection=collection
        )

        self.resource = ResourceFactory(
            owner=self.user1, deployment=self.deployment1, status=ResourceStatus.PRIVATE
        )
        self.sequence = SequenceFactory(
            collection=self.classification_project_collection
        )
        self.sequence.resources.set(
            [self.resource1, self.resource2, self.resource3, self.resource4]
        )
        self.classification = ClassificationFactory(
            resource=self.resource,
            owner=self.user1,
            project=self.classification_project,
            collection=self.classification_project_collection,
        )
        self.classification1 = Classification.objects.get(
            project=self.classification_project, resource=self.resource1
        )
        self.ai_classification1 = AIClassificationFactory(
            classification=self.classification1, owner=self.user1
        )
        self.dynamic_ai_classification1 = AIClassificationDynamicAttrsFactory(
            ai_classification=self.ai_classification1,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )
        self.classification2 = Classification.objects.get(
            project=self.classification_project, resource=self.resource2
        )
        self.ai_classification2 = AIClassificationFactory(
            classification=self.classification2, owner=self.user1
        )
        self.dynamic_ai_classification2 = AIClassificationDynamicAttrsFactory(
            ai_classification=self.ai_classification2,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )
        self.classification3 = Classification.objects.get(
            project=self.classification_project, resource=self.resource3
        )
        self.user_classification3 = UserClassificationFactory(
            classification=self.classification3, owner=self.user1
        )
        self.dynamic_user_classification3 = UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification3,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )
        self.classification4 = Classification.objects.get(
            project=self.classification_project, resource=self.resource4
        )
        self.user_classification4 = UserClassificationFactory(
            classification=self.classification4, owner=self.user1
        )
        self.dynamic_user_classification4 = UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )
        self.client.force_authenticate(self.user1)

        # Create project settings (name, logo, social media)
        CitizenScienceConfigurationFactory(
            project_name="Trapper",
            linkedin_url="http://linkedin.com/",
            facebook_url="http://facebook.com/",
            twitter_url="http://twitter.com/",
        ).update_object()

    def test_single_classify_with_correct_data(self):
        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        data = {
            "dynamic": [
                {
                    "observation_type": "vehicle",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                    "bboxes_raw": [
                        [0.111, 0.111, 0.222, 0.333],
                    ],
                }
            ],
        }

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )

        response = self.client.post(
            reverse(
                "single_classify",
                args=[self.classification_project.slug, self.classification4.pk],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNone(response.data)

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            1,
        )
        dynamic_user_classification = (
            self.classification4.user_classifications.first().dynamic_attrs.first()
        )
        self.assertEqual(
            dynamic_user_classification.observation_type,
            ObservationType.VEHICLE,
        )
        self.assertEqual(
            dynamic_user_classification.bboxes,
            [[0.111, 0.111, 0.222, 0.333]],
        )
        self.assertTrue(
            self.classification_project.classification_project_roles.filter(
                user=self.user1, name=ClassificationProjectRoleLevels.CS_USER
            )
        )

    def test_single_classify_message_sent_when_species_is_tracked_correct(self):
        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.VEHICLE,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        data = {
            "dynamic": [
                {
                    "observation_type": "animal",
                    "species": self.species2.pk,
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                }
            ],
        }

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )

        response = self.client.post(
            reverse(
                "single_classify",
                args=[self.classification_project.slug, self.classification4.pk],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNone(response.data)

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            1,
        )
        dynamic_user_classification = (
            self.classification4.user_classifications.first().dynamic_attrs.first()
        )
        self.assertEqual(
            dynamic_user_classification.observation_type,
            ObservationType.ANIMAL,
        )
        self.assertEqual(
            dynamic_user_classification.species,
            self.species2,
        )
        self.assertEqual(
            dynamic_user_classification.bboxes,
            [[0.111, 0.111, 0.222, 0.333]],
        )

    def test_single_classify_with_multi_data_correct(self):
        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        data = {
            "dynamic": [
                {
                    "observation_type": "vehicle",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                },
                {
                    "observation_type": "animal",
                    "bboxes": {
                        "left": 0.333,
                        "top": 0.222,
                        "width": 0.111,
                        "height": 0.111,
                    },
                },
            ],
        }

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )

        response = self.client.post(
            reverse(
                "single_classify",
                args=[self.classification_project.slug, self.classification4.pk],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNone(response.data)

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )
        dynamic_user_classifications = (
            self.classification4.user_classifications.first().dynamic_attrs.all()
        )
        self.assertEqual(
            dynamic_user_classifications[0].observation_type,
            ObservationType.VEHICLE,
        )
        self.assertEqual(
            dynamic_user_classifications[0].bboxes,
            [[0.111, 0.111, 0.222, 0.333]],
        )
        self.assertEqual(
            dynamic_user_classifications[1].observation_type,
            ObservationType.ANIMAL,
        )
        self.assertEqual(
            dynamic_user_classifications[1].bboxes,
            [[0.333, 0.222, 0.111, 0.111]],
        )

    def test_single_classify_mark_as_empty_correct(self):
        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        data = {
            "dynamic": [
                {
                    "observation_type": "blank",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                }
            ],
        }

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )

        response = self.client.post(
            reverse(
                "single_classify",
                args=[self.classification_project.slug, self.classification4.pk],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNone(response.data)

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            1,
        )
        dynamic_user_classification = (
            self.classification4.user_classifications.first().dynamic_attrs.first()
        )
        self.assertEqual(
            dynamic_user_classification.observation_type,
            ObservationType.BLANK,
        )
        self.assertEqual(
            dynamic_user_classification.bboxes,
            [],
        )

    def test_single_classify_mark_as_empty_with_many_dynamic_data(self):
        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.VEHICLE,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        data = {
            "dynamic": [
                {
                    "observation_type": "animal",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                },
                {
                    "observation_type": "blank",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                },
            ],
        }

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )

        response = self.client.post(
            reverse(
                "single_classify",
                args=[self.classification_project.slug, self.classification4.pk],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(
            response.json(),
            {
                "dynamic": [
                    "If image is marked as empty, only one classification should be passed"
                ]
            },
        )

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )
        dynamic_user_classification = (
            self.classification4.user_classifications.first().dynamic_attrs.all()
        )
        self.assertEqual(
            dynamic_user_classification[0].observation_type,
            ObservationType.ANIMAL,
        )
        self.assertEqual(
            dynamic_user_classification[0].bboxes,
            [[0.444, 0.444, 0.333, 0.333]],
        )
        self.assertEqual(
            dynamic_user_classification[1].observation_type,
            ObservationType.VEHICLE,
        )
        self.assertEqual(
            dynamic_user_classification[1].bboxes,
            [[0.333, 0.333, 0.444, 0.444]],
        )

    def test_single_classify_with_approved_classification_incorrect(self):
        self.classification4.approved_source = self.user_classification4
        self.classification4.approved_by = self.user2
        self.classification4.save()

        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        data = {
            "dynamic": [
                {
                    "observation_type": "vehicle",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                }
            ],
        }

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )

        response = self.client.post(
            reverse(
                "single_classify",
                args=[self.classification_project.slug, self.classification4.pk],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(
            response.json(),
            {
                "non_field_errors": [
                    "You can only pass classification only to not classified resources."
                ]
            },
        )

        self.assertEqual(
            self.classification4.user_classifications.count(),
            1,
        )
        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )

    def test_single_classify_with_dynamic_classifier_field(self):
        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        self.classificator.custom_attrs = {
            "ciekawe_co_sie_wywali": {
                "target": "D",
                "values": "",
                "initial": "",
                "required": True,
                "field_type": "S",
                "description": "no ciekawe ciekawe",
            }
        }

        self.classificator.dynamic_attrs_order = (
            f"{self.classificator.dynamic_attrs_order},comments,ciekawe_co_sie_wywali"
        )
        self.classificator.predefined_attrs = {
            "comments": {"target": "D", "required": False}
        }
        self.classificator.save()

        data = {
            "dynamic": [
                {
                    "observation_type": "vehicle",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                }
            ],
        }

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )

        response = self.client.post(
            reverse(
                "single_classify",
                args=[self.classification_project.slug, self.classification4.pk],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNone(response.data)

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            1,
        )
        dynamic_user_classification = (
            self.classification4.user_classifications.first().dynamic_attrs.first()
        )
        self.assertEqual(
            dynamic_user_classification.observation_type,
            ObservationType.VEHICLE,
        )
        self.assertEqual(
            dynamic_user_classification.bboxes,
            [[0.111, 0.111, 0.222, 0.333]],
        )

    def test_feedback_classify_with_correct_data(self):
        self.classification4.approved_source = self.user_classification4
        self.classification4.approved_by = self.user2
        self.classification4.save()

        data = {
            "dynamic": [
                {
                    "observation_type": "vehicle",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                }
            ],
        }
        self.assertEqual(
            self.classification4.user_classifications.count(),
            1,
        )
        response = self.client.post(
            reverse(
                "feedback_classify",
                args=[self.classification_project.slug, self.classification4.pk],
            ),
            data,
            format="json",
        )
        self.assertIsNone(response.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # No new classification will be created
        self.assertEqual(
            self.classification4.user_classifications.count(),
            1,
        )
        self.assertTrue(
            self.classification4.user_classifications.all()[0].is_feedback,
        )
        self.assertEqual(
            self.classification4.user_classifications.get(
                is_feedback=True
            ).dynamic_attrs.count(),
            1,
        )
        dynamic_user_classification = self.classification4.user_classifications.get(
            is_feedback=True
        ).dynamic_attrs.first()
        self.assertEqual(
            dynamic_user_classification.observation_type,
            ObservationType.VEHICLE,
        )
        self.assertEqual(
            dynamic_user_classification.bboxes,
            [[0.111, 0.111, 0.222, 0.333]],
        )
        self.assertEqual(self.user2.received_messages.count(), 1)
        self.assertTrue(
            self.classification_project.classification_project_roles.filter(
                user=self.user1, name=ClassificationProjectRoleLevels.CS_USER
            )
        )

    def test_feedback_classify_without_approved_classify(self):
        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        data = {
            "dynamic": [
                {
                    "observation_type": "vehicle",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                }
            ],
        }

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )

        response = self.client.post(
            reverse(
                "feedback_classify",
                args=[self.classification_project.slug, self.classification4.pk],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(
            response.json(),
            {
                "non_field_errors": [
                    "You can only pass feedback only to classified resources."
                ]
            },
        )

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )

    def test_group_classify_with_ai_classification_blank_correct(self):
        data = {
            "classification_ids": [self.classification1.pk, self.classification2.pk],
            "dynamic": [{"observation_type": "blank"}],
        }
        response = self.client.post(
            reverse(
                "group_classify",
                args=[self.classification_project.slug],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNone(response.data)

        self.assertEqual(
            self.classification1.user_classifications.first()
            .dynamic_attrs.first()
            .observation_type,
            ObservationType.BLANK,
        )
        self.assertEqual(
            self.classification1.user_classifications.first()
            .dynamic_attrs.first()
            .bboxes,
            [],
        )
        self.assertEqual(
            self.classification2.user_classifications.first()
            .dynamic_attrs.first()
            .observation_type,
            ObservationType.BLANK,
        )
        self.assertEqual(
            self.classification2.user_classifications.first()
            .dynamic_attrs.first()
            .bboxes,
            [],
        )
        self.assertTrue(
            self.classification_project.classification_project_roles.filter(
                user=self.user1, name=ClassificationProjectRoleLevels.CS_USER
            )
        )

    def test_group_classify_with_user_classification_blank_correct(self):
        self.dynamic_user_classification3.observation_type = ObservationType.BLANK
        self.dynamic_user_classification3.bboxes = None
        self.dynamic_user_classification3.save()

        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        data = {
            "classification_ids": [self.classification3.pk, self.classification4.pk],
            "dynamic": [{"observation_type": "blank"}],
        }
        response = self.client.post(
            reverse(
                "group_classify",
                args=[self.classification_project.slug],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNone(response.data)

        self.assertEqual(
            self.classification3.user_classifications.first()
            .dynamic_attrs.first()
            .observation_type,
            ObservationType.BLANK,
        )
        self.assertEqual(
            self.classification3.user_classifications.first()
            .dynamic_attrs.first()
            .bboxes,
            [],
        )
        self.assertEqual(
            self.classification4.user_classifications.first()
            .dynamic_attrs.first()
            .observation_type,
            ObservationType.BLANK,
        )
        self.assertEqual(
            self.classification4.user_classifications.first()
            .dynamic_attrs.first()
            .bboxes,
            [],
        )
        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            1,
        )

    def test_group_classify_with_blank_user_classification_with_correct_data(self):
        self.dynamic_user_classification3.observation_type = ObservationType.BLANK
        self.dynamic_user_classification3.bboxes = None
        self.dynamic_user_classification3.save()

        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        # bboxes should be ignored
        data = {
            "classification_ids": [self.classification3.pk, self.classification4.pk],
            "dynamic": [
                {
                    "observation_type": "vehicle",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                }
            ],
        }
        response = self.client.post(
            reverse(
                "group_classify",
                args=[self.classification_project.slug],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNone(response.data)

        self.assertEqual(
            self.classification3.user_classifications.first()
            .dynamic_attrs.first()
            .observation_type,
            ObservationType.BLANK,
        )
        self.assertEqual(
            self.classification3.user_classifications.first()
            .dynamic_attrs.first()
            .bboxes,
            None,
        )
        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )
        dynamic_user_classification = (
            self.classification4.user_classifications.first().dynamic_attrs.all()
        )

        obs_types = [x.observation_type for x in dynamic_user_classification]
        self.assertListEqual(
            obs_types, [ObservationType.VEHICLE, ObservationType.VEHICLE]
        )

        bboxes = [x.bboxes for x in dynamic_user_classification]
        self.assertCountEqual(
            bboxes, [[[0.444, 0.444, 0.333, 0.333]], [[0.333, 0.333, 0.444, 0.444]]]
        )

    def test_group_classify_validation_dynamic_attrs(self):
        data = {
            "classification_ids": [self.classification3.pk, self.classification4.pk],
            "dynamic": [{"observation_type": "animal"}, {"observation_type": "animal"}],
        }
        response = self.client.post(
            reverse(
                "group_classify",
                args=[self.classification_project.slug],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(
            response.json(), {"dynamic": ["Field should have only one classification."]}
        )

    def test_group_classify_with_approved_classification_incorrect(self):
        user_classification2 = UserClassificationFactory(
            classification=self.classification2, owner=self.user1
        )
        UserClassificationDynamicAttrsFactory(
            userclassification=user_classification2,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )

        ai_classification4 = AIClassificationFactory(
            classification=self.classification4, owner=self.user1
        )
        AIClassificationDynamicAttrsFactory(
            ai_classification=ai_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )

        self.classification2.approved_source = user_classification2
        self.classification2.approved_by = self.user1
        self.classification2.save()

        self.classification4.approved_source = self.user_classification4
        self.classification4.approved_by = self.user2
        self.classification4.save()

        data = {
            "classification_ids": [self.classification2.pk, self.classification4.pk],
            "dynamic": [{"observation_type": "blank"}],
        }
        response = self.client.post(
            reverse(
                "group_classify",
                args=[self.classification_project.slug],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(
            response.json(),
            {
                "classification_ids": [
                    "Incorrect sequence ID. You can only pass classification only to not classified resources."
                ]
            },
        )

        self.assertEqual(
            self.classification4.user_classifications.count(),
            1,
        )

        self.assertEqual(
            self.classification2.user_classifications.count(),
            1,
        )

    def test_feedback_group_classify_with_ai_classification_blank(self):
        user_classification2 = UserClassificationFactory(
            classification=self.classification2, owner=self.user1
        )
        UserClassificationDynamicAttrsFactory(
            userclassification=user_classification2,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )

        ai_classification4 = AIClassificationFactory(
            classification=self.classification4, owner=self.user1
        )
        AIClassificationDynamicAttrsFactory(
            ai_classification=ai_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )

        self.classification2.approved_source = user_classification2
        self.classification2.approved_by = self.user1
        self.classification2.save()

        self.classification4.approved_source = self.user_classification4
        self.classification4.approved_by = self.user2
        self.classification4.save()

        data = {
            "classification_ids": [
                self.classification1.pk,
                self.classification2.pk,
                self.classification4.pk,
            ],
            "dynamic": [
                {
                    "observation_type": "vehicle",
                    "bboxes": {
                        "left": 0.111,
                        "top": 0.111,
                        "width": 0.222,
                        "height": 0.333,
                    },
                }
            ],
        }
        response = self.client.post(
            reverse(
                "feedback_group_classify",
                args=[self.classification_project.slug],
            ),
            data,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(
            response.json(),
            {
                "classification_ids": [
                    "You can pass feedback only to classified resources."
                ]
            },
        )

    def test_feedback_group_classify_with_one_classify_without_user_classification_correct(
        self,
    ):
        user_classification2 = UserClassificationFactory(
            classification=self.classification2, owner=self.user1
        )
        UserClassificationDynamicAttrsFactory(
            userclassification=user_classification2,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.444, 0.444, 0.333, 0.333]],
        )

        self.classification2.approved_source = user_classification2
        self.classification2.approved_by = self.user1
        self.classification2.save()

        data = {
            "classification_ids": [self.classification1.pk, self.classification2.pk],
            "dynamic": [{"observation_type": "blank"}],
        }
        response = self.client.post(
            reverse(
                "feedback_group_classify",
                args=[self.classification_project.slug],
            ),
            data,
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(
            response.json(),
            {
                "classification_ids": [
                    "You can pass feedback only to classified resources."
                ]
            },
        )

    def test_feedback_group_classify_without_correct_classifications(self):
        data = {
            "classification_ids": [self.classification1.pk, self.classification2.pk],
            "dynamic": [{"observation_type": "blank"}],
        }
        response = self.client.post(
            reverse(
                "feedback_group_classify",
                args=[self.classification_project.slug],
            ),
            data,
            format="json",
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertDictEqual(
            response.json(),
            {
                "classification_ids": [
                    "Incorrect sequence IDs, no classifications found."
                ]
            },
        )

    def test_request_for_new_species_with_correct_data(self):
        UserClassificationDynamicAttrsFactory(
            userclassification=self.user_classification4,
            observation_type=ObservationType.ANIMAL,
            bboxes=[[0.333, 0.333, 0.444, 0.444]],
        )

        data = {
            "species": "Bączek",
            "comment": "It'll be nice to have bączek inside team.",
        }

        self.assertEqual(
            self.classification4.user_classifications.first().dynamic_attrs.count(),
            2,
        )
        with patch(
            "trapper.apps.media_classification.serializers_rest.send_request_new_species_for_classify"
        ) as mocked_celery_task:
            response = self.client.post(
                reverse(
                    "request_new_species_for_classify",
                    args=[self.classification_project.slug, self.classification4.pk],
                ),
                data,
                format="json",
            )
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            self.assertIsNone(response.data)
            self.assertTrue(mocked_celery_task.called)
        self.assertFalse(
            self.classification_project.classification_project_roles.filter(
                user=self.user1, name=ClassificationProjectRoleLevels.CS_USER
            )
        )
