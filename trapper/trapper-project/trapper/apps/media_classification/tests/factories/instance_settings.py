from PIL import Image
from django.core.files.uploadedfile import SimpleUploadedFile

from trapper.apps.citizen_science.models import CitizenScienceConfiguration


class CitizenScienceConfigurationFactory:
    IMG_FNAME = "test_image.jpeg"

    def __init__(
        self, project_name: str, linkedin_url: str, facebook_url: str, twitter_url: str
    ):
        self.project_name = project_name
        self.linkedin_url = linkedin_url
        self.facebook_url = facebook_url
        self.twitter_url = twitter_url

    def update_object(self):
        project_logo = Image.new("RGB", (250, 250), (255, 255, 255))

        cs_config = CitizenScienceConfiguration.get_solo()
        cs_config.project_name = self.project_name
        cs_config.project_logo = SimpleUploadedFile(
            name=self.IMG_FNAME,
            content=project_logo.tobytes(),
            content_type="image/jpeg",
        )
        cs_config.linkedin_url = self.linkedin_url
        cs_config.facebook_url = self.facebook_url
        cs_config.twitter_url = self.twitter_url
        cs_config.save()
