from collections import namedtuple
from typing import Iterable, Dict, Any, cast

from trapper.apps.media_classification.models import (
    StandardStaticAttrsMixin,
    StandardDynamicAttrsMixin,
    AIClassificationDynamicAttrs,
)


def _copy_static_attrs_from(
    classificator,
    static_attrs: StandardStaticAttrsMixin,
):
    static_class_form_initial = {}
    # First set all active standard attributes
    for attr_name in classificator.active_standard_attrs("STATIC"):
        attr_value = getattr(static_attrs, attr_name)
        static_class_form_initial[attr_name] = attr_value
    # Then set custom attributes
    if static_attrs.static_attrs:
        static_class_form_initial.update(
            cast(Dict[str, Any], static_attrs.static_attrs)
        )

    return static_class_form_initial


def _copy_dattrs_from(
    classificator,
    dynamic_attributes_list: Iterable[StandardDynamicAttrsMixin],
):
    dynamic_formset_initial = []
    for dattrs in dynamic_attributes_list:
        # if dattrs come from observations stored in Resource.data
        if type(dattrs) is dict:
            dattrs["species"] = dattrs.pop("species_id", None)
            dattrs = namedtuple("dattrs", dattrs.keys())(*dattrs.values())

        dattrs_initial: Dict[str, Any] = {}
        # First set all active standard attributes
        for attr_name in classificator.active_standard_attrs("DYNAMIC"):
            attr_value = getattr(dattrs, attr_name)
            if attr_name == "species" and attr_value:
                attr_value = getattr(attr_value, "pk", None) or attr_value
            dattrs_initial[attr_name] = attr_value
        # bboxes
        if dattrs.bboxes:
            dattrs_initial["bboxes"] = dattrs.bboxes
        if dattrs.attrs:
            dattrs_initial.update(cast(Dict[str, Any], dattrs.attrs))

        if hasattr(dattrs, "classification_confidence"):
            dattrs_initial["classification_confidence"] = cast(
                AIClassificationDynamicAttrs, dattrs
            ).classification_confidence

        dynamic_formset_initial.append(dattrs_initial)

    return dynamic_formset_initial
