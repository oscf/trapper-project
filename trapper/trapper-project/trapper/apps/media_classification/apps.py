from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class MediaClassificationConfig(AppConfig):
    name = "trapper.apps.media_classification"
    verbose_name = _("Media classification")
