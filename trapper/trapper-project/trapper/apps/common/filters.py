# -*- coding: utf-8 -*-
"""Commonly used filters"""
import datetime
import functools
import operator
import pytz

import django_filters
from django import forms
from django.conf import settings
from django.contrib.postgres.search import SearchQuery
from django.db.models import Q
from django.utils import timezone

from rest_framework.filters import SearchFilter

from trapper.apps.common.tools import parse_pks
from trapper.middleware import get_current_user


class BaseOwnBooleanFilter(django_filters.Filter):
    """Base boolean filter class for limiting queryset to values that are
    owned by currently logged in user or user is in managers (optionally)
    or have a role in some project (optionally)"""

    field_class = forms.BooleanField
    status_class = None

    def __init__(
        self,
        owner_field="owner",
        managers=True,
        managers_field="managers",
        role_levels=None,
        role_field="project_roles",
        *args,
        **kwargs,
    ):
        self.owner_field = owner_field
        self.managers = managers
        self.managers_field = managers_field
        self.role_levels = role_levels
        self.role_field = role_field
        super().__init__(*args, **kwargs)

    def filter(self, queryset, value):
        user = get_current_user()
        if not user.is_authenticated:
            return queryset.none()
        if user.is_staff:
            return queryset
        if value is True:
            or_queries = [
                Q(**{self.owner_field: user}),
            ]
            if self.managers:
                or_queries.append(Q(**{self.managers_field: user}))
            if self.role_levels:
                or_queries.append(
                    Q(**{"{role_field}__user".format(role_field=self.role_field): user})
                    & Q(
                        **{
                            "{role_field}__name__in".format(
                                role_field=self.role_field
                            ): self.role_levels
                        }
                    )
                )
            # TODO: Replace reduce with simple for loop
            queryset = queryset.filter(functools.reduce(operator.or_, or_queries))
        return queryset


class BaseDateFilter(django_filters.DateFilter):
    """Base date filter with custom input formats"""

    field_class = forms.DateField

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.field.input_formats = settings.DATE_INPUT_FORMATS


class BaseTimeFilter(django_filters.Filter):
    """Base time filter compatible with a
    :class:`django.db.models.DateTimeField`."""

    field_class = forms.CharField

    def __init__(self, time_format, lookup_expr, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.time_format = time_format
        self.lookup_expr = lookup_expr

    def filter(self, qs, value):
        # workaround to be able to always apply this filter to UTC datetimes
        # see https://stackoverflow.com/a/55934338
        timezone.activate(pytz.UTC)
        if not value:
            return qs
        try:
            value = datetime.datetime.strptime(value, self.time_format).time()
        except ValueError:
            return qs.none()
        query = {
            f"{self.field_name}__time__{self.lookup_expr}": value,
        }
        return qs.filter(**query)


class BaseLocationsMapFilter(django_filters.Filter):
    """Filters queryset based on locations set passed from
    the map view `/geomap/map`. Received locations are in a form of
    list of integers: `int1,int2,...,intN`"""

    def filter(self, qs, value):
        if value:
            pks = parse_pks(value)
            if pks:
                return qs.filter(
                    **{"{model_field}__in".format(model_field=self.field_name): pks}
                ).distinct()
        return qs


class BaseFilterSet(django_filters.FilterSet):
    """Simple filter that makes possible to filter
    through pk that is list of integers: `int1,int2,...,intN`"""

    pk = django_filters.CharFilter(method="filter_pk")

    def filter_pk(self, qs, name, value):
        """Prepare list of pk from string"""
        if value.strip() == "":
            return qs
        ids = parse_pks(pks=value)
        qs = qs.filter(pk__in=ids)
        return qs


# customized DRF SearchFilter backend
class FullTextSearchFilter(SearchFilter):
    """
    TODO: docstrings
    """

    reserved_terms = ["&", "|", ")", "(", ":", "*", ":*", "!", "<", ">", "<->"]

    def filter_queryset(self, request, queryset, view):
        search_fields = getattr(view, "search_fields", None)
        search_terms = self.get_search_terms(request)
        if not search_fields or not search_terms:
            return queryset
        # format searchvector raw query
        for i, term in enumerate(search_terms):
            if i == 0:
                raw_search_terms = term
                continue
            if (
                term not in self.reserved_terms
                and search_terms[i - 1] not in self.reserved_terms
            ):
                raw_search_terms = f"{raw_search_terms} & {term}"
            else:
                raw_search_terms = f"{raw_search_terms} {term}"
        kwargs = {
            k: SearchQuery(raw_search_terms, search_type="raw") for k in search_fields
        }
        return queryset.filter(
            functools.reduce(
                operator.or_, (Q(**d) for d in [dict([i]) for i in kwargs.items()])
            )
        )
