# -*- coding: utf-8 -*-
"""Model definitions or utils that can be used in other parts of project"""

from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _
from tinymce.models import HTMLField

from trapper.apps.common.fields import SVGImageFormField


class BaseAccessMember(models.Model):
    """Abstract model that hold user and created date field, that can
    be used in various applications

    This model adds:

    * `user` - foreign key to :class:`auth.User`
    * `date_created` - :class:`models.DateTimeField` with default value set to
        :func:`datetime.datetime.now`
    """

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.DO_NOTHING)
    date_created = models.DateTimeField(
        auto_now_add=True, editable=False, verbose_name=_("Date created")
    )

    class Meta:
        abstract = True


class SVGImageField(models.ImageField):
    def formfield(self, **kwargs):
        defaults = {"form_class": SVGImageFormField}
        defaults.update(kwargs)
        return super().formfield(**defaults)


class BaseDateModel(models.Model):
    """
    Abstract model that hold created and updated date field, that can
    be used in various applications

    This model adds:

    * `date_created` - :class:`models.DateTimeField` with default value set to
        :func:`datetime.datetime.now` on create
    * `date_modified` - :class:`models.DateTimeField` with default value set to
        :func:`datetime.datetime.now` on update

    """

    date_created = models.DateTimeField(auto_now_add=True, editable=False)
    date_modified = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True


class Consent(BaseDateModel):
    class Project(models.TextChoices):
        EXPERT = "expert", _("Trapper Expert")
        CS = "cs", _("Citizen Science")

    class Type(models.TextChoices):
        GDPR = "gdpr", _("GDPR")
        TOS = "tos", _("Term of Service")

    project = models.CharField(
        max_length=8,
        choices=Project.choices,
    )
    type = models.CharField(
        max_length=8,
        choices=Type.choices,
    )
    language = models.CharField(
        choices=settings.LANGUAGES,
        max_length=4,
    )
    content = HTMLField()
    version = models.CharField(blank=True, max_length=8)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["project", "type", "language"],
                name="unique_consent_project_language_type",
            )
        ]
