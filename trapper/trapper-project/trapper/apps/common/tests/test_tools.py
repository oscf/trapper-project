from django.test import TestCase, override_settings
from rest_framework.exceptions import ValidationError
from rest_framework.settings import api_settings

from trapper.apps.citizen_science.models import CitizenScienceConfiguration
from trapper.apps.common.tools import non_field_errors, TrapperCSMail


class ToolsTest(TestCase):
    def test_non_field_errors(self):
        output = non_field_errors("Test")

        self.assertIsInstance(output, ValidationError)
        self.assertEqual(output.status_code, 400)
        self.assertIn(api_settings.NON_FIELD_ERRORS_KEY, output.detail)
        self.assertEqual(str(output.detail["non_field_errors"][0]), "Test")

    def test_trapper_cs_mail_create_title(self):
        def create_trapper_cs_mail():
            return TrapperCSMail(
                template_name="common/generic_message.html",
                title="Test title",
                context={},
                user_mails=[],
            )

        CitizenScienceConfiguration.objects.create(project_name="Test project name")
        with override_settings(DOMAIN_NAME="localhost"):
            self.assertEqual(
                create_trapper_cs_mail().title,
                "[Test project name][LOCALHOST] Test title",
            )

        with override_settings(DOMAIN_NAME="dev.trapper"):
            self.assertEqual(
                create_trapper_cs_mail().title,
                "[Test project name][DEV] Test title",
            )

        with override_settings(DOMAIN_NAME="uat.trapper"):
            self.assertEqual(
                create_trapper_cs_mail().title,
                "[Test project name][UAT] Test title",
            )
