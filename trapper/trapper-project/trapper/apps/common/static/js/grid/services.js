'use strict';

angular.module('trapperGrid').factory('config', function () {
    var service = {};

    service.listStatuses = {
        loading: '<span class="fa fa-cog fa-spin"></span> ' + global['Translations']['GLOBAL']['loading_data'],
        deleting: '<span class="fa fa-cog fa-spin"></span> ' + global['Translations']['GLOBAL']['delete_selected_records'],
        error: '<span class="fa fa-exclamation"></span> ' + global['Translations']['GLOBAL']['reload_page'],
        empty: '<span class="fa fa-info"></span> ' + global['Translations']['GLOBAL']['empty']
    };

    service.dateFormat = {
        picker: 'YYYY-MM-DD',
        json: 'YYYY-MM-DD',
        table: 'YYYY-MM-DD HH:mm:ss Z',
        simple: 'YYYY-MM-DD HH:mm:ss'
    };

    service.timeFormat = {
        is24: false,
        picker: 'H:i'
    };

    service.mediaIcons = {
        'A': '<span class="fa fa-file-audio-o fa-3x"></span>',
        'V': '<span class="fa fa-file-video-o fa-3x"></span>',
        'I': '<span class="fa fa-file-image-o fa-3x"></span>',
        default: '<span class="fa fa-file-o fa-3x"></span>'
    };

    service.classifiedIcons = {
        'true': '<span class="fa fa-check fa-2x text-success" data-tooltip="tooltip" title="User classification"></span>',
        'false': '<span class="fa fa-close fa-2x text-success" data-tooltip="tooltip" title="User classification"></span>'
    };

    service.classifiedAIIcons = {
        'true': '<span class="fa fa-check fa-2x text-info" data-tooltip="tooltip" title="AI classification"></span>',
        'false': '<span class="fa fa-close fa-2x text-info" data-tooltip="tooltip" title="AI classification"></span>'
    };

    service.optionList = {
        bool: [{
            value: '',
            label: global['Translations']['GLOBAL']['all']
        }, {
            value: 'True',
            label: global['Translations']['GLOBAL']['yes'],
        }, {
            value: 'False',
            label: global['Translations']['GLOBAL']['no']
        }],
        progress: [{
            value: '',
            label: global['Translations']['GLOBAL']['all'],
        }, {
            value: '1',
            label: global['Translations']['GLOBAL']['ongoing'],
        }, {
            value: '2',
            label: global['Translations']['GLOBAL']['finished']
        }],
        media: [{
            value: '',
            label: global['Translations']['GLOBAL']['all']
        }, {
            value: 'I',
            label: global['Translations']['GLOBAL']['image'],
        }, {
            value: 'A',
            label: global['Translations']['GLOBAL']['audio']
        }, {
            value: 'V',
            label: global['Translations']['GLOBAL']['video']
        }],
        status: [{
            value: '',
            label: global['Translations']['GLOBAL']['all']
        }, {
            value: 'Public',
            label: global['Translations']['GLOBAL']['public']
        }, {
            value: 'Private',
            label: global['Translations']['GLOBAL']['private']
        }, {
            value: 'OnDemand',
            label: global['Translations']['GLOBAL']['ondemand']
        }],
        status_simple: [{
            value: '',
            label: global['Translations']['GLOBAL']['all']
        }, {
            value: 'Public',
            label: global['Translations']['GLOBAL']['public']
        }, {
            value: 'Private',
            label: global['Translations']['GLOBAL']['private']
        }],
        edit_status: [{
            value: '',
            label: '-'
        }, {
            value: 'Everyone can edit',
            label: global['Translations']['GLOBAL']['everyone']
        }, {
            value: 'Only editors can edit',
            label: global['Translations']['GLOBAL']['only_editors']
        }, {
            value: 'Only owner can edit',
            label: global['Translations']['GLOBAL']['only_owner']
        }],
        share_status: [{
            value: '',
            label: '-'
        }, {
            value: 'everyone (public)',
            label: global['Translations']['GLOBAL']['everyone']
        }, {
            value: 'anyone with link',
            label: global['Translations']['GLOBAL']['anyone_with_link']
        }, {
            value: 'Only owner can edit',
            label: global['Translations']['GLOBAL']['only_owner_edit']
        }],
        observation_type: [{
            value: '',
            label: global['Translations']['GLOBAL']['all']
        }, {
            value: 'human',
            label: global['Translations']['GLOBAL']['human'],
        }, {
            value: 'animal',
            label: global['Translations']['GLOBAL']['animal']
        }, {
            value: 'vehicle',
            label: global['Translations']['GLOBAL']['vehicle']
        }, {
            value: 'blank',
            label: global['Translations']['GLOBAL']['blank']
        }, {
            value: 'unknown',
            label: global['Translations']['GLOBAL']['unknown']
        }, {
            value: 'unclassified',
            label: global['Translations']['GLOBAL']['unclassified']
        }],
    };

    service.grid = {};
    service.grid.pageSizes = [10, 50, 100, 200, 500];
    service.grid.defaultSize = service.grid.pageSizes[0];

    service.columns = {
        resources: [
            '',
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['type'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['name'],
            global['Translations']['SERVICES']['COLUMNS']['RESOURCES']['is_empty'],
            global['Translations']['SERVICES']['COLUMNS']['RESOURCES']['observation_type'],
            global['Translations']['SERVICES']['COLUMNS']['RESOURCES']['species'],
            global['Translations']['SERVICES']['COLUMNS']['RESOURCES']['tags'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['date_recorded'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['owner'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions']
        ],
        collections: [
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['name'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['description'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['status'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['owner'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions']
        ],
        project_collections: [
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['name'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['status'],
            global['Translations']['SERVICES']['COLUMNS']['PROJECT_COLLECTIONS']['active'],
            global['Translations']['SERVICES']['COLUMNS']['PROJECT_COLLECTIONS']['total'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['classified'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['approved'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['classify']
        ],
        research_projects: [
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['name'],
            global['Translations']['SERVICES']['COLUMNS']['RESEARCH_PROJECTS']['acronym'],
            global['Translations']['SERVICES']['COLUMNS']['RESEARCH_PROJECTS']['keywords'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions']
        ],
        classification_projects: [
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['name'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['research_project'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['status'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions']
        ],
        classifications: [
            '',
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['type'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['name'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['classification_project'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['date_recorded'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['approved'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['classified'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['classification_tags'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['classify']
        ],
        user_classifications: [
            '',
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['type'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['name'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['date_recorded'],
            global['Translations']['SERVICES']['COLUMNS']['USER_CLASSIFICATION']['user'],
            global['Translations']['SERVICES']['COLUMNS']['USER_CLASSIFICATION']['updated'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['approved'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['classification_tags'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions']
        ],
        ai_classifications: [
            '',
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['type'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['name'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['date_recorded'],
            global['Translations']['SERVICES']['COLUMNS']['AI_CLASSIFICATION']['ai_provider'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['approved'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['classification_tags'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions']
        ],
        classificators: [
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['name'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['owner'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['date_updated'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions']
        ],
        locations: [
            'ID',
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['research_project'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['owner'],
            global['Translations']['SERVICES']['COLUMNS']['LOCATIONS']['coordinates'],
            global['Translations']['SERVICES']['COLUMNS']['LOCATIONS']['timezone'],
            global['Translations']['SERVICES']['COLUMNS']['LOCATIONS']['country'],
            global['Translations']['SERVICES']['COLUMNS']['LOCATIONS']['state'],
            global['Translations']['SERVICES']['COLUMNS']['LOCATIONS']['county'],
            global['Translations']['SERVICES']['COLUMNS']['LOCATIONS']['city'],
            global['Translations']['SERVICES']['COLUMNS']['LOCATIONS']['is_public'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions']
        ],
        deployments: [
            'ID',
            global['Translations']['SERVICES']['COLUMNS']['DEPLOYMENTS']['location_id'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['research_project'],
            global['Translations']['SERVICES']['COLUMNS']['DEPLOYMENTS']['start'],
            global['Translations']['SERVICES']['COLUMNS']['DEPLOYMENTS']['end'],
            global['Translations']['SERVICES']['COLUMNS']['DEPLOYMENTS']['corr_setup'],
            global['Translations']['SERVICES']['COLUMNS']['DEPLOYMENTS']['corr_tstamp'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['owner'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions']
        ],
        maps: [
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['name'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['description'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['owner'],
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['date_updated'],
            '',
            global['Translations']['SERVICES']['COLUMNS']['COMMON']['actions']
        ]
    };

    return service;
});

// data get, post & remove services
angular.module('trapperGrid').factory('dataService', ['$http', '$q', function ($http, $q) {
    var service = {};

    service.load = function (url, queryParams) {
        return $http.get(
            url, {
                params: queryParams
            }
        ).then(function (response) {
            if (response && response.data) {
                return response.data;
            }
            return $q.reject(global['Translations']['ERROR']['invalid_data_returned']);
        });
    };

    service.post = function (url, data) {
        return $http({
            method: 'POST',
            url: url,
            data: data
        }).then(function (response) {
            if (response && response.data) {
                return response.data;
            }
            return $q.reject(global['Translations']['ERROR']['invalid_data_returned']);
        });
    };

    service.remove = function (url, records) {
        var pks = records.join(',');
        return $http({
            method: 'POST',
            url: url,
            data: {
                pks: pks
            }
        }).then(function (response) {
            if (!response.data.status) {
                throw new Error(response.data.msg);
            }
            return response;
        });
    };

    return service;
}]);

// filter service
angular.module('trapperGrid').factory('filterService', ['$filter', function ($filter) {
    function Service(onUpdate) {
        this.filters = {};
        this.hasFilters = false;
        this.onUpdate = onUpdate || function () {
        };
    }

    Service.prototype.clearFilters = function () {
        this.filters = {};
        this.onUpdate('clear');
    };

    Service.prototype.rebuildFilters = function () {
        this.onUpdate('rebuild');
    };

    Service.prototype.updateFilter = function (filter) {
        var filterObj = this.filters[filter.name];

        if (!filterObj) {
            filterObj = this.filters[filter.name] = '';
        } else {
            filterObj.type = filter.type;
            filterObj.value = filter.value;
            filterObj.exact = filter.exact;
        }

        this.onUpdate(filterObj);
    };

    return {
        create: function (onUpdate) {
            return new Service(onUpdate);
        }
    };
}]);
