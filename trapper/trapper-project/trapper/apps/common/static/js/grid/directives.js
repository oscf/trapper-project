(function(window, angular) {
    'use strict';
    var trapperGrid = angular.module('trapperGrid');

    /* TRAPPER CUSTOM MODULES */

    var alert = window.TrapperApp.Alert,
        modal = window.TrapperApp.Modal,
        plugins = window.TrapperApp.Plugins;

    /* HELPER DIRECTIVES */

    // http://stackoverflow.com/questions/17470790/how-to-use-a-keypress-event-in-angularjs
    trapperGrid.directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if (event.which === 13) {
                    scope.$apply(function() {
                        scope.$eval(attrs.ngEnter);
                    });
                    event.preventDefault();
                }
            });
        };
    });

    trapperGrid.directive('bindHtml', ['$compile', function ($compile) {
	return function(scope, element, attrs) {
            scope.$watch(
		function(scope) {
		    return scope.$eval(attrs.bindHtml);
		},
		function(value) {
		    element.html(value);
		    $compile(element.contents())(scope);
		}
            );
	};
    }]);

    /* ACTION DIRECTIVES */

    function removeAction(scope, url, records) {
        modal.confirm({
            title: global['Translations']['DIRECTIVES']['delete_selected_records'],
            content: global['Translations']['DIRECTIVES']['you_have_selected'] + '<strong>' +
		records.length + '</strong>' + global['Translations']['DIRECTIVES']['records_to_delete'] +
                global['Translations']['GLOBAL']['continue_question'],
            buttons: [{
                label: global['Translations']['GLOBAL']['yes'],
                type: 'success',
                onClick: function() {
                    scope.remove(url, records);
                }
            }, {
                label: global['Translations']['GLOBAL']['no'],
                type: 'danger'
            }]
        });
    }

    trapperGrid.directive('actionRemove', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-danger" title="' +
		global['Translations']['GLOBAL']['delete'] + '"><span class="fa fa-trash-o"></span></button>',
            link: function postLink(scope) {
                scope.execute = function($event, record) {
                    $event.stopPropagation();
                    removeAction(scope, record.delete_data, [record.pk]);
                };
            }
        };
    });

    var getPKS = function(scope) {
        return scope.data.selected.join(',');
    };
    var msg_zero_items_selected = global['Translations']['DIRECTIVES']['select_at_least_one'];
    var msg_processing = '<div class="alert alert-info"><span class="fa fa-cog fa-spin">' +
        '</span> ' + global['Translations']['GLOBAL']['processing_data'] + '</div>';
    var msg_server_error = global['Translations']['ERROR']['unexpected_server_error'];


    trapperGrid.directive('actionRemoveSelected', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#" ng-disabled="!filters.owner">' +
		global['Translations']['DIRECTIVES']['delete_selected'] + '</a>',
            link: function postLink(scope) {
                scope.execute = function($event, url) {
                    $event.preventDefault();
                    var records = scope.data.selected;
                    if (!records.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }
                    removeAction(scope, url, records);
                };
            }
        };
    });

    trapperGrid.directive('actionApproveSelected', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['approve_selected'] + '</a>',
            link: function postLink(scope) {
                scope.execute = function($event, url) {
                    $event.preventDefault();

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.confirm({
                        title: global['Translations']['DIRECTIVES']['approve_selected_user_classification'],
                        content: global['Translations']['DIRECTIVES']['you_have_selected'] +
			    '<strong>' + scope.data.selected.length + '</strong> ' +
                            global['Translations']['DIRECTIVES']['user_classification_to_approve'] +
			    global['Translations']['GLOBAL']['continue_question'],
                        buttons: [{
                            label: global['Translations']['GLOBAL']['yes'],
                            type: 'success',
                            hide: false,
                            onClick: function() {
                                $('.modal-body').prepend(msg_processing);
                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: { pks: pks }
                                }).then(function(response) {
                                    if (!response.data.status) {
                                        alert.error(response.data.msg);
                                        modal.hideModal();
                                    } else {
                                        alert.success(response.data.msg);
                                        scope.load();
                                        modal.hideModal();
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            }
                        }, {
                            label: global['Translations']['GLOBAL']['no'],
                            type: 'danger'
                        }]
                    });
                };
            }
        };
    }]);

    trapperGrid.directive('actionApproveSelectedAiClassifications', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['approve_selected'] + '</a>',
            link: function postLink(scope) {
                scope.execute = function($event, url) {
                    $event.preventDefault();

                    const pks = getPKS(scope);

                    if (!pks.length) {
                        alert.error(global['Translations']['DIRECTIVES']['select_one_classification']);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['approve_selected_ai_classifications'],
                        url: url,
                        onShow: function($modal) {
                            const $form = $modal.find('form');

                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $("#id_ai_classification_pks").val(pks);
                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    modal.hideModal();
                                    scope.load();
                                    if (response.data.success) {
                                        alert.success(response.data.msg);
                                    } else {
                                        alert.error(response.data.msg);
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });
                };
            }
        };
    }]);

    trapperGrid.directive("actionCopyBboxesFromAi", ['$http', function ($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['copy_bboxes_from_ai'] + '</a>',
            link: function postLink(scope) {
                scope.execute = function ($event, url) {
                    $event.preventDefault();

                    const pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(global['Translations']['DIRECTIVES']['select_one_classification']);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['copy_bboxes_from_ai'],
                        url: url,
                        onShow: function ($modal) {
                            const $form = $modal.find('form');

                            $form.on("submit", function(e) {
                                e.preventDefault();
                                $("#id_ai_classification_pks").val(pks);
                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function (response) {
                                    modal.hideModal();
                                    scope.load();
                                    if (response.data.success) {
                                        alert.success(response.data.msg);
                                    } else {
                                        alert.error(response.data.msg);
                                    }
                                }).catch(function (error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                })
                            })
                        }
                    })

                }
            }
        }
    }]);

    trapperGrid.directive('actionGenerateDataPackage', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['generate_data_package'] + '</a>',
            link: function postLink(scope) {
                scope.execute = function($event, url) {
                    $event.preventDefault();

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['generate_data_package'],
                        url: url,
                        onShow: function($modal) {
                            var $form = $modal.find('form');

                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $form.prepend(msg_processing);
                                $("#id_resources_pks").val(pks);

                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        if (response.data.form_html) {
                                            $form.html(
                                                response.data.form_html
                                            );
                                        } else {
                                            alert.error(response.data.msg);
                                        }
                                    } else {
                                        alert.success(response.data.msg);
                                        modal.hideModal();
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });

                };
            }
        };
    }]);

    trapperGrid.directive('actionRegenerateTokens', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['regenerate_tokens'] + '</a>',
            link: function postLink(scope) {
                scope.execute = function($event, url) {
                    $event.preventDefault();

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['regenerate_tokens'],
                        url: url,
                        onShow: function($modal) {
                            var $form = $modal.find('form');

                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $form.prepend(msg_processing);
                                $("#id_resources_pks").val(pks);

                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        if (response.data.form_html) {
                                            $form.html(
                                                response.data.form_html
                                            );
                                        } else {
                                            alert.error(response.data.msg);
                                        }
                                    } else {
                                        alert.success(response.data.msg);
                                        modal.hideModal();
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });

                };
            }
        };
    }]);

    trapperGrid.directive('actionUpdate', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-default" title="' +
		global['Translations']['GLOBAL']['update'] + '"><span class="fa fa-pencil"></span></button>',
            link: function postLink(scope) {
                scope.execute = function($event, record, redirect) {
                    $event.stopPropagation();
                    if (redirect) {
                        window.location = record.update_data;
                        return;
                    }
                    modal.external({
                        title: global['Translations']['GLOBAL']['update'],
                        url: record.update_data,
                        onShow: function($modal) {
                            var $form = $modal.find('form');
                            plugins.collapsable();
                            plugins.select2();
                            plugins.select2ajaxForms();
                            plugins.datepicker();

                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $form.prepend(msg_processing);

                                $http({
                                    method: 'POST',
                                    url: $form.attr('action'),
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        $form.html(
                                            response.data.form_html
                                        );
                                        plugins.collapsable();
                                        plugins.select2();
                                        plugins.datepicker();
                                        return;
                                    }
                                    alert.success(response.data.msg);
                                    scope.load();
                                    modal.hideModal();
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });
                };
            }
        };
    }]);

    trapperGrid.directive('actionBulkUpdate', ['$http', '$q', 'config', function($http, $q, config) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#" ng-disabled="!filters.owner">' +
		global['Translations']['DIRECTIVES']['bulk_update'] + '</a>',
            link: function postLink(scope) {

                scope.execute = function(url) {

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['bulk_update'],
                        url: url,
                        onShow: function($modal) {
                            var $form = $modal.find('form');
                            plugins.select2();
                            plugins.datepicker();
                            plugins.lockerInputs();
                            plugins.select2ajaxForms();

                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $form.prepend(msg_processing);
                                $("#id_records_pks").val(pks);

                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        if (response.data.form_html) {
                                            $form.html(
                                                response.data.form_html
                                            );
                                            plugins.select2();
                                            plugins.datepicker();
                                            plugins.lockerInputs();
                                            plugins.select2ajaxForms();
                                        } else {
                                            alert.error(response.data.msg);
                                            modal.hideModal();
                                        }
                                    } else {
                                        alert.success(response.data.msg);
                                        scope.load();
                                        modal.hideModal();
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });
                };
            }
        };
    }]);

    trapperGrid.directive('actionResourceFeedback', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
	    template: '<button class="btn btn-default"><span class="fa fa-gear"></span> Feedback</button>',
            link: function postLink(scope) {
                scope.execute = function(url) {
                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(global['Translations']['DIRECTIVES']['select_at_least_one']);
                        return;
                    }
                    modal.external({
                        title: global['Translations']['DIRECTIVES']['resource_feedback'],
                        url: url,
                        onShow: function ($modal) {
                            var $form = $modal.find('form');
                            $form.on('submit', function (e) {
                                e.preventDefault();
                                $("#id_classification_pks").val(pks);
                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function (response) {
                                    if (response.data.status) {
                                        modal.hideModal();
                                        alert.success(response.data.msg);
                                    } else {
                                        $form.html(response.data.form_html);
                                    }
                                }).catch(function (error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        },
                        size: 'md'
                    });
                };
            }
        };
    }]);

    trapperGrid.directive('actionImportAttrs', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
	    template: '<button class="btn btn-default"><span class="fa fa-gear"></span> Import attributes</button>',
            link: function postLink(scope) {
                scope.execute = function(url) {
                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(global['Translations']['DIRECTIVES']['select_at_least_one']);
                        return;
                    }
                    modal.external({
                        title: 'Import standard attributes',
                        url: url,
                        onShow: function($modal) {
                            var $form = $modal.find('form');
                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $("#id_collections_pks").val(pks);
                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (response.data.status) {
					modal.hideModal();
                                        alert.success(response.data.msg);
                                    } else {
					$form.html(response.data.form_html);
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        },
                        size: 'md'
                    });
                };
            }
        };
    }]);

    trapperGrid.directive('actionClassifyFormModal', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
	    template: '<a class="btn btn-default"><span class="fa fa-gear"></span> '+
		global['Translations']['DIRECTIVES']['classify']+'</a>',
            link: function postLink(scope) {
                scope.execute = function(url, resource) {
		    if (resource) {
			var classificationPk = resource.classification_data.pk
			url = url + String(classificationPk) + '/';
			scope.clearSelection();
			scope.data.selected.push(resource.pk);
		    } else {
			var pks = getPKS(scope);
			if (!pks.length) {
                            alert.error(global['Translations']['DIRECTIVES']['select_at_least_one']);
                            return;
			}
		    };

		    var addFormEvents = function() {
			plugins.dynamicTable('#table-repeatable');
			$('a#classify').click(function() {
			    submitClassForm(false);
			});
			$('a#approve').click(function() {
			    submitClassForm(true);
			});
		    };

		    var submitClassForm = function(approve) {
			$('#resources-pks').val(scope.data.selected.join(','));
			var $form = $('#classify-form')
			var data = $form.serializeArray();
			if (approve) {
			    data.push({name: 'approve', value: approve});
			};
			$http({
			    method: 'POST',
			    url: url,
			    data: data
			}).then(function(response) {
			    if (response.data.status) {
				modal.hideModal();
				alert.success(response.data.msg);
				scope.clearSelection();
				scope.load_resources();
			    } else {
				$form.html(response.data.form_html);
				addFormEvents();
			    }
			}).catch(function(error) {
			    modal.hideModal();
			    alert.error(error || msg_server_error);
			});
		    }

		    $http({url: url}).then(function(response) {
			if (response.data.status) {
			    modal.show({
				title: global['Translations']['DIRECTIVES']['classify']
				    + ' (' + scope.data.selected.length.toString() + ' resources)',
				content: response.data.form_html,
				onShow: function($modal) {
				    addFormEvents();
				},
				onHide: function($modal) {
				    scope.clearSelection();
				},
				size: 'xwide'
			    });
			};
		    })
		};
            }
        };
    }]);

    trapperGrid.directive('actionClassifyAi', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['classify_ai'] + '</a>', //ng-disabled="!filters.owner"
            link: function postLink(scope) {

                scope.execute = function($event, url) {
                    $event.preventDefault();

                    var pks = getPKS(scope);

                    if (!pks.length) {
                        alert.error(global['Translations']['DIRECTIVES']['select_one_classification']);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['classify_ai'],
                        url: url,
                        onShow: function($modal) {
                            var $form = $modal.find('form');

                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $("#id_classifications_pks").val(pks);
                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    modal.hideModal();
                                    if (response.data.success) {
                                        alert.success(response.data.msg);
                                    } else {
                                        alert.error(response.data.msg);
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });
                };
            }
        };
    }]);

    trapperGrid.directive('actionUpdateLocation', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-default" title="' +
		global['Translations']['DIRECTIVES']['update_with_map'] + '"><span class="fa fa-pencil"></span></button>',
            link: function postLink(scope) {
                scope.execute = function($event, record) {
                    $event.stopPropagation();
                    window.location.href = record.update_data;
                };
            }
        };
    });

    trapperGrid.directive('actionMap', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-default" title="' +
		global['Translations']['DIRECTIVES']['show_map'] + '"><span class="fa fa-globe"></span></button>',
            link: function postLink(scope) {
                scope.execute = function($event, record) {
                    $event.stopPropagation();
                    localStorage.setItem('trapper.geomap.filter', JSON.stringify([-1]));
                    window.location = record.detail_data;
                };
            }
        };
    });

    trapperGrid.directive('actionPermission', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-primary" title="' +
		global['Translations']['DIRECTIVES']['ask_for_permissions'] + '"><span class="fa fa-question"></span></button>',
            link: function postLink(scope) {
                scope.execute = function($event, record) {
                    $event.stopPropagation();
                    window.location = '/storage/collection/request/' + record.pk;
                };
            }
        };
    });

    function saveClassifyFilters(collection, deployments) {
        var filters_data = {
            'collection_pk': collection,
            'filters': {
                'deployment': deployments
            }
        };
        localStorage.setItem('trapper.classify.settings', JSON.stringify(filters_data));
    }

    trapperGrid.directive('actionClassify', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-default" title="' +
		global['Translations']['DIRECTIVES']['classify'] + '"><span class="fa fa-eye"></span></button>',
            link: function postLink(scope) {
                scope.execute = function(
                    $event, record, deployment_based_nav
                ) {
                    $event.stopPropagation();
                    if (!deployment_based_nav) {
                        localStorage.removeItem('trapper.classify.settings');
                        window.open(record.classify_data);
                    } else {
                        if (!record.resource.deployment) {
                            var deployments = [];
                            localStorage.removeItem('trapper.classify.settings');
                        } else {
                            var deployments = [record.resource.deployment];
                            saveClassifyFilters(record.collection, deployments);
                        }
                    }
                    if (!deployments.length) {
                        window.open(record.classify_data);
                    } else {
                        window.open(
                            record.classify_data + '?deployments=' + deployments.join(',')
                        );
                    }
                };
            }
        };
    });

    trapperGrid.directive('actionClassifyCollection', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-default" title="' + global['Translations']['DIRECTIVES']['classify'] + '"><span class="fa fa-eye"></span></button>',
            link: function postLink(scope) {
                scope.execute = function(
                    $event, record
                ) {
                    $event.stopPropagation();
                    if (typeof(scope.filters.deployments) !== 'undefined') {
                        var deployments = scope.filters.deployments[record.pk];
                    } else {
                        var deployments = []
                    }
                    saveClassifyFilters(record.pk, deployments);
                    if (!deployments.length) {
                        window.open(record.classify_data);
                    } else {
                        window.open(
                            record.classify_data + '?deployments=' + deployments.join(',')
                        );
                    }
                };
            }
        };
    });

    trapperGrid.directive('actionUserClassificationDetails', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-default" title="' +
		global['Translations']['GLOBAL']['details'] + '"><span class="fa fa-search"></span></button>',
            link: function postLink(scope) {
                scope.execute = function(
                    $event, record, deployment_based_nav
                ) {
                    $event.stopPropagation();
                    if (!deployment_based_nav) {
                        localStorage.removeItem('trapper.classify.settings');
                        window.open(record.detail_data);
                    } else {
                        if (!record.resource.deployment) {
                            var deployments = [];
                            localStorage.removeItem('trapper.classify.settings');
                        } else {
                            var deployments = [record.resource.deployment];
                            saveClassifyFilters(record.collection, deployments);
                        }
                    }
                    if (!deployments.length) {
                        window.open(record.detail_data);
                    } else {
                        window.open(
                            record.detail_data + '?deployments=' + deployments.join(',')
                        );
                    }
                };
            }
        };
    });

    trapperGrid.directive('actionWarning', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-warning" title="' +
		global['Translations']['GLOBAL']['warning'] +
		'"><span class="fa fa-exclamation-triangle"></span></button>',
            link: function postLink(scope) {
                scope.execute = function($event, content, record) {
                    $event.stopPropagation();

                    modal.alert({
                        title: global['Translations']['GLOBAL']['warning'],
                        content: content
                    });
                };
            }
        };
    });

    var doc = window.document;

    function c(k) {
        return (doc.cookie.match('(^|; )' + k + '=([^;]*)') || 0)[2];
    }

    function sendPost(url, fields) {
        var form = doc.createElement('form'),
            csrf = doc.createElement('input');

        form.method = 'POST';
        form.action = url;

        fields.forEach(function(field) {
            var input = doc.createElement('input');
            input.type = 'hidden';
            input.name = field.name;
            input.value = field.value;

            form.appendChild(input);
        });

        csrf.type = 'hidden';
        csrf.name = 'csrfmiddlewaretoken';
        csrf.value = c('csrftoken');

        form.appendChild(csrf);

        doc.body.appendChild(form);

        form.submit();
    }

    trapperGrid.directive('actionCreateCollection', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-success">' +
		global['Translations']['DIRECTIVES']['create_collection'] + '</button>',
            link: function postLink(scope) {

                scope.execute = function(url, app) {

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['create_collection'],
                        url: url + '?app=' + app,
                        onShow: function($modal) {
                            var $form = $modal.find('form');
                            plugins.select2();

                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $form.prepend(msg_processing);
                                $("#id_resources_pks").val(pks);
                                $("#id_app").val(app);

                                $http({
                                    method: 'POST',
                                    url: $form.attr('action'),
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        $form.html(
                                            response.data.form_html
                                        );
                                        plugins.select2();
                                        return;
                                    }
                                    window.location = response.data.url;
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });
                };
            }
        };
    }]);

    // add resources to collection
    trapperGrid.directive('actionAddtoCollection', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['add_to_collection'] + '</a>',
            link: function postLink(scope) {
                scope.execute = function($event, url) {
                    $event.preventDefault();

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['choose_collection'],
                        url: url,
                        onShow: function($modal) {
                            var $form = $modal.find('form');
                            plugins.select2ajaxForms();
                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $form.prepend(msg_processing);
                                $("#id_resources_pks").val(pks);
                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        if (response.data.form_html) {
                                            $form.html(
                                                response.data.form_html
                                            );
                                            plugins.select2ajaxForms();
                                        } else {
                                            alert.error(response.data.msg);
                                            modal.hideModal();
                                        }
                                    } else {
                                        alert.success(response.data.msg);
                                        modal.hideModal();
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });

                };
            }
        };
    }]);

    // add collections to research project
    trapperGrid.directive('actionAddtoResearch', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<span>' + global['Translations']['DIRECTIVES']['add_to_research_project'] + '</span>',
            link: function postLink(scope) {
                scope.execute = function($event, url, pks) {
                    $event.preventDefault();

                    var pks = pks || getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['choose_research_project'],
                        url: url,
                        onShow: function($modal) {
                            var $form = $modal.find('form');
                            plugins.select2ajaxForms();
                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $form.prepend(msg_processing);
                                $("#id_collections_pks").val(pks);
                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        if (response.data.form_html) {
                                            $form.html(
                                                response.data.form_html
                                            );
                                            plugins.select2ajaxForms();
                                        } else {
                                            alert.error(response.data.msg);
                                            modal.hideModal();
                                        }
                                    } else {
                                        alert.success(response.data.msg);
                                        modal.hideModal();
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });

                };
            }
        };
    }]);

    // add research project collections to classification project
    trapperGrid.directive('actionAddtoClassification', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['add_to_classification_project'] + '</a>',
            link: function postLink(scope) {
                scope.execute = function($event, url, rproject) {
                    $event.preventDefault();

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['choose_classification_project'],
                        url: url,
                        onShow: function($modal) {
                            var $form = $modal.find('form');
                            plugins.select2ajaxForms();
                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $form.prepend(msg_processing);
                                $("#id_collections_pks").val(pks);
                                $("#id_rproject").val(rproject);
                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        if (response.data.form_html) {
                                            $form.html(
                                                response.data.form_html
                                            );
                                            plugins.select2ajaxForms();
                                        } else {
                                            alert.error(response.data.msg);
                                            modal.hideModal();
                                        }
                                    } else {
                                        alert.success(response.data.msg);
                                        modal.hideModal();
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });

                };
            }
        };
    }]);

    trapperGrid.directive('actionDefinePrefix', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#" ng-disabled="!filters.owner">' +
		global['Translations']['DIRECTIVES']['define_prefix'] + '</a>',
            link: function postLink(scope) {

                function sendRequest(url, prefix, append, pks) {
                    return $http({
                        method: 'POST',
                        url: url,
                        data: {
                            pks: pks,
                            custom_prefix: prefix,
                            append: append
                        }
                    }).then(function(response) {
                        if (!response.data.status) {
                            throw new Error(response.data.msg);
                        }

                        alert.success(global['Translations']['DIRECTIVES']['prefix_defined'] + prefix);

                        return response.data.changed;
                    }).catch(function(error) {
                        alert.error(error || msg_server_error);
                    });
                }

                scope.execute = function($event, url) {
                    $event.preventDefault();

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.confirm({
                        title: global['Translations']['DIRECTIVES']['define_prefix'],
                        content: [
                            '<label class="checkbox-inline"><input type="checkbox">' +
				global['Translations']['DIRECTIVES']['append_location_id'] + '</label>',
                            '<input type="text" class="form-control" placeholder="' +
				global['Translations']['DIRECTIVES']['define_your_prefix'] + '">'
                        ].join('\n'),
                        size: 'sm',
                        buttons: [{
                            label: global['Translations']['GLOBAL']['save'],
                            type: 'success',
                            onClick: function($modal) {
                                var prefix = $modal.find('input[type="text"]').val();
                                var append = $modal.find('input[type="checkbox"]').prop('checked');

                                if (prefix.length || append) {
                                    sendRequest(url, prefix, append, pks).then(function() {
                                        scope.load();
                                    }).catch(function(error) {
                                        alert.error(error || msg_server_error);
                                    });
                                }
                            }
                        }]
                    });
                };
            }
        };
    }]);

    trapperGrid.directive('actionShowMap', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            transclude: true,
            template: ['<div class="btn-group">',
                '<button type="button" class="btn btn-default" ng-disabled="!data.selectedCounter">',
                '<span class="fa fa-globe"></span> ' + global['Translations']['DIRECTIVES']['show_map'] + '</button>',
                '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"',
                ' ng-disabled="!data.selectedCounter">',
                '<span class="fa fa-angle-down"></span>',
                '</button>',
                '<div class="dropdown-menu plain">',
                '<ng-transclude></ng-transclude>',
                '</div></div>'
            ].join('\n'),
            link: function postLink(scope, element, attrs) {
                var btn = element.find('button:nth-child(1)');
                var select = element.find('select');

                select.select2().on('select2-selecting', function(event) {
                    execute(event.val);
                });

                btn.on('click', function() {
                    execute(attrs.url);
                });

                function getFields(fieldName, fieldPk) {
                    var fields = {}
                    var records = scope.data.records.filter(
                        function(record) {
                            return scope.data.selected.indexOf(record.pk) != -1
                        }
                    );
                    records.forEach(function(record) {
                        fields[record[fieldPk]] = record[fieldName]
                    })
                    return fields;
                }

                function execute(url) {
                    localStorage.setItem('trapper.geomap.source', attrs.type);
                    if (attrs.type === 'collections') {
                        var fields = getFields('name', attrs.field)
                        var pks = Object.keys(fields)
                        localStorage.setItem('trapper.geomap.collections_names', JSON.stringify(fields));
                        localStorage.setItem('trapper.geomap.filter', JSON.stringify(pks));
                    } else {
                        localStorage.setItem('trapper.geomap.filter', JSON.stringify(scope.data.selected));
                    }
                    window.location = url;
                }
            }
        };
    });

    function sendRequest($http, url, flag, pks) {
        return $http({
            method: 'POST',
            url: url,
            data: {
                pks: pks
            }
        }).then(function(response) {
            if (!response.data.status) {
                throw new Error(response.data.msg);
            }

            return response.data.changed;
        });
    }

    trapperGrid.directive('actionUnlinkSelected', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['remove_from_collection'] + '</a>',
            link: function postLink(scope) {

                scope.execute = function($event, url) {
                    $event.preventDefault();

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.confirm({
                        title: global['Translations']['DIRECTIVES']['remove_resources'],
                        content: global['Translations']['DIRECTIVES']['remove_question'] +
                            scope.data.selected.length + global['Translations']['DIRECTIVES']['selected_resources_remove'],
                        buttons: [{
                            label: global['Translations']['GLOBAL']['yes'],
                            type: 'success',
                            onClick: function() {
                                alert.info(
                                    global['Translations']['DIRECTIVES']['until_process_is_over_resources']
                                );
                                scope.status.deleting = true;
                                sendRequest($http, url, true, pks).then(function() {
                                    alert.success(global['Translations']['DIRECTIVES']['resources_removed']);
                                    scope.status.deleting = false;
                                    scope.load();
                                }).catch(function(error) {
                                    scope.status.deleting = false;
                                    scope.load();
                                    alert.error(error || msg_server_error);
                                });
                            }
                        }, {
                            label: global['Translations']['GLOBAL']['no'],
                            type: 'danger'
                        }]
                    });
                };
            }
        };
    }]);

    trapperGrid.directive('actionUnlink2Selected', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['remove_from_this_project'] + '</a>',
            link: function postLink(scope) {

                scope.execute = function($event, url) {
                    $event.preventDefault();

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.confirm({
                        title: global['Translations']['DIRECTIVES']['remove_project_collections'],
                        content: global['Translations']['DIRECTIVES']['remove_question'] +
                            scope.data.selected.length + global['Translations']['DIRECTIVES']['selected_collections_remove'],
                        buttons: [{
                            label: global['Translations']['GLOBAL']['yes'],
                            type: 'success',
                            onClick: function() {
                                alert.info(
                                    global['Translations']['DIRECTIVES']['until_process_is_over_project']
                                );
                                scope.status.deleting = true;
                                sendRequest($http, url, true, pks).then(function() {
                                    alert.success(global['Translations']['DIRECTIVES']['collections_removed']);
                                    scope.status.deleting = false;
                                    scope.load();
                                }).catch(function(error) {
                                    scope.status.deleting = false;
                                    scope.load();
                                    alert.error(error || msg_server_error);
                                });
                            }
                        }, {
                            label: global['Translations']['GLOBAL']['no'],
                            type: 'danger'
                        }]
                    });
                };
            }
        };
    }]);

    trapperGrid.directive('actionBlurHumans', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['blur_humans'] + '</a>',
            link: function postLink(scope) {
                scope.execute = function($event, url) {
                    $event.preventDefault();

                    let pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['blur_humans'],
                        url: url,
                        onShow: function ($modal) {
                            let $form = $modal.find('form');

                            $form.on("submit", function (e) {
                                e.preventDefault();
                                $("#id_pks").val(pks);

                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        alert.error(response.data.msg);
                                    } else {
                                        alert.success(response.data.msg);
                                        modal.hideModal();
                                    }
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            })
                        }
                    })
                };
            }
        }
    }]);

    trapperGrid.directive('actionBulkCreateDeployments', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['DIRECTIVES']['add_deployments'] + '</a>',
            link: function postLink(scope) {

                scope.execute = function(url) {

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.external({
                        title: 'Add deployments',
                        url: url,
                        onShow: function($modal) {
                            var $form = $modal.find('form');

                            plugins.select2();
                            plugins.datepicker();

                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $form.prepend(msg_processing);
                                $('#id_locations_pks').val(pks);

                                $http({
                                    method: 'POST',
                                    url: $form.attr('action'),
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        $form.html(
                                            response.data.form_html
                                        );
                                        plugins.select2();
                                        plugins.datepicker();
                                        return;
                                    }
                                    window.location = response.data.url;
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });
                };
            }
        };
    }]);

    // angular helper function (not public) to build querystring from js object   
    function forEachSorted(obj, iterator, context) {
        var keys = sortedKeys(obj);
        for (var i = 0; i < keys.length; i++) {
            iterator.call(context, obj[keys[i]], keys[i]);
        }
        return keys;
    }

    // angular helper function (not public) to build querystring from js object   
    function sortedKeys(obj) {
        var keys = [];
        for (var key in obj) {
            if (obj.hasOwnProperty(key)) {
                keys.push(key);
            }
        }
        return keys.sort();
    }

    // angular helper function (not public) to build querystring from js object   
    function encodeUriQuery(val, pctEncodeSpaces) {
        return encodeURIComponent(val).
        replace(/%40/gi, '@').
        replace(/%3A/gi, ':').
        replace(/%24/g, '$').
        replace(/%2C/gi, ',').
        replace(/%3B/gi, ';').
        replace(/%20/g, (pctEncodeSpaces ? '%20' : '+'));
    }

    // angular helper function (not public) to build querystring from js object   
    function buildUrl(url, params) {
        if (!params) return url;
        var parts = [];
        forEachSorted(params, function(value, key) {
            if (value === null || angular.isUndefined(value)) return;
            if (!angular.isArray(value)) value = [value];

            angular.forEach(value, function(v) {
                if (angular.isObject(v)) {
                    if (angular.isDate(v)) {
                        v = v.toISOString();
                    } else {
                        v = angular.toJson(v);
                    }
                }
                parts.push(encodeUriQuery(key) + '=' +
                    encodeUriQuery(v));
            });
        });
        if (parts.length > 0) {
            url += ((url.indexOf('?') == -1) ? '?' : '&') + parts.join('&');
        }
        return url;
    }

    trapperGrid.directive('actionExportWithFilters', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-default"><span class="fa fa-download"></span> ' +
		global['Translations']['GLOBAL']['export'] + '</button>',
            link: function postLink(scope) {
                scope.execute = function(url) {
                    window.open(buildUrl(url, scope.flattenFilters(scope.filters)));
                };
            }
        };
    });

    trapperGrid.directive('actionExportWithFiltersAggregate', ['$http', function ($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#">' + global['Translations']['GLOBAL']['export_agg'] + '</a>',
            link: function postLink(scope) {
                scope.execute = function (url) {
                    modal.external({
                        title: global['Translations']['GLOBAL']['export_agg'],
                        url: url,
                        onShow: function ($modal) {
                            let $form = $modal.find('form');
                            $form.on('submit', function (e) {
                                e.preventDefault();
                                let filtersStr = buildUrl("", scope.flattenFilters(scope.filters));
                                $("#id_api_url_filters").val(filtersStr);
                                $http({
                                    // call to FormView
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function (response) {
                                    if (response.data.status) {
                                        modal.hideModal();
                                        alert.success(response.data.msg);
                                        // call to ListAPIView
                                        window.open(response.data.url);
                                    } else {
                                        $form.html(response.data.form_html);
                                    }
                                }).catch(function (error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        },
                        size: 'lg'
                    });
                };
            }
        };
    }]);

    trapperGrid.directive('actionExportWithFiltersList', function() {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<a href="#"><span></span></a>',
            link: function postLink(scope, element, attrs) {
                var tag = element.find('span');
                tag[0].innerHTML = ' ' + attrs.title;
                scope.execute = function(url) {
                    window.open(buildUrl(url, scope.flattenFilters(scope.filters)));
                };
            }
        };
    });

    trapperGrid.directive('actionBuildSequences', ['$http', function($http) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            template: '<button class="btn btn-success"><span class="fa fa-cubes"></span> ' +
		global['Translations']['DIRECTIVES']['rebuild_sequences'] + '</button>',
            link: function postLink(scope) {

                scope.execute = function(url) {

                    var pks = getPKS(scope);
                    if (!pks.length) {
                        alert.error(msg_zero_items_selected);
                        return;
                    }

                    modal.external({
                        title: global['Translations']['DIRECTIVES']['rebuild_sequences'],
                        url: url,
                        onShow: function($modal) {
                            var $form = $modal.find('form');

                            $form.on('submit', function(e) {
                                e.preventDefault();
                                $("#id_collections_pks").val(pks);

                                $http({
                                    method: 'POST',
                                    url: url,
                                    data: $form.serializeArray()
                                }).then(function(response) {
                                    if (!response.data.success) {
                                        if (response.data.form_html) {
                                            $form.html(
                                                response.data.form_html
                                            );
                                        } else {
                                            alert.error(response.data.msg);
                                        }
                                    } else {
                                        alert.success(response.data.msg);
                                    }
                                    modal.hideModal();
                                }).catch(function(error) {
                                    modal.hideModal();
                                    alert.error(error || msg_server_error);
                                });
                            });
                        }
                    });
                };
            }
        };
    }]);

    /* FILTER DIRECTIVES */

    trapperGrid.directive('filterDate', ['$timeout', 'config', function($timeout, cfg) {
        return {
            restrict: 'A',
            scope: {
                model: '=filterDate',
                name: '=filterName',
                filterService: '='
            },
            link: function postLink(scope, element) {
                var timeoutId;

                scope.model = {};

                scope.$on('filters:clear', function() {
                    picker.find('input[type=text]').datepicker('setDate', '');
                });

                function update() {
                    if (timeoutId) { $timeout.cancel(timeoutId); }

                    timeoutId = $timeout(function() {
                        scope.filterService.updateFilter({
                            name: scope.name,
                            type: 'date',
                            value: scope.model
                        });
                    }, 50);
                }

                var picker = $(element).datepicker({
                    format: cfg.dateFormat.picker.toLowerCase(),
                    clearBtn: true,
                    autoclose: true,
                    orientation: 'bottom right',
                    todayHighlight: true
                });

                picker.on('changeDate', function(e) {
                    scope.model = e.format();
                    update();

                }).on('clearDate', function(e) {
                    scope.model = '';
                    update();
                });
            }
        };
    }]);

    trapperGrid.directive('filterDaterange', ['$timeout', 'config', function($timeout, cfg) {
        return {
            restrict: 'A',
            scope: {
                model: '=filterDaterange',
                name: '=filterName',
                filterService: '='
            },
            link: function postLink(scope, element) {
                var timeoutId;

                scope.model = {
                    from: '',
                    to: ''
                };

                scope.model = angular.extend(
                    scope.model, scope.$parent.filters[scope.name]
                );

                scope.$on('filters:clear', function() {
                    picker.find(
			'input[type=text][placeholder=' + global['Translations']['GLOBAL']['from'] + ']'
		    ).datepicker('setDate', '');
                    picker.find(
			'input[type=text][placeholder=' + global['Translations']['GLOBAL']['to'] + ']'
		    ).datepicker('setDate', '');
                });

                scope.$on('filters:rebuild', function() {
                    picker.find(
			'input[type=text][placeholder=' + global['Translations']['GLOBAL']['from'] + ']'
		    ).datepicker(
                        'setDate',
                        scope.model.from
                    );
                    picker.find(
			'input[type=text][placeholder=' + global['Translations']['GLOBAL']['to'] + ']'
		    ).datepicker(
                        'setDate',
                        scope.model.to
                    );
                });

                function update() {
                    if (timeoutId) { $timeout.cancel(timeoutId); }

                    timeoutId = $timeout(function() {
                        scope.filterService.updateFilter({
                            name: scope.name,
                            type: 'daterange',
                            value: scope.model
                        });
                    }, 50);
                }

                var picker = $(element).datepicker({
                    format: cfg.dateFormat.picker.toLowerCase(),
                    clearBtn: true,
                    autoclose: true,
                    orientation: 'bottom right',
                    todayHighlight: true
                });

                picker.on('changeDate', function(e) {
                    scope.model[e.target.placeholder] = e.format();
                    update();

                }).on('clearDate', function(e) {
                    scope.model[e.target.placeholder] = '';
                    update();
                });
            }
        };
    }]);

    trapperGrid.directive('filterTimerange', ['$timeout', 'config', function($timeout, cfg) {
        return {
            restrict: 'A',
            scope: {
                model: '=filterTimerange',
                name: '=filterName',
                filterService: '='
            },
            link: function postLink(scope, element, attr) {
                var timeoutId;

                scope.model = {
                    from: '',
                    to: ''
                };

                scope.model = angular.extend(
                    scope.model, scope.$parent.filters[scope.name]
                );

                scope.$on('filters:clear', function() {
                    pickerFrom.timepicker('setTime', '');
                    pickerTo.timepicker('setTime', '');
                    scope.model = {};
                });

                scope.$on('filters:rebuild', function() {
                    pickerFrom.timepicker(
                        'setTime',
                        scope.model.from
                    );
                    pickerTo.timepicker(
                        'setTime',
                        scope.model.to
                    );
                    //update();
                });

                function update() {
                    if (timeoutId) { $timeout.cancel(timeoutId); }

                    timeoutId = $timeout(function() {
                        scope.filterService.updateFilter({
                            name: scope.name,
                            type: 'timerange',
                            value: scope.model
                        });
                    }, 50);
                }

                var inputs = $(element).find('input');
                var pickerFrom = $(inputs[0]).timepicker({
                    show2400: cfg.timeFormat.is24,
                    timeFormat: cfg.timeFormat.picker
                });
                var pickerTo = $(inputs[1]).timepicker({
                    show2400: cfg.timeFormat.is24,
                    timeFormat: cfg.timeFormat.picker
                });

                pickerFrom.on('change', function(e) {
                    scope.model[e.target.placeholder] = e.target.value;
                    update();
                });
                pickerTo.on('change', function(e) {
                    scope.model[e.target.placeholder] = e.target.value;
                    update();
                });
            }
        };
    }]);

    trapperGrid.directive('filterTokens', function() {
        return {
            restrict: 'A',
            scope: {
                filterService: '='
            },
            require: 'ngModel',
            link: function postLink(scope, element, attr, ctrl) {
                var filterName = attr.filterTokens;
                if (attr.class == 'select2ajax') {
                    var ajaxOptions = {
                        multiple: attr.filterMultiple,
                        placeholder: attr.filterPlaceholder,
                        apiUrl: attr.filterApiUrl,
                        textLabel: attr.filterTextLabel,
                        idLabel: attr.filterIdLabel,
                        initLabel: attr.filterInitLabel
                    };
                    var select2 = plugins.select2ajax(element, ajaxOptions);
                } else {
                    var select2 = $(element).select2({
                        minimumResultsForSearch: 5,
                        minimumInputLength: 3
                    });
                };

                scope.$on('filters:clear', function() {
                    setTimeout(function() {
                        select2.select2('val', null).trigger('change');
                    }, 0);
                });

                scope.$on('filters:rebuild', function() {
                    select2.select2('val', scope.$parent.filters[filterName]).trigger('change');
                });

                scope.$watch(function() {
                    return ctrl.$modelValue;
                }, function(newValue, oldValue) {
                    if (newValue === oldValue) { return; }
                    // workaround for select2 v3.x.x with ajax
                    if (attr.class == 'select2ajax') {
                        if (!Array.isArray(newValue)) {
                            if (newValue.length == 0) {
                                newValue = Array()
                            } else {
                                newValue = newValue.split(',');
                            }
                        }
                        ctrl.$setViewValue(newValue);
                    }
                    scope.filterService.updateFilter({
                        name: filterName,
                        type: 'array',
                        value: newValue
                    });
                });
            }
        };
    });

    trapperGrid.directive('filterText', function() {
        return {
            restirct: 'A',
            require: 'ngModel',
            scope: {
                filterService: '='
            },
            link: function postLink(scope, element, attr, ctrl) {
                var filterName = attr.filterText;
                var format = attr.filterFormat;

                // set default value - for selects actually
                ctrl.$setViewValue('');

                scope.$on('filters:clear', function() {
                    ctrl.$setViewValue('');
                    ctrl.$render();
                });

                scope.$watch(function() {
                    return ctrl.$modelValue;
                }, function(newValue, oldValue) {
                    if (newValue === oldValue) { return; }

                    if (format && newValue) {
                        if (format === 'float') {
                            newValue = parseFloat(newValue, 10);
                        } else if (format === 'integer') {
                            newValue = parseInt(newValue, 10);
                        } else if (format === 'boolean') {
                            newValue = (newValue.toLowerCase() === 'true');
                        }
                    }

                    scope.filterService.updateFilter({
                        name: filterName,
                        type: 'text',
                        value: newValue,
                        exact: true
                    });
                });
            }
        };
    });

    trapperGrid.directive('filterCheckbox', function() {
        return {
            restirct: 'A',
            require: 'ngModel',
            scope: {
                settings: '=filterCheckbox',
                filterService: '='
            },
            link: function postLink(scope, element, attr, ctrl) {
                var filterName = scope.settings.name;

                scope.$on('filters:clear', function() {
                    ctrl.$setViewValue(false);
                    ctrl.$render();
                });

                scope.$watch(function() {
                    return ctrl.$modelValue;
                }, function(newValue, oldValue) {
                    if (newValue === oldValue) { return; }

                    scope.filterService.updateFilter({
                        name: filterName,
                        type: 'text',
                        value: newValue ? scope.settings.value : '',
                        exact: true
                    });
                });
            }
        };
    });

    trapperGrid.directive('filterSearch', ['$timeout', function($timeout) {
        return {
            restirct: 'A',
            require: 'ngModel',
            scope: {
                filterService: '='
            },
            link: function postLink(scope, element, attr, ctrl) {
                var filterName = attr.filterSearch;
                var timeoutId;

                scope.$on('filters:clear', function() {
                    ctrl.$setViewValue('');
                    ctrl.$render('');
                });

                scope.$watch(function() {
                    return ctrl.$modelValue;
                }, function(newValue, oldValue) {
                    if (timeoutId) { $timeout.cancel(timeoutId); }

                    timeoutId = $timeout(function() {
                        if (newValue === oldValue) { return; }

                        if (newValue.length < 3 && oldValue && oldValue.length < 3) {
                            newValue = '';
                        }

                        scope.filterService.updateFilter({
                            name: filterName,
                            type: 'text',
                            value: newValue
                        });
                    }, 250);
                });
            }
        };
    }]);

}(window, angular));
