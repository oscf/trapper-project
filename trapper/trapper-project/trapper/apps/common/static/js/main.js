'use strict';

(function (global, namespace) {
    var alert = global[namespace].Alert || {};
    var module = {
        run: function () {
            alert.init();
            this.initActiveApp();
        },
        initActiveApp: function () {
            var appInfo = global.document.body.dataset.app;
            if (!appInfo) {
                return;
            }

            console.log("AppInfo:", appInfo);
            appInfo = appInfo.split('::');
            var appName = appInfo[0][0].toUpperCase() + appInfo[0].slice(1);

            var activeApp = global[namespace][appName];
            console.log("Active app:", appName)
            if (activeApp.hasOwnProperty(appInfo[1])) {
                activeApp[appInfo[1]].call(activeApp);
            } else {
                activeApp.init();
            }
        }

    };

    // safe way to handle DOM ready event
    function DOMready() {
        window.document.removeEventListener('DOMContentLoaded', DOMready);
        module.run();
    }

    if (document.readyState === 'complete') {
        module.run();
    } else {
        window.document.addEventListener('DOMContentLoaded', DOMready);
    }
}(window, 'TrapperApp'));
