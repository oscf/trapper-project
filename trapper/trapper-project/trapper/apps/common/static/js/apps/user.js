'use strict';

(function (global, namespace, moduleName) {

    var module = {};

    var plugins = global[namespace].Plugins;
    var alert = global[namespace].Alert;
    var modal = global[namespace].Modal;
    var uploader = global[namespace].Uploader;

    module.init = function () {
        console.log(moduleName + ' initialize');
        plugins.fileInputs();
    };

    module.edit = function () {
        plugins.select2();
	// add click event "Show token"
	$('#token').click(function(event) {
	    event.preventDefault();
	    modal.show({
		title: 'Your API authorization token',
		content: $(this).attr('token')
	    });
	});
    };

// if passed namespace does not exist, create one
    global[namespace] = global[namespace] || {};

// append module to given namespace
    global[namespace][moduleName] = module;

}(window, 'TrapperApp', 'User'));
