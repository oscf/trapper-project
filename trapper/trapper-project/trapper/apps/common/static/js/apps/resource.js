'use strict';

(function(global, namespace, moduleName) {

    var module = {};
    var plugins = global[namespace].Plugins;
    var alert = global[namespace].Alert;
    var modal = global[namespace].Modal;

    var doc = global.document;

    var comments = function() {
        var replyBtns = doc.querySelectorAll('.btn-reply');
        var parentId = doc.querySelector('#id_parent');
        var message = doc.querySelector('#id_comment');

        function reply(event) {
            parentId.value = event.target.parentNode.parentNode.parentNode.dataset.pk;
            message.focus();
        }
        [].forEach.call(replyBtns, function(btn) {
            btn.addEventListener('click', reply);
        });
    };

    var deleteConfirm = function() {
        var deleteBtn = doc.querySelector('.btn-delete');
        if (!deleteBtn) return;
        deleteBtn.addEventListener('click', showModal);

        function showModal(e) {
            e.preventDefault();
            modal.confirm({
                title: global['Translations']['GLOBAL']['delete_resources'],
                content: global['Translations']['GLOBAL']['delete_resources_question'],
                buttons: [{
                    type: 'success',
                    label: global['Translations']['GLOBAL']['yes'],
                    onClick: function() {
                        window.location = e.target.href;
                    }
                }, {
                    type: 'danger',
                    label: 'No'
                }]
            });
        }
    };

    module.list = function() {
        plugins.select2();
    };

    module.upload = module.update = function() {
        plugins.datepicker();
        plugins.fileInputs();
        plugins.select2ajaxForms();
        plugins.select2();
    };

    module.preview = function() {
        deleteConfirm();
        comments();
        plugins.smallMap();
	plugins.videoPlayer('trapper-video-player');
	var gallery = plugins.gallery('#div-img-bbox', 'this');
	// add click event "Share"
	$('#share').click(function(event) {
	    event.preventDefault();
	    modal.show({title: 'Share resource', content: this.href});
	});
	// add click event "Show original"
	$('#show-original').click(function(event) {
	    event.preventDefault();
	    gallery[0].dataset.src = this.href;
	    gallery.trigger('click');
	});
	// add click event "Show objects"
	$('#show-objects').click(function(event) {
	    event.preventDefault();
	    plugins.drawCanvas('#img-bbox', true);
	});
    };

    // if passed namespace does not exist, create one
    global[namespace] = global[namespace] || {};

    // append module to given namespace
    global[namespace][moduleName] = module;

}(window, 'TrapperApp', 'Resource'));
