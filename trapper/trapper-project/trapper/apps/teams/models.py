from django.db import models
from django.contrib.gis.db import models as gis_models
from django.conf import settings

from django.utils.translation import gettext_lazy as _

from django.core.validators import ValidationError

from trapper.apps.media_classification.models import ClassificationProject
from trapper.apps.geomap.models import Deployment
from trapper.apps.common.models import BaseDateModel


class Team(BaseDateModel):
    """
    Team model which integrating specific Classification Project
    with Team Leader (owner) and other CS users

    Team can be extra defined by polygon data
    """

    name = models.CharField(_("Name"), max_length=150)
    description = models.TextField(_("Description"))

    classification_project = models.ForeignKey(
        ClassificationProject,
        verbose_name=_("Classification project"),
        on_delete=models.CASCADE,
        related_name="classification_project_teams",
        help_text=_("Active workspace in Citizen Science"),
    )

    team_leader = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Team Leader"),
        on_delete=models.CASCADE,
        related_name="team_leaders",
    )
    members = models.ManyToManyField(
        settings.AUTH_USER_MODEL,
        verbose_name=_("Members"),
        related_name="members",
        through="TeamMembership",
    )
    deployments = models.ManyToManyField(
        Deployment,
        verbose_name=_("Deployments"),
        related_name="team_deployments",
        blank=True,
    )

    polygon = gis_models.PolygonField(_("Polygon"), blank=True, null=True)

    class Meta:
        ordering = ["-date_created"]

        verbose_name = _("Team")

    def can_manage(self, user) -> bool:
        """
        Return True or False depends on if the user is a Team Leader
        and can manage the team (edit, delete)
        """

        return self.team_leader == user

    @property
    def members_count(self) -> int:
        """
        Return number of all members in the Team
        """

        return self.members.count()

    def validate_unique(self, *args, **kwargs) -> None:
        """
        Overrided default method to make sure that team leader
        will be not able to create a team with the same name
        and classification project
        """

        super(Team, self).validate_unique(*args, **kwargs)

        qs = self.__class__._default_manager.filter(
            team_leader=self.team_leader,
            classification_project=self.classification_project,
            name=self.name,
        )

        # Exclude itself during existing object edition
        if not self._state.adding and self.pk is not None:
            qs = qs.exclude(pk=self.pk)

        if qs.exists():
            raise ValidationError(
                {
                    "name": ["This team was created before"],
                }
            )

    def __str__(self) -> str:
        return self.name


class TeamMembership(BaseDateModel):
    """
    Team membership model keep current user membership
    status and role in available Teams
    """

    class MembershipRole(models.IntegerChoices):
        TEAM_LEADER = 0, "Team Leader"
        MEMBER = 1, "Member"

    class MembershipStatus(models.IntegerChoices):
        PENDING = 0, "Pending"
        APPROVED = 1, "Approved"
        REJECTED = 2, "Rejected"

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("User"),
        on_delete=models.CASCADE,
        related_name="user_memberships",
    )
    team = models.ForeignKey(
        Team,
        verbose_name=_("Team"),
        on_delete=models.CASCADE,
        related_name="team_memberships",
    )

    role = models.SmallIntegerField(
        _("Role"), choices=MembershipRole.choices, default=MembershipRole.TEAM_LEADER
    )
    status = models.SmallIntegerField(
        _("Status"),
        choices=MembershipStatus.choices,
        default=MembershipStatus.PENDING,
        db_index=True,
    )
    motivation = models.TextField(_("Motivation"), default="", blank=True)

    def validate_unique(self, *args, **kwargs) -> None:
        """
        Overrided default method to make sure that single user
        will be not a member more than once in the team
        """

        super(TeamMembership, self).validate_unique(*args, **kwargs)

        qs = self.__class__._default_manager.filter(user=self.user, team=self.team)

        # Exclude itself during existing object edition
        if not self._state.adding and self.pk is not None:
            qs = qs.exclude(pk=self.pk)

        if qs.exists():
            raise ValidationError(
                {
                    "user": ["This user exists in the team"],
                }
            )

    class Meta:
        ordering = ["-date_created"]

        verbose_name = _("Team membership")

    def __str__(self) -> str:
        return str(self.user)
