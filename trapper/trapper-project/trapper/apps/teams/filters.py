import django_filters
from django_filters import CharFilter, BooleanFilter

from django.db.models import Q

from .models import Team


class TeamFilters(django_filters.FilterSet):
    """
    Custom search and filtering criteria for Teams
    """

    search = CharFilter(method="search_term")
    owned_by_me = BooleanFilter(method="owner_filter")

    def search_term(self, queryset, name, value: str):
        """
        Searching function on Team name and description values
        """

        if value:
            queryset = queryset.filter(
                Q(name__icontains=value) | Q(description__icontains=value)
            ).distinct()

        return queryset

    def owner_filter(self, queryset, name, value: str):
        """
        Filtering Teams created by the user (Team Leader)
        """

        user = getattr(self.request, "user", None)

        if user and value:
            queryset = queryset.filter(team_leader=user)

        return queryset

    class Meta:
        model = Team
        fields = ["search", "owned_by_me"]


class NewMemberFilters(django_filters.FilterSet):
    """
    Custom search for Users
    """

    user = CharFilter(method="search_user")

    def search_user(self, queryset, name, value: str):
        """
        Searching function on User username, email and last name
        """

        queryset = queryset.filter(username__icontains=value)

        return queryset
