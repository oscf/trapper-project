# -*- coding: utf-8 -*-
"""Views used by DRF to display json data used by research application"""

from trapper.apps.common.views_api import PaginatedReadOnlyModelViewSet
from trapper.apps.extra_tables import serializers
from trapper.apps.extra_tables.filters import SpeciesFilter
from trapper.apps.extra_tables.models import Species


class SpeciesViewSet(PaginatedReadOnlyModelViewSet):
    """
    Return data of species taxonomy.
    """

    serializer_class = serializers.SpeciesSerializer
    filterset_class = SpeciesFilter
    search_fields = ["english_name", "latin_name"]
    queryset = Species.objects.all()
