import factory

from trapper.apps.extra_tables.models import Species


class SpeciesFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Species

    latin_name = factory.Faker("word", locale="la")
    english_name = factory.Sequence(lambda n: f"english_name_{n}")
