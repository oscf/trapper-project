# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.8-slim-buster

# Install OS depoendencies
RUN apt-get update \
    && mkdir -p /usr/share/man/man1 \
    && mkdir -p /usr/share/man/man7 \
    && apt-get install -y \
    libxml2-dev \
    libjpeg-dev \
    libxslt1-dev \
    libproj-dev \
    libpq-dev \
    zlib1g-dev \
    libldap2-dev \
    libsasl2-dev \
    ldap-utils \
    libspatialindex-dev \
    gdal-bin \
    ffmpeg \
    libyaml-dev \
    gettext \
    make \
    gcc \
    git \
    libmemcached-dev

# Install python requirements
COPY ./trapper/setup.py ./
RUN pip install --ignore-installed -e .

# Expose port 8000
EXPOSE 8000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

# Set main folder and copy data
WORKDIR /app
ADD . /app

RUN chmod +x /app/trapper/entrypoint.sh && chmod +x /app/trapper/wait_for.sh
RUN chmod -R 755 /app

# Create trapper user & switch to this new account
ARG TRAPPER_USER_UID
ARG TRAPPER_USER_GID
ARG APP_VERSION
RUN echo '{ "version": "'${APP_VERSION}'" }' > /version.json
RUN groupadd -g ${TRAPPER_USER_GID} trapper && useradd trapper -u ${TRAPPER_USER_UID} -g ${TRAPPER_USER_GID} && chown -R trapper /app
USER trapper

CMD ["/app/trapper/entrypoint.sh"]