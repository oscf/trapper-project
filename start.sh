#!/usr/bin/env bash
set -e

function usage () {
echo  "$(basename "$0") [options] [CONFIGURATION] -- Start Trapper with given configuration.
Options:
    -p         - Pull docker images.
    -b         - Build docker images.
    -d         - Use external database instead of docker postgres service.
Configurations:
    prod       - Production grade Trapper.
    dev        - For development purposes."
}

# Is the .env file created?
if [ ! -f .env ]; then
    echo "To set the configuration, copy your customized file trapper.env as .env"
    echo "cp trapper.env .env"
    exit 0
fi

pull=false
build=false
external_db=false

export APP_VERSION=$(git describe --tags --always --dirty)

# Parse options
while getopts ":pbd" opt; do
    case ${opt} in
	p)
	    pull=true
	    ;;
	b)
	    build=true
	    ;;
	d)
	    external_db=true
	    ;;
	\?)
	    echo "Invalid Option: -$OPTARG" 1>&2
	    exit 1
	    ;;
    esac
done
shift $((OPTIND -1))

# Is the configuration selected?
if [ "$1" != "prod_db" ] && [ "$1" != "prod" ] && [ "$1" != "dev_db" ] && [ "$1" != "dev" ]; then
    usage
    exit 0
fi

# update .env file with system-level parameters
export TRAPPER_USER_UID=$(id -u)
export TRAPPER_USER_GID=$(id -g)

tmpfile=$(mktemp)
cat .env | envsubst > "$tmpfile" &&  mv "$tmpfile" .env

# prepare SSL certificates
source .env
if [ ! -f $SSL_CERTIFICATE ] || [ ! -f $SSL_CERTIFICATE_KEY ]; then
    # SSL certificates are not available so generate them
    openssl req -subj '/CN=localhost' -x509 -newkey rsa:4096 -nodes -keyout ./ssl/key.pem -out ./ssl/cert.pem -days 365;
    # combine key.pem & cert.pem into one file to support FTPS for pure-ftpd
    cat ./ssl/cert.pem ./ssl/key.pem > ./ssl/combined_cert.pem
else
    # SSL certificates exist and can be mounted as docker volumes so only create the combined cert for pure-ftpd
    cat $SSL_CERTIFICATE $SSL_CERTIFICATE_KEY > ./ssl/combined_cert.pem
fi

config=$1; shift
case "$config" in
    prod)
	if [ "$1" = "stop" ]; then
	    docker compose -f docker-compose.yml -f docker-compose.prod.yml down
	    exit 0
	fi
	if $build; then
	    docker compose -f docker-compose.yml -f docker-compose.prod.yml build
	fi
	if $pull; then
	    docker compose -f docker-compose.yml -f docker-compose.prod.yml pull --ignore-pull-failures
	fi
	if $external_db; then
	    docker compose -f docker-compose.yml -f docker-compose.prod.yml up --scale postgres=0
	else
	    docker compose -f docker-compose.yml -f docker-compose.prod.yml up
	fi
	;;
    dev)
	if [ "$1" = "stop" ]; then
	    docker compose down
	    exit 0
	fi
	if $build; then
	    docker compose build
	fi
	if $pull; then
	    docker compose pull --ignore-pull-failures
	fi
	if $external_db; then
	    docker compose up --scale postgres=0
	else
	    docker compose up
	fi
	;;
esac
